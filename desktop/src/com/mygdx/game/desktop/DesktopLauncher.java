package com.mygdx.game.desktop;

import main.Game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Balance";
		config.width = Game.V_WIDTH * 2;
		config.height = Game.V_HEIGHT * 2;
		config.useGL30 = true; //this is important
		new LwjglApplication(new Game(), config);
	}
}
