package com.wecanbalance.duplicate.multiplayer;


public class CreatePlayerEvent extends NetEvent
{
	private int x;
	private int y;
	
	private static final long serialVersionUID = 1L;
	
	public CreatePlayerEvent(int x, int y)
	{
		super(EventType.CREATE_PLAYER);
		this.setX(x);
		this.setY(y);
	}

	@Override
	public byte[] serializeHeader()
	{
		this.message = serializeMessage();
		this.header.putInt(message.length);
		this.header.putChar((char) this.type.ordinal());
		return this.header.array();

	}

	@Override
	public byte[] serializeMessage()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(x);
		sb.append(",");
		sb.append(y);
		this.message = sb.toString().getBytes();
		return this.message;
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}
	
	
}
