package com.wecanbalance.duplicate.multiplayer;

public interface Serialize
{
	public byte[] serializeHeader();
	public byte[] serializeMessage();
}
