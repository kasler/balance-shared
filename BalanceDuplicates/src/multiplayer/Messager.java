package com.wecanbalance.duplicate.multiplayer;

import java.util.concurrent.ConcurrentLinkedQueue;

public class Messager
{
	private int i;
	protected ConnectionI connection;
	private ConcurrentLinkedQueue<byte[]> queue;
	private long delay;

	public Messager(ConnectionI connection, long delayMS)
	{	
		queue = new ConcurrentLinkedQueue<byte[]>();
		this.delay = delayMS;
		i = 0;
		this.connection = connection;
	}
	
	public void send(BalanceMessage msg)
	{
		msg.i = i;
		++i;
		
		connection.sendMessage(msg.toSend());
	}
	
	public void add(byte[] msg)
	{
		queue.add(msg);
	}
	
	public BalanceMessage recv()
	{
		if (!this.queue.isEmpty()) //&&
				//TimeUtils.timeSinceMillis(this.queue.peek().time) > delay)
		{
			return new BalanceMessage(queue.poll());
		}
		
		return null;
	}
}