package com.wecanbalance.duplicate.multiplayer;

public class NetworkCons
{
	private final static int tcpPort = 26001;
	private final static int udpPort = 27000;
	private final static String localhost = "localhost";
	private final static int timeout = 5000;
	private final static String androidLocalhost = "10.0.2.2";
	private final static int HEADER_LENGTH = 4;

	public static int getTcpPort()
	{
		return tcpPort;
	}

	public static int getUdpPort()
	{
		return udpPort;
	}

	public static String getLocalhost()
	{
		return localhost;
	}

	public static int getTimeout()
	{
		return timeout;
	}

	public static String getAndroidlocalhost()
	{
		return androidLocalhost;
	}

	public static int getHeaderLength()
	{
		return HEADER_LENGTH;
	}
}
