package com.wecanbalance.duplicate.multiplayer;

public class BalanceMessage
{
	public float posX;
	public float posY;
	public float velX;
	public float velY;
	public float angVel;
	public long time;
	public int i;
	
	public BalanceMessage(float posX, float posY, float velX, float velY, float angVel, long time)
	{
		this.i = 0;
		this.posX = posX;
		this.posY = posY;
		this.velX = velX;
		this.velY = velY;
		this.angVel = angVel;
		this.time = time;
	}
	
	public BalanceMessage(byte[] msg)
	{	
		this.i = bytearrayToInt(msg, 0);
		this.posX = bytearrayToInt(msg, 4);
		this.posY = bytearrayToInt(msg, 8);
		this.velX = bytearrayToInt(msg, 12);
		this.velY = bytearrayToInt(msg, 16);
		this.angVel = bytearrayToInt(msg, 20);
		this.time = this.i = bytearrayToInt(msg, 24);
	}
	
	private int bytearrayToInt(byte[] msg, int startIndex)
	{
		return (msg[startIndex++]<<24)&0xff000000|
				(msg[startIndex++]<<16)&0x00ff0000|
				(msg[startIndex++]<< 8)&0x0000ff00|
				(msg[startIndex++]<< 0)&0x000000ff;
	}
	
	private void intToBytearray(int n, int startIndex, byte[] b)
	{
		for (int i = 0; i < 4; i++)
		    b[startIndex++] = (byte)(n >>> (i * 8));
	}

	public byte[] toSend()
	{
		byte[] b = new byte[28];
		
		intToBytearray(this.i, 0, b);
		intToBytearray((int) this.posX, 4, b);
		intToBytearray((int) this.posY, 8, b);
		
		intToBytearray((int) this.velX, 12, b);
		intToBytearray((int) this.velY, 16, b);
		
		intToBytearray((int) this.angVel, 20, b);
		
		intToBytearray((int) this.time, 24, b);
		
		return b;
	}
}
