package com.wecanbalance.duplicate.multiplayer;

public interface ConnectionI
{
	public boolean connectUdp();
	public void connectTcp();
	public void sendMessage(String message);
	public void sendMessage(byte[] message);
	public void disconnect();
	
}
