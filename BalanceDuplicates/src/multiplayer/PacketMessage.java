package com.wecanbalance.duplicate.multiplayer;

public class PacketMessage {

	public PacketMessage(String string)
	{
		this.message = string;
	}

	//This is the Packet class. Everything in this object can be sent over the network!
	public String message;
	
}
