package com.wecanbalance.duplicate.multiplayer;

import java.nio.ByteBuffer;

public abstract class NetEvent implements Serialize
{
	protected ByteBuffer header;
	protected EventType type;
	protected byte[] message;
		
	public enum EventType 
	{
		CREATE_PLAYER,
		MOVEMENT
	}
	
	public NetEvent(EventType type)
	{
		this.header = ByteBuffer.allocate(4);
		this.type = type;
	}
	
	public ByteBuffer getHeader()
	{
		return header;
	}

	public void setHeader(ByteBuffer header)
	{
		this.header = header;
	}

	public byte[] getMessage()
	{
		return message;
	}

	public void setMessage(byte[] message)
	{
		this.message = message;
	}
}
