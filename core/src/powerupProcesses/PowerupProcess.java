package powerupProcesses;

import processes.AnimationDrawProcess;
import processes.FrameDrawProcess;
import processes.Process;
import main.Game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import entities.B2DSprite;

public class PowerupProcess extends Process
{		
	AnimationDrawProcess creationAnimation;
	AnimationDrawProcess glowingAnimation;
	FrameDrawProcess powerupIconProcess;
	
	int loopsForFade;
	
	public PowerupProcess(String i_name, B2DSprite i_parent,
						  TextureRegion powerupIconFrame, TextureRegion[] glowingSprites)
	{
		super(i_name, i_parent, false);
		
		loopsForFade = 30;
		
		powerupIconProcess = new FrameDrawProcess(i_name, i_parent, powerupIconFrame);
		
		Texture creationtex = Game.res.getTexture("powerup_creation");
		
		TextureRegion[] creationSprites = TextureRegion.split(creationtex, 
															  32 * Game.OBJECT_SCALE, 
															  32 * Game.OBJECT_SCALE)[0];
		creationAnimation = new AnimationDrawProcess("powerupCreation", creationSprites, false, 1, i_parent);

		glowingAnimation = new AnimationDrawProcess("powerupGlowing", glowingSprites, 1/12f, true, 1, i_parent, 10000);
	}
	
	@Override
	public void start()
	{
		creationAnimation.start();
		powerupIconProcess.start();
		glowingAnimation.start();
	}

	@Override
	public void pause()
	{
		powerupIconProcess.pause();
		creationAnimation.pause();
		glowingAnimation.pause();
	}

	@Override
	public void resume()
	{
		powerupIconProcess.resume();
		creationAnimation.resume();
		glowingAnimation.resume();
	}
	
	public void stop()
	{
		powerupIconProcess.stop();
		creationAnimation.stop();
		glowingAnimation.stop();
	}

	@Override
	public void update(float dt)
	{
		if (isCompleted)
		{
			return;
		}
		
		if (!creationAnimation.isProcessComplete())
		{
			creationAnimation.update(dt);
		}
		
		powerupIconProcess.update(dt);
		
		glowingAnimation.update(dt);

		// Should we start fading?
		if (glowingAnimation.getTimesPlayed() >= loopsForFade)
		{
			((AnimationDrawProcess)glowingAnimation).setDelay(1f/24f);
		}
		
		if (glowingAnimation.isProcessComplete())
		{
			isCompleted = true;
		}
	}

	@Override
	public void render(SpriteBatch sb)
	{
		if (isCompleted)
		{
			return;
		}
		
		powerupIconProcess.render(sb);
		glowingAnimation.render(sb);
		
		if (!creationAnimation.isProcessComplete())
		{
			creationAnimation.render(sb);
		}
	}

	@Override
	public void finish()
	{
		super.finish();
		powerupIconProcess.finish();
		creationAnimation.finish();
		glowingAnimation.finish();
	}
}
