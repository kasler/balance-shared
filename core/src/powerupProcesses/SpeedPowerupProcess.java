package powerupProcesses;

import gameObjects.Timer;

import java.util.ArrayList;

import processes.AfterImageProcess;
import main.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars;
import definitions.GameStats;
import definitions.PlayerStats;
import definitions.B2DVars.FACING_SIDE;
import entities.Player;

public class SpeedPowerupProcess extends AfterImageProcess
{
	private Timer powerupTimer;
	Player player;
	SpeedPowerupStats stats;
	
	public class SpeedPowerupStats
	{
		public int level;
		public long time;
		public float topSpeed;
		public float acceleration;
		public float groundDamping;
		public float airDamping;
		
		public SpeedPowerupStats(PlayerStats stats)
		{
			level = stats.speedPowerupLevel;
			time = GameStats.speedPowerupTime[stats.typeIndex][level];
			topSpeed = stats.topSpeed * GameStats.speedPowerupSpeedFactor[stats.typeIndex][level];
			acceleration = stats.accelerationForce * GameStats.speedPowerupAccelerationFactor[stats.typeIndex][level];
			groundDamping = GameStats.speedPowerupGroundDamping[stats.typeIndex][level];
			airDamping = GameStats.speedPowerupAirDamping[stats.typeIndex][level];
		}
	}
	
	public SpeedPowerupProcess(String name, Player parent)
	{
		super(name, parent, 2, 2, 0.5f, false);

		this.player = parent;
		
		stats = new SpeedPowerupStats(player.getStats());
		
		powerupTimer = new Timer(stats.time , false, 1);
	}
	
	public void start()
	{
		super.start();
		
		isCompleted = false;
		
		player.setTopSpeed(stats.topSpeed);
		player.setAcceleration(stats.acceleration);
		player.setBrakes(stats.groundDamping);

		powerupTimer.start();
	}
	
	public void stop()
	{
		super.stop();
		powerupTimer.stop();
		end();
	}
	
	public void pause()
	{
		super.pause();
		
		powerupTimer.pause();
	}
	
	public void resume()
	{
		super.resume();
		
		powerupTimer.resume();
	}
	
	public void finish()
	{
		super.finish();
		powerupTimer.finish();
		
		end();
	}

	private void end()
	{
		player.setTopSpeedToDefault();
		player.setAccelerationToDefault();
		player.setBrakesToDefault();
		player.setAirDampingToDefault();
		
		// If we are past top speed right now, we should cap it
		// TODO how to cap
		//if (player.getBody().getlinea)
		
		isCompleted = true;
	}

	@Override
	public void update(float dt)
	{
		if (powerupTimer.update(dt))
		{
			if (powerupTimer.isComplete())
			{
				end();
				return;
			}
		}

		if (isCompleted)
		{
			return;
		}
		
		super.update(dt);
	}

	@Override
	public void render(SpriteBatch sb)
	{
		if (isCompleted)
		{
			return;
		}
		
		super.render(sb);
	}
}
