package powerupProcesses;

import processes.AfterImageProcess;
import processes.AnimationDrawProcess;
import processes.Process;
import gameObjects.Timer;
import main.Game;
import states.Play;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Fixture;

import definitions.B2DVars;
import definitions.GameStats;
import entities.B2DSprite;
import entities.Player;

public class FlyingPressPowerup extends Process
{
	private static final float BAR_BRAKES_SPEED = 0.95f;

	private static final float SHAKE_LENGTH = 0.9f;

	private static final int SMASH_SHAKE_SIZE = 120;

	private static final long CHARGE_EFFECT_TIME = 2000;

	private static final int SMASH_SPEED = -10;

	protected static final float ALPHA_SCALE = 0.8f;

	private static final int AFTER_IMAGES_AMOUNT = 20;

	private static final long BAR_BRAKES_DELAY_TIME = 50;
	
	private AnimationDrawProcess ChargeEffect;
	private AnimationDrawProcess ChargeFailEffect;
	private AnimationDrawProcess AirSmashEffect;
	private AfterImageProcess SmashEffect;
	private AnimationDrawProcess AfterSmashEffect;
	
	Process curRunningProcess;
	
	private TextureRegion[] chargeFailTextures;
	private TextureRegion[] airSmashEffectTextures;
	private TextureRegion[] afterSmashTextures;
	private Timer barBrakesTimer;
	
	public FlyingPressPowerup(String name, B2DSprite parent, TextureRegion[] chargeTextures,
															 TextureRegion[] chargeFailTextures)
	{
		super(name, parent, false);
		
		curRunningProcess = null;
		
		//TextureRegion[] sprites = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		Texture tex = Game.res.getTexture("powerup_creation");
		airSmashEffectTextures = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		
		tex = Game.res.getTexture("powerup_creation");
		afterSmashTextures = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		
		// Initializing all processes
		ChargeEffect = new AnimationDrawProcess("ChargeEffect", chargeTextures, 1/12f, true, 1, parent, CHARGE_EFFECT_TIME);
		AirSmashEffect = null;
		
		this.chargeFailTextures = chargeFailTextures;
	}

	public void start()
	{
		super.start();
		
		// Start charging
		ChargeEffect.start();
		curRunningProcess = ChargeEffect;
		
		barBrakesTimer = null;
	}

	@Override
	public void update(float dt)
	{
		// The charge went off when we were on the ground
		if (ChargeFailEffect != null)
		{
			// Waiting for the turn off animation to end
			if (ChargeFailEffect.isRunning())
				ChargeFailEffect.update(dt);
			// It ended, powerup is over
			else
				finish();
		}
		// If we are charging
		else if (ChargeEffect.isRunning())
		{
			ChargeEffect.update(dt);
		}
		// Just finished charging
		else if (AirSmashEffect == null)
		{
			// If we are touching anything, we failed
			if (((Player)parent).isTouchingAnything())             
			{
				ChargeFailEffect = new AnimationDrawProcess(name, chargeFailTextures, false, 1, (B2DSprite) parent);
				ChargeFailEffect.start();
				curRunningProcess = ChargeFailEffect;
				ChargeFailEffect.update(dt);
			}
			// Start smashin'
			else
			{
				AirSmashEffect = new AnimationDrawProcess("AirSmashEffect", airSmashEffectTextures, 1/12f, false, 1, (B2DSprite) parent);
				AirSmashEffect.start();
				curRunningProcess = AirSmashEffect;
				
				//Play.getInstance().GetGameCamera().switchToSpecificZoom(-0.1f, 1.2f);

				// Setting player in place for smash
				((Player)parent).setGodMode(true);
				//((B2DSprite)parent).getBody().setType(BodyType.KinematicBody);
				
			}
		}
		// In the middle of the effect of the coming smash
		else if (AirSmashEffect.isRunning())
		{
			AirSmashEffect.update(dt);
			
			((B2DSprite)parent).getBody().setLinearVelocity(0, 0);
			((B2DSprite)parent).getBody().setAngularVelocity(0);
			parent.setDrawingAngle(0);
			
			if (AirSmashEffect.isProcessComplete())
			{
				// Smashing down
				SmashEffect = new AfterImageProcess("SmashEffect", (B2DSprite) parent, 1, AFTER_IMAGES_AMOUNT, ALPHA_SCALE, false);
				SmashEffect.start();
				curRunningProcess = SmashEffect;
				
				((B2DSprite)parent).getBody().setLinearVelocity(0, SMASH_SPEED);
				((B2DSprite)parent).getBody().setAngularVelocity(0);
				((Player)parent).setLockedMovements(true);
				
				//Play.getInstance().GetGameCamera().switchToSpecificZoom(0.05f, 2.5f);
				
				// Smash
				((Player)parent).setIsSmashing(true);
			}
		}
		else if (SmashEffect.isRunning())
		{
			SmashEffect.update(dt);
			
			parent.setDrawingAngle(0);
			
			// Check if we hit someone
			if (((B2DSprite)parent).isTouchingAnything())
			{
				// Shakeeeeee
				Play.getInstance().GetGameCamera().applyShake(SMASH_SHAKE_SIZE, SHAKE_LENGTH);
				
				// If we touch the bar indirectly
				Fixture bar = ((B2DSprite)parent).getTouchedFixtureRecursively("bar-bar");
				
				if (bar != null)
				{
					System.out.println("HAPPENED");
					// The smash force is the force by level * proximity to bar's center * game's size scale (=bar's size scale)
					float smashForce = GameStats.airSmashPowerupForce[((Player)parent).getStats().airSmashPowerupLevel] *
														(Play.getInstance().getBar().getXPosition() - ((Player)parent).getXPosition()) *
																				B2DVars.GAME_SIZE;
					bar.getBody().applyTorque(smashForce, true);
					barBrakesTimer = new Timer(BAR_BRAKES_DELAY_TIME, false, 1);
					barBrakesTimer.start();
				}
				
				// Finished smash effect
				SmashEffect.finish();
			}
			
			if (SmashEffect.isProcessComplete())
			{
				// after smashing down
				AfterSmashEffect = new AnimationDrawProcess("AfterSmashEffect", afterSmashTextures, 1/12f, false, 1, (B2DSprite) parent);
				AfterSmashEffect.start();
				curRunningProcess = AfterSmashEffect;
			}
		}
		else if (AfterSmashEffect.isRunning())
		{
			((B2DSprite)parent).getBody().setLinearVelocity(0, 0);
			AfterSmashEffect.update(dt);
		}
		else if (AfterSmashEffect.isProcessComplete() &&
				(barBrakesTimer == null || barBrakesTimer.isComplete())) //&&
				//Play.getInstance().GetGameCamera().zoom == 3)
		{
			//Play.getInstance().GetGameCamera().switchToNormalZoom();
			finish();
		}
		
		if (barBrakesTimer != null && barBrakesTimer.getIsRunning())
		{
			barBrakesTimer.update(dt);

			if (barBrakesTimer.isComplete())
				Play.getInstance().getBar().breakMomentum(BAR_BRAKES_SPEED);
		}
	}

	@Override
	public void render(SpriteBatch sb)
	{
		if (isRunning())
		{
			curRunningProcess.render(sb);
		}
	}
	
	public void stop()
	{
		super.stop();
		
		if (ChargeEffect != null)
			ChargeEffect.stop();
		if (ChargeFailEffect != null)
			ChargeFailEffect.stop();
		if (AirSmashEffect != null)
			AirSmashEffect.stop();
		if (SmashEffect != null)
			SmashEffect.stop();
		if (AfterSmashEffect != null)
			AfterSmashEffect.stop();
		if (barBrakesTimer != null)
			barBrakesTimer.stop();

		end();
	}
	
	public void finish()
	{
		super.finish();
		
		if (ChargeEffect != null)
			ChargeEffect.finish();
		if (ChargeFailEffect != null)
			ChargeFailEffect.finish();
		if (AirSmashEffect != null)
			AirSmashEffect.finish();
		if (SmashEffect != null)
			SmashEffect.finish();
		if (AfterSmashEffect != null)
			AfterSmashEffect.finish();
		if (barBrakesTimer != null)
			barBrakesTimer.finish();
		
		end();
	}

	private void end()
	{
		((Player)parent).setGodMode(false);
		((Player)parent).setIsSmashing(false);
		((Player)parent).setLockedMovements(false);
		
		ChargeFailEffect = null;
		AirSmashEffect = null;
		SmashEffect = null;
		AfterSmashEffect = null;
		barBrakesTimer = null;
	}
}
