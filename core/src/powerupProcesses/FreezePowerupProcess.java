package powerupProcesses;

import gameObjects.Timer;

import java.util.ArrayList;
import java.util.Iterator;

import processes.TimedProcess;
import states.Play;
import main.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars;
import definitions.GameStats;
import entities.Player;
import entities.Weight;

public class FreezePowerupProcess extends TimedProcess
{
	private boolean thawing;

	public FreezePowerupProcess(String name, Player parent)
	{
		super(name,
			  GameStats.freezePowerupTime[parent.getStats().typeIndex][parent.getStats().freezePowerupLevel], 
			  1, parent, false);
		
		thawing = false;
	}
	
	public void start()
	{
		super.start();
	}
	
	public void onTimerStart()
	{
		Play p = Play.getInstance();
		
		p.freeze();

		Iterator<Weight> i = p.getWeights().iterator();
		
		while (i.hasNext())
		{
			Weight w = i.next();
			w.getProcessManager().addToActiveProcesses("Freeze");
			w.freeze();
		}
		
		p.getBar().getProcessManager().addToActiveProcesses("Freeze");
		p.getBar().freeze();
		
		p.getBackground().freeze();
	}
	
	public void stop()
	{
		super.stop();
		
		onTimeout();
	}
	
	protected void onTimeout()
	{
		Play p = Play.getInstance();
		
		p.thaw();
		
		Iterator<Weight> i = p.getWeights().iterator();
		
		while (i.hasNext())
		{
			Weight w = i.next();
			//w.removeFromActiveProcesses("Freeze");
			if (w.getProcessManager().isProcessActive("Freeze"))
				w.getProcessManager().getActiveProcess("Freeze").finish();
			w.thaw();
		}
		
		p.getBar().thaw();
		if (p.getBar().getProcessManager().isProcessActive("Freeze"))
			p.getBar().getProcessManager().getActiveProcess("Freeze").finish();
		
		p.getBackground().thaw();
		
		
		thawing = true;
	}

	public void pause()
	{
		super.pause();
	}
	
	public void resume()
	{
		super.resume();
	}
	
	public void finish()
	{
		super.finish();
		
		onTimeout();
	}

	@Override
	public void update(float dt)
	{
		super.update(dt);
		
		Play p = Play.getInstance();
		
		Iterator<Weight> i = p.getWeights().iterator();
		
		while (thawing && i.hasNext())
		{
			Weight w = i.next();
			
			if (!w.getProcessManager().getProcess("Freeze").isProcessComplete())
				return;
			
			w.getProcessManager().removeFromActiveProcesses("Freeze");
		}
		
		if (thawing && p.getBar().getProcessManager().getProcess("Freeze").isProcessComplete())
		{
			Play.getInstance().getBar().getProcessManager().removeFromActiveProcesses("Freeze");
			 isCompleted = true;
		}
	}

	@Override
	protected void onIterationComplete() {
		// TODO Auto-generated method stub
		
	}
}

