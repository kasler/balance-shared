package main;

import handlers.Atlas;
import handlers.Content;
import handlers.GameStateManager;
import handlers.MyInput;
import handlers.MyInputProcessor;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Game implements ApplicationListener {

	public static final String TITLE = "Block Bunny";
	public static final int V_WIDTH = 320;
	public static final int V_HEIGHT = 240;
	public static final int SCALE = 1;
	public static final int OBJECT_SCALE = 8;

	public static final float STEP = 1 / 60f;
	
	private float accum;

	private SpriteBatch sb;
	private OrthographicCamera hudCam;

	protected GameStateManager gsm;

	private boolean restartPlay;
	
	public static Content res;
	public static Atlas atlas;
	
	public void create()
	{
		//Texture.setEnforcePotImages(false);
		atlas = new Atlas();

		Gdx.input.setInputProcessor(new MyInputProcessor());

		res = new Content();

		res.loadTexture("res/images/bunny2.png", "bunny");
		res.loadTexture("res/images/weight2.png", "weight");
		res.loadTexture("res/images/weightf2.png", "weightf");


		res.loadTexture("res/images/powerup_creation.png", "powerup_creation");
		res.loadTexture("res/images/powerup_glow.png", "powerup_glow");
		//		res.loadTexture("res/images/powerup_moving_glow.png", "powerup_moving_glow");
		res.loadTexture("res/images/powerup_glow.png", "powerup_moving_glow");

		res.loadTexture("res/images/powerup_missile.png", "powerup_missile");
		res.loadTexture("res/images/powerup_speed.png", "powerup_speed");
		res.loadTexture("res/images/powerup_freeze.png", "powerup_freeze");

		res.loadTexture("res/images/bar2.png", "bar");
		res.loadTexture("res/images/barf2.png", "barf");
		res.loadTexture("res/images/bgs.png", "bgs");

		res.loadTexture("res/images/bgs3.png", "bgs");

		sb = new SpriteBatch(1000);
		//cam.setToOrtho(false, V_WIDTH * ZOOM, V_HEIGHT * ZOOM);

		hudCam = new OrthographicCamera();
		hudCam.setToOrtho(false, V_WIDTH * SCALE, V_HEIGHT * SCALE);
		
		restartPlay = false;
	}

	public void render()
	{
		accum += Gdx.graphics.getDeltaTime();
		while(accum >= STEP) 
		{
			accum -= STEP;
			gsm.update(STEP);
			MyInput.update();
		}
		gsm.render();
		
		if (restartPlay)
		{
			gsm.restartState();
			restartPlay = false;
		}
	}

	public void dispose() {

	}

	public SpriteBatch getSpriteBatch() { return sb; }
	public OrthographicCamera getHUDCamera() { return hudCam; }

	public void resize(int w, int h) {}
	public void pause() {}
	public void resume() {}

	public void restartPlay()
	{
		this.restartPlay = true;
	}
}
