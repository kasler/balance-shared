package handlers;

import java.util.Stack;

import main.Game;
import states.GameState;

public abstract class GameStateManager
{	
	private Game game;
	
	private Stack<GameState> gameStates;
	
	public static final int PLAY = 912837;
	
	public GameStateManager(Game game) {
		this.game = game;
		gameStates = new Stack<GameState>();
		pushState(PLAY);
	}
	
	public Game game() { return game; }
	
	public void update(float dt) {
		gameStates.peek().update(dt);
	}
	
	public void render() {
		gameStates.peek().render();
	}
	
	abstract protected GameState getState(int state);
	
	public void setState(int state) {
		popState();
		pushState(state);
	}
	
	public void pushState(int state) {
		gameStates.push(getState(state));
	}
	
	public void popState() {
		GameState g = gameStates.pop();
		g.dispose();
	}
	
	public void restartState()
	{
		gameStates.peek().restart();
	}
	
	public void restartState(int state)
	{
		if(state == PLAY)
			getState(state).restart();
	}

	public Game getGame()
	{
		return game;
	}
}