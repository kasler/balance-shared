package handlers;

import powerupProcesses.FlyingPressPowerup;
import states.Play;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import entities.Bar;
import entities.Player;
import entities.Powerup;
import entities.Weight;

public class MyContactListener implements ContactListener {
	
	private Array<Body> bodiesToRemove;
	protected World world;
	protected Contact c;
	private boolean d;
	private boolean ghostBarActivated;
	
	public MyContactListener(World pworld) {
		super();
		world = pworld;
	}

	protected void beginSquishyCollision(Fixture squishy, Fixture otherFX )
	{
		if (squishy.getBody().getUserData() instanceof Player )
		{
			((Player)(squishy.getBody().getUserData())).die();
		}
		
		if (otherFX.getBody().getUserData() instanceof Player )
		{
			((Player)(otherFX.getBody().getUserData())).die();
		}
	}
	
	protected void beginBarCollision(Fixture barFX, Fixture otherFx)
	{
		if (otherFx == null)
			return;
		
		Bar bar = ((Bar)barFX.getBody().getUserData());
		
		bar.addTouch(otherFx, false);
		
		/*if (otherFx.getUserData().equals("player"))
		{
			Player player = ((Player)(otherFx.getBody().getUserData()));
			
			// TODO Decide how much force we use when smashing down according to where on the bar we
			//		are smashing
			if (player.isSmashing())
			{
				//otherFx.getBody().setLinearVelocity(player.getLatestAirVel(), -800);
				otherFx.getBody().setLinearVelocity(player.getLatestAirVel(), 0);
				bar.getBody().applyForce(0, -100000,
										 c.getWorldManifold().getPoints()[0].x,
										 c.getWorldManifold().getPoints()[0].y,
										 true);
			}
			
			//c.getWorldManifold().
		}*/
	}
	
	protected void endBarCollision(Fixture barFX, Fixture otherFX)
	{
		if (otherFX == null)
			return;
		
		Bar bar = ((Bar)barFX.getBody().getUserData());
		
		bar.removeTouch(otherFX);
	}
	
	protected void beginFootCollision(Fixture footFx, Fixture otherFx)
	{
		if (footFx == null || otherFx == null)
			return;
		
		Player player = ((Player)(footFx.getBody().getUserData()));
			
		if (isWallSensor(otherFx))
		{
			if (player.isFixtureGroundable(otherFx))
				player.addTouch(otherFx, true);
			else
				player.addTouch(otherFx, false);
		}
		else if (otherFx.getUserData().equals("weight") || otherFx.getUserData().equals("inertiaGhost"))
		{
			player.addTouch(otherFx, true);
			//footFx.getBody().setLinearVelocity(player.getLatestAirVel(), 0);
		}
		else if (otherFx.getUserData().equals("bar"))
		{
			player.addTouch(otherFx, true);
		}
		// If we touched a powerup
		else if (otherFx.getUserData().equals("powerup"))
		{
			Powerup powerup = ((Powerup)(otherFx.getBody().getUserData()));
			
			player.obtainPowerup(powerup.getPowerupType());
			
			powerup.setObtained();
		}
		else if (otherFx.getUserData().equals("barGhost"))
		{
			player.addTouch(otherFx, true);
		}
	}
	
	protected void beginWeightCollision(Fixture weightFx, Fixture otherFx)
	{
		if (otherFx == null)
			return;
		
		Weight weight = ((Weight)(weightFx.getBody().getUserData()));

		if (otherFx.getUserData().equals("weight") ||
			otherFx.getUserData().equals("bar"))
		{
			weight.addTouch(otherFx, true);
			
			float hitSpeed = -1 * weightFx.getBody().getLinearVelocity().y;
			
			Play.getInstance().GetGameCamera().applyShakeByMass(weight.getMass(), hitSpeed);
			
			weight.getBody().setLinearVelocity(0, 0);
		}
		else
			weight.addTouch(otherFx, false);
	}
	
	private boolean isWallSensor(Fixture fix)
	{
		if (((String)fix.getUserData()).startsWith("weight_wall"))
			return true;
		return false;
	}

	protected void endWeightCollision(Fixture weightFx, Fixture otherFx)
	{
		if (weightFx == null)
			return;

		Weight weight = ((Weight)(weightFx.getBody().getUserData()));

		weight.removeTouch(otherFx);
	}
	
	// called when two fixtures start to collide
	public void beginContact(Contact c) {
		this.c = c;
		Fixture fa = c.getFixtureA();
		Fixture fb = c.getFixtureB();
		
		if(fa == null || fb == null) return;
		
		beginCollisions(fa, fb);
		beginCollisions(fb, fa);

		//beginPoerupCollision(fa, fb);
	}
	
	public void beginCollisions(Fixture collides, Fixture collidesWith)
	{
		if(collides.getUserData() != null)
		{
			if (collides.getUserData().equals("foot"))
			{
				beginFootCollision(collides, collidesWith);
			}
			else if (collides.getUserData().equals("weight") || isWallSensor(collides))
			{
				beginWeightCollision(collides, collidesWith);
			}
			else if (collides.getUserData().equals("bar"))
			{
				beginBarCollision(collides, collidesWith);
			}
			else if (collides.getUserData().equals("squish"))
			{
				beginSquishyCollision(collides, collidesWith);
			}
			
		}
	}

	public void endCollisions(Fixture collides, Fixture collidesWith)
	{
		if(collides.getUserData() != null)
		{
			if (collides.getUserData().equals("foot"))
			{
				endFootCollision(collides, collidesWith);
			}
			else if (collides.getUserData().equals("weight") || isWallSensor(collides))
			{
				endWeightCollision(collides, collidesWith);
			}
			if (collides.getUserData().equals("bar"))
			{
				endBarCollision(collides, collidesWith);
			}
		}
	}
	
	// called when two fixtures no longer collide
	public void endContact(Contact c) {
		
		Fixture fa = c.getFixtureA();
		Fixture fb = c.getFixtureB();
		
		if(fa == null || fb == null) return;
		
		endCollisions(fa, fb);
		endCollisions(fb, fa);
	}
	
	protected void endFootCollision(Fixture footFx, Fixture otherFx)
	{
		if (footFx != null && otherFx != null)
		{
			if (isWallSensor(otherFx) ||
				otherFx.getUserData().equals("weight") ||
				otherFx.getUserData().equals("inertiaGhost") ||
				otherFx.getUserData().equals("bar") ||
				otherFx.getUserData().equals("barGhost"))
			{
				Player player = ((Player)(footFx.getBody().getUserData())); 
				player.removeTouch(otherFx);
			}
			
		}
	}

	public Array<Body> getBodiesToRemove() { return bodiesToRemove; }
	
	public void preSolve(Contact c, Manifold m)
	{
	}

	public void postSolve(Contact c, ContactImpulse ci)
	{
		if (c.getFixtureA() == null || c.getFixtureB() == null)
			return;
		
		Player player = null;
		String fa = ((String)c.getFixtureA().getUserData());
		String fb = ((String)c.getFixtureB().getUserData());
			
		if (ci.getNormalImpulses()[0] > 20 && (fa.equals("inertiaGhost") || fb.equals("inertiaGhost")))
		{
			Fixture weightFx = null;
			
			System.out.println("NORMAL: " + ci.getNormalImpulses()[0]);
			if (fa.equals("player"))
			{
				player = (Player)c.getFixtureA().getBody().getUserData();
				weightFx = c.getFixtureB();
			}
			else if (fb.equals("foot") || fb.equals("player"))
			{
				player = (Player)c.getFixtureB().getBody().getUserData();
				weightFx = c.getFixtureA();
			}
			
			if (player != null && player.getTouchingFixtures().containsKey("bar-bar"))
			{
				// TODO GHOST BAR
				// we need to pass the player to ghost bar mode (wake up ghost bar and change masks) with the pressuring weight until he leaves the weight
				player.applyGhostBarMode(weightFx);
				
				//player.die();
				System.out.println("!!!!!!!!!!!!!!!!!!!!!! " + ci.getNormalImpulses()[0]);
				Play.getInstance().getBar().revertToPrevAngVel();
				
				/*float angle = (float) (Math.PI + Math.atan(c.getWorldManifold().getNormal().y / c.getWorldManifold().getNormal().x));
				float x = (float) (ci.getNormalImpulses()[0] * Math.cos(angle));
				float y = (float) (ci.getNormalImpulses()[0] * Math.sin(angle));
				System.out.println("FORCE x:" + x + " y:" + y);
				player.getBody().applyForceToCenter(0, 0 - ci.getNormalImpulses()[0], true);*/
			}
		}
	}
}