package handlers;

import java.util.HashMap;

import states.Play;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class Atlas
{
	private static final String defaultPath = "res/atlases/";
	
	private TextureAtlas atlasSprites;

	private HashMap<String, TextureRegion> textureRegions;
	private HashMap<String, TextureRegion[]> textureAnimations;

	public Atlas()
	{
		this(null);
	}
	
	public Atlas(String path)
	{
		textureRegions = new HashMap<String, TextureRegion>();
		textureAnimations = new HashMap<String, TextureRegion[]>();
		
		if (path == null)
			path = defaultPath;
		
		atlasSprites = new TextureAtlas(path + "sprites.atlas");
		
		loadAllTextureRegionsAndAnimations();
	}
	
	private void loadAllTextureRegionsAndAnimations()
	{
		textureAnimations.put("bunny", (TextureRegion[]) atlasSprites.findRegions("bunny").toArray(AtlasRegion.class));
		textureAnimations.put("robot", (TextureRegion[]) atlasSprites.findRegions("robot").toArray(AtlasRegion.class));
	}
	
	public TextureRegion[] getTextureAnimation(String key)
	{
		TextureRegion[] s = textureAnimations.get(key);
		TextureRegion[] c = new TextureRegion[s.length];
		for (int i = 0; i < s.length; i++)
			c[i] = new TextureRegion(s[i]);
		return c;
	}
	
	public TextureRegion getTextureAnimationFrame(String key, int index)
	{
		return textureAnimations.get(key)[index];
	}

	public TextureRegion getTextureRegion(String key)
	{
		return textureRegions.get(key);
	}

	/*public void loadTextureRegions(String atlasRegions, String key)
	{
		TextureRegion tex = new TextureRegion(atlasSprites );
		textureRegions.put(key, tex);
	}*/
}