package handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import processes.FrameDrawProcess;
import processes.Process;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ProcessManager
{
	protected HashMap<String, Process> processesMap;
	protected HashMap<String, Process> activeProcessesMap;
	private List<Process> activeProcessesList;
	
	public ProcessManager()
	{
		processesMap = new HashMap<String, Process>();
		activeProcessesMap = new HashMap<String, Process>();
		activeProcessesList = new LinkedList<Process>();
	}
	
	public void update(float dt)
	{
		java.util.Iterator<Process> itr = activeProcessesList.iterator();
		Process curProcess = null;
		
		//iterate through HashMap values iterator
	    while(itr.hasNext())
	    {
	    	curProcess = itr.next();
	    	curProcess.update(dt);
	    	
	    	// Lose the ones completed
	    	if (curProcess.isProcessComplete())
	    	{
	    		itr.remove();
	    		activeProcessesMap.remove(curProcess.getName());
	    	}
	    }
	}

	public void render(SpriteBatch sb)
	{
		java.util.Iterator<Process> itr = activeProcessesList.iterator();
		 
		//iterate through HashMap values iterator
	    while(itr.hasNext())
	    {
	    	Process cur = itr.next(); 
	    	cur.render(sb);
	    	
	    	if (cur.isDebugParent() && cur instanceof FrameDrawProcess)
	    	{
	    		((FrameDrawProcess)cur).serverRender(sb);
	    		((FrameDrawProcess)cur).pastRender(sb);
	    	}
	    }
	}
	
	public void addProcess(String name, Process process)
	{
		processesMap.put(name, process);
	}
	
	public void addToActiveProcesses(String name)
	{
		addToActiveProcesses(name, true);
	}
	
	public void addToActiveProcessesBack(String name)
	{
		addToActiveProcessesBack(name, true);
	}
	
	public void addToActiveProcesses(String name, boolean andStartIt)
	{
		if (processesMap.containsKey(name) && !activeProcessesMap.containsKey(name))
		{
			Process p = processesMap.get(name);
			activeProcessesMap.put(name, p);
			activeProcessesList.add(p);
			
			if (andStartIt)
			{
				p.start();
			}
		}
	}
	
	public void addToActiveProcessesBack(String name, boolean andStartIt)
	{
		if (processesMap.containsKey(name) && !activeProcessesMap.containsKey(name))
		{
			Process p = processesMap.get(name);
			
			activeProcessesMap.put(name, p);
			activeProcessesList.add(0, p);
			
			if (andStartIt)
			{
				p.start();
			}
		}
	}
	
	public void removeFromActiveProcesses(String name, boolean andStopIt)
	{
		Process p = activeProcessesMap.remove(name);
		
		if (p != null)
		{
			Iterator<Process> itr = activeProcessesList.iterator();
			while (itr.hasNext())
			{
				Process cur = itr.next();
				
				if (cur == p)
				{
					activeProcessesList.remove(cur);
					break;
				}
			}
			
			if (andStopIt)
				p.stop();
		}
	}
	
	public void removeFromActiveProcesses(String name)
	{
		removeFromActiveProcesses(name, true);
	}
	
	public boolean isProcessActive(String name)
	{
		return activeProcessesMap.containsKey(name);
	}
	
	public Process getActiveProcess(String name)
	{
		return activeProcessesMap.get(name);
	}
	
	public Process getProcess(String name)
	{
		return processesMap.get(name);
	}
	
	public boolean isProcessExists(String name)
	{
		return processesMap.containsKey(name);
	}
	
	public void removeAllActiveProcesses()
	{
		activeProcessesMap.clear();
		activeProcessesList.clear();
	}
	
	public boolean areProcessesActive()
	{
		System.out.println("TRYING");
		
		return !activeProcessesMap.isEmpty();
	}
	
	public void startProcess(String name)
	{
		if (processesMap.containsKey(name))
		{
			processesMap.get(name).start();
		}
	}

	public void finishAllActiveProcesses()
	{
		//obtain an Iterator for Collection
	    java.util.Iterator<Process> itr = activeProcessesList.iterator();
	   
	    //iterate through HashMap values iterator
	    while(itr.hasNext())
	    {
	    	itr.next().finish();
	    }
	}

	public void clearAll()
	{
		processesMap.clear();
		activeProcessesMap.clear();
	}
	
	public Iterator<Process> getActiveProcessesIterator()
	{
		return activeProcessesList.iterator();
	}

	public void removeFromActiveProcessesByIterator(String name, Iterator<Process> itr, boolean andStopIt)
	{
		Process p = activeProcessesMap.remove(name);
		
		itr.remove();
		
		if (andStopIt)
			p.stop();
	}
}
