package handlers;

public class MyInput {
	
	public static boolean[] keys;
	public static boolean[] pkeys;
	
	public static final int NUM_KEYS = 10;
	
	public static final int SPACE_BTN = 0;
	public static final int RIGHT_BTN = 1;
	public static final int LEFT_BTN = 2;
	public static final int W_BTN = 3;
	public static final int S_BTN = 4;
	public static final int E_BTN = 5;
	public static final int Q_BTN = 6;
	public static final int CTRL_BTN = 7;
	public static final int R_BTN = 8;
	public static final int ESCAPE_BTN = 9;
	

	static {
		keys = new boolean[NUM_KEYS];
		pkeys = new boolean[NUM_KEYS];
	}
	
	public static void update() {
		for(int i = 0; i < NUM_KEYS; i++) {
			pkeys[i] = keys[i];
		}
	}
	
	public static void setKey(int i, boolean b) { keys[i] = b; }
	public static boolean isDown(int i) { return keys[i]; }
	public static boolean isPressed(int i) { return keys[i] && !pkeys[i]; }
	
}
















