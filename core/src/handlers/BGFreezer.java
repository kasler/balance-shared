package handlers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import entities.SpriteFreezer;

public class BGFreezer extends SpriteFreezer
{
	private boolean darkening;

	public BGFreezer()
	{
		super();
		
		darkening = true;
	}
	
	public void renderFreezeStart(SpriteBatch sb)
	{
		if (isFrozen)
		{
			// Don't make it too blue
			if (isFreezing)
			{
				bluePercent += 0.05;
				
				if (bluePercent >= 1.4f)
				{
					isFreezing = false;
				}
			}
			else if (isThawing)
			{
				bluePercent -= 0.05;
				
				if (bluePercent <= 1f)
				{
					isThawing = false;
					isFrozen = false;
				}
			}
			else
			{
				if (darkening)
				{
					bluePercent -= 0.005f;
					
					if (bluePercent <= 1.2f)
					{
						darkening = false;
					}
				}
				else
				{
					bluePercent += 0.005f;
					
					if (bluePercent >= 1.5f)
					{
						darkening = true;
					}
				}
			}
		
			// Adding the blue percent
			Color color = sb.getColor();
			
			originalColor = color.cpy();
			
			
			color.b *= bluePercent;
			//color.r /= bluePercent*2;
			//color.g /= bluePercent*2;
			
			
			if (color.b > 2)
			{
				color.b = 2;
			}
			
			/*if (color.g < 0.1f)
			{
				color.g = 0.1f;
			}
			
			if (color.r < 0.1f)
			{
				color.r = 0.1f;
			}*/
			
			//color.a *= 0.9;

			color.set(0.8f, 0.8f, 1f, color.a);
			
			
			//sb.setColor(color);
		}
	}
}
