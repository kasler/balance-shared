package handlers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

public class MyInputProcessor extends InputAdapter {

	public MyInputProcessor()
	{
	}
	
	public boolean keyDown(int k) {
		if(k == Keys.LEFT) {
			MyInput.setKey(MyInput.LEFT_BTN, true);
		}
		if(k == Keys.RIGHT) {
			MyInput.setKey(MyInput.RIGHT_BTN, true);
		}
		if (k == Keys.SPACE)
		{
			MyInput.setKey(MyInput.SPACE_BTN, true);
		}
		if (k == Keys.W)
		{
			MyInput.setKey(MyInput.W_BTN, true);
		}
		if (k == Keys.S)
		{
			MyInput.setKey(MyInput.S_BTN, true);
		}
		if (k == Keys.E)
		{
			MyInput.setKey(MyInput.E_BTN, true);
		}
		if (k == Keys.Q)
		{
			MyInput.setKey(MyInput.Q_BTN, true);
		}
		if (k == Keys.CONTROL_LEFT)
		{
			MyInput.setKey(MyInput.CTRL_BTN, true);
		}
		if (k == Keys.ESCAPE)
		{
			MyInput.setKey(MyInput.ESCAPE_BTN, true);
		}
		if (k == Keys.R)
		{
			MyInput.setKey(MyInput.R_BTN, true);
		}
		return true;
	}
	
	public boolean keyUp(int k) {
		if(k == Keys.LEFT) {
			MyInput.setKey(MyInput.LEFT_BTN, false);
		}
		if(k == Keys.RIGHT) {
			MyInput.setKey(MyInput.RIGHT_BTN, false);
		}
		if (k == Keys.SPACE)
		{
			MyInput.setKey(MyInput.SPACE_BTN, false);
		}
		if (k == Keys.W)
		{
			MyInput.setKey(MyInput.W_BTN, false);
		}
		if (k == Keys.S)
		{
			MyInput.setKey(MyInput.S_BTN, false);
		}
		if (k == Keys.E)
		{
			MyInput.setKey(MyInput.E_BTN, false);
		}
		if (k == Keys.Q)
		{
			MyInput.setKey(MyInput.Q_BTN, false);
		}
		if (k == Keys.CONTROL_LEFT)
		{
			MyInput.setKey(MyInput.CTRL_BTN, false);
		}
		if (k == Keys.ESCAPE)
		{
			MyInput.setKey(MyInput.ESCAPE_BTN, false);
		}
		if (k == Keys.R)
		{
			MyInput.setKey(MyInput.R_BTN, false);
		}
		return true;
	}
	
}
