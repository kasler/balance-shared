package network;

import java.nio.ByteBuffer;


public class Header implements Serialize
{

	private static final long serialVersionUID = 1L;
	
	public MessageType type;
	public int messageLength;
	public ByteBuffer buffer;
	
	public enum MessageType
	{
		KEY_PRESS,
		UPDATE,
		TEST;
		
		public char toChar()
		{
			int ord = ordinal();
			char charOrd = (char) ((char) ord + '0');
			return charOrd;  
		}
	}
	
	public Header(MessageType type, int messageLength)
	{
		this.type = type;
		this.messageLength = messageLength;
	}

	@Override
	public ByteBuffer serializeMessage()
	{
		buffer = ByteBuffer.allocate(NetworkCons.getHeaderLength());
		buffer.putInt(messageLength);
		buffer.putChar(this.type.toChar());
		return buffer;
	}
	
	
}
