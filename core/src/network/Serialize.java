package network;

import java.nio.ByteBuffer;

public interface Serialize
{
	public ByteBuffer serializeMessage();
}
