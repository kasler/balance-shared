package network;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import network.Header.MessageType;
import network.messages.BalanceMessage;
import network.messages.TestMessage;
import network.messages.UpdateMessage;


public class DeserializeWomanager
{
	private interface MessageFactory {
	    BalanceMessage create(ByteBuffer arr);
	}
	private static final Map<MessageType,MessageFactory> factoryMap =
	    Collections.unmodifiableMap(new HashMap<MessageType,MessageFactory>() {/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
	        put(MessageType.TEST, new MessageFactory() { public BalanceMessage create(ByteBuffer arr) { return new TestMessage(arr); }});
	        put(MessageType.UPDATE, new MessageFactory() { public BalanceMessage create(ByteBuffer arr) { return new UpdateMessage(arr); }});
	    }});

	private static BalanceMessage createMessage(MessageType type, ByteBuffer message) {
	    MessageFactory factory = factoryMap.get(type);
	    if (factory == null) {
	        throw new RuntimeException("No such message type : " + type);
	    }
	    return factory.create(message);
	}
	
	public static BalanceMessage deserialize(ByteBuffer headerBuff, ByteBuffer messageBuffer)
	{
		int length = headerBuff.getInt();
		char charType = headerBuff.getChar();
		int intType = (int) charType - '0';
		
		MessageType type = MessageType.values()[intType];
		BalanceMessage message = createMessage(type, messageBuffer);
		
		return message;
	}


}
