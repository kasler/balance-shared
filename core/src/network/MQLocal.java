package network;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import network.messages.BalanceMessage;

import com.badlogic.gdx.utils.TimeUtils;

public class MQLocal
{
	private int i;
	private float pingMS;
	private Queue<BalanceMessage> messageQueue;

	public MQLocal(float pingMS)
	{
		i = 0;
		this.pingMS = pingMS;
		this.messageQueue = new LinkedList<BalanceMessage>(); 
	}
	
	public void send(BalanceMessage msg)
	{
		msg.i = i;
		this.messageQueue.add(msg);
		++i;
	}
	
	public BalanceMessage recv()
	{
		BalanceMessage cur = null;
		
		if (!this.messageQueue.isEmpty() &&
			TimeUtils.timeSinceMillis(this.messageQueue.peek().time) > pingMS)
		{
			cur = this.messageQueue.poll();
		}
		
		return cur;
	}
}
