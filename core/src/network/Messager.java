package network;

import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentLinkedQueue;

import network.messages.BalanceMessage;

public class Messager
{
	private int i;
	private ConcurrentLinkedQueue<BalanceMessage> queue;
	private long delay;

	public Messager(long delayMS)
	{	
		queue = new ConcurrentLinkedQueue<BalanceMessage>();
		this.delay = delayMS;
		i = 0;
	}
	
	
	public void add(ByteBuffer header, ByteBuffer msg)
	{
		BalanceMessage bm = DeserializeWomanager.deserialize(header, msg);
		System.out.println("Added to queue : " + bm.toString());
		queue.add(bm);
	}
	
	public BalanceMessage recv()
	{
		if (!this.queue.isEmpty()) //&&
				//TimeUtils.timeSinceMillis(this.queue.peek().time) > delay)
		{
			//return new BalanceMessage(queue.poll());
		}
		
		return null;
	}
}