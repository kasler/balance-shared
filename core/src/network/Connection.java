package network;

import network.messages.BalanceMessage;

public abstract class Connection
{
	protected Messager messager;
	
	public Connection(Messager messager)
	{
		this.messager = messager;
	}
	
	public abstract boolean connectUdp();
	public abstract void connectTcp();
	public abstract void sendMessage(String message);
	public abstract void sendMessage(byte[] message);
	public abstract void disconnect();
	public abstract void sendMessage(BalanceMessage message);
	
	
}
