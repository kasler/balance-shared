package network.messages;

import java.nio.ByteBuffer;

import network.Deserialize;
import network.Header;
import network.Serialize;

import com.badlogic.gdx.utils.TimeUtils;

public abstract class BalanceMessage implements Serialize, Deserialize
{
	public long time;
	public int i;
	public ByteBuffer message;
	public Header header;
	
	public BalanceMessage()
	{
		i = 0;
		this.time = TimeUtils.millis();
	}
	
	
	public BalanceMessage(byte[] msg)
	{/*
		this.i = bytearrayToInt(msg, 0);
		this.posX = bytearrayToInt(msg, 4);
		this.posY = bytearrayToInt(msg, 8);
		this.velX = bytearrayToInt(msg, 12);
		this.velY = bytearrayToInt(msg, 16);
		this.angVel = bytearrayToInt(msg, 20);
		this.time = this.i = bytearrayToInt(msg, 24);*/
	}
	
	private int bytearrayToInt(byte[] msg, int startIndex)
	{/*
		return (msg[startIndex++]<<24)&0xff000000|
				(msg[startIndex++]<<16)&0x00ff0000|
				(msg[startIndex++]<< 8)&0x0000ff00|
				(msg[startIndex++]<< 0)&0x000000ff;*/
		return 0;
	}
	
	private void intToBytearray(int n, int startIndex, byte[] b)
	{/*
		for (int i = 0; i < 4; i++)
		    b[startIndex++] = (byte)(n >>> (i * 8));*/
	}

	
}
