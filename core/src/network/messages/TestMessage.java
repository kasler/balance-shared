package network.messages;

import java.nio.ByteBuffer;

import network.Header;
import network.NetworkCons;
import network.Header.MessageType;

public class TestMessage extends BalanceMessage
{
	public String mes = "TestMessage";
	
	public TestMessage()
	{
		super();
	}
	
	public TestMessage(ByteBuffer arr)
	{
		super();
		
		message = arr;
		deserialize();
	}

	@Override
	public ByteBuffer serializeMessage()
	{
		int strLength = mes.length();
		
		message = ByteBuffer.allocate(strLength);
		message.put(mes.getBytes());
		header = new Header(MessageType.TEST, strLength);
		ByteBuffer headerBuf = header.serializeMessage();
		ByteBuffer wholeMessage = ByteBuffer.allocate(strLength + NetworkCons.getHeaderLength());
		wholeMessage.put(headerBuf.array());
		wholeMessage.put(message.array());
		return wholeMessage;
	}

	@Override
	public void deserialize()
	{
		mes = new String(message.array());
	}
	
	@Override public String toString()
	{
		return mes;
	}
	

}
