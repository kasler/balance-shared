package network.messages;

import java.nio.ByteBuffer;

import network.Header;
import network.Header.MessageType;
import network.NetworkCons;

public class UpdateMessage extends BalanceMessage
{
	public static final int NUM_BOOL_VARS = 3;
	public static final int NUM_FLOAT_VARS = 9;
	public float posX;
	public float posY;
	public float velX;
	public float velY;
	public float angVel;
	public float angle;
	public float linDamp;
	public float angDamp;
	public float step;
	public boolean isLeftPressed;
	public boolean isRightPressed;
	public boolean isJumpPressed;
	
	
	
	public UpdateMessage(float posX, float posY, float velX, float velY, float angVel, float angle
			, float linDamp, float angDamp, float step, boolean isLeft, boolean isRight, boolean isJump)
	{
		super();
		
		this.posX = posX;
		this.posY = posY;
		this.velX = velX;
		this.velY = velY;
		this.angVel = angVel;
		this.angle = angle;
		this.linDamp = linDamp;
		this.angDamp = angDamp;
		this.step = step;
		this.isLeftPressed = isLeft;
		this.isRightPressed = isRight;
		this.isJumpPressed = isJump;
	}

	
	public UpdateMessage(ByteBuffer arr)
	{
		super();
		
		message = arr;
		deserialize();
		
	}

	@Override
	public ByteBuffer serializeMessage()
	{
		// boolean = 1 byte, float = 4 bytes
		
		int messageLength = NUM_BOOL_VARS * NetworkCons.BOOL_SIZE + NUM_FLOAT_VARS * NetworkCons.FLOAT_SIZE;
		this.header = new Header(MessageType.UPDATE, messageLength);
		ByteBuffer headerBuff = this.header.serializeMessage();
		
		message = ByteBuffer.allocate(messageLength);
		message.putFloat(this.posX);
		message.putFloat(this.posY);
		message.putFloat(this.velX);
		message.putFloat(this.velY);
		message.putFloat(this.angVel);
		message.putFloat(this.angle);
		message.putFloat(this.linDamp);
		message.putFloat(this.angDamp);
		message.putFloat(this.step);
		message.put((byte) ((boolean) isLeftPressed ? 1 : 0));
		message.put((byte) ((boolean) isRightPressed ? 1 : 0));
		message.put((byte) ((boolean) isJumpPressed ? 1 : 0));
		
		ByteBuffer wholeBuff = ByteBuffer.allocate(NetworkCons.getHeaderLength() + messageLength);
		wholeBuff.put(headerBuff.array());
		wholeBuff.put(message.array());
		
		return wholeBuff;
	}

	@Override
	public void deserialize()
	{
		this.posX = message.getFloat();
		this.posY = message.getFloat();
		this.velX = message.getFloat();
		this.velY = message.getFloat();
		this.angVel = message.getFloat();
		this.angle = message.getFloat();
		this.linDamp = message.getFloat();
		this.angDamp = message.getFloat();
		this.step = message.getFloat();
		byte isLeftByte = message.get();
		this.isLeftPressed = isLeftByte == 1 ? true : false;
		byte isRightByte = message.get();
		this.isRightPressed = isRightByte == 1 ? true : false;
		byte isJumpByte = message.get();
		this.isJumpPressed = isJumpByte == 1 ? true : false;
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof UpdateMessage))return false;
	    UpdateMessage otherMyClass = (UpdateMessage)other;
	    
	    return this.posX == otherMyClass.posX && this.posY == otherMyClass.posY && this.velX == otherMyClass.velX
	    		&& this.velY == otherMyClass.velY && this.angVel == otherMyClass.angVel && this.angle == otherMyClass.angle
	    		&& this.linDamp == otherMyClass.linDamp && this.angDamp == otherMyClass.angDamp && this.step == otherMyClass.step 
	    		&& this.isLeftPressed == otherMyClass.isLeftPressed && this.isRightPressed == otherMyClass.isRightPressed 
	    		&& this.isJumpPressed == otherMyClass.isJumpPressed;
	}
	
	@Override 
	public String toString()
	{
		return "posx : " + this.posX + "posy : " + this.posY + "time : " + this.time;
	}
}