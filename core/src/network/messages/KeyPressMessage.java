package network.messages;

import java.nio.ByteBuffer;

public class KeyPressMessage extends BalanceMessage
{
	public boolean isLeftPressed;
	public boolean isRightPressed;
	public boolean isJumpPressed;
	
	public KeyPressMessage()
	{
		super();
	}
	
	public KeyPressMessage(boolean isLeftPressed, boolean isRightPressed, boolean isJumpPressed)
	{
		super();
		
		this.isLeftPressed = isLeftPressed;
		this.isRightPressed = isRightPressed;
		this.isJumpPressed = isJumpPressed;
	}

	@Override
	public ByteBuffer serializeMessage()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deserialize()
	{
		// TODO Auto-generated method stub
		
	}
}