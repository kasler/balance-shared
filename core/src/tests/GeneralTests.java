package tests;

import java.nio.ByteBuffer;

import network.DeserializeWomanager;
import network.NetworkCons;
import network.messages.BalanceMessage;
import network.messages.UpdateMessage;

import org.junit.Test;

public class GeneralTests
{

	@Test
	public void serializeTest()
	{
		BalanceMessage update = new UpdateMessage(11.3f, 121.12f, 45, 11.1121f, 70, 81, 8, 
				79, 18, true, false, true);
		ByteBuffer wholeMessage = update.serializeMessage();
		ByteBuffer message = update.message;
		ByteBuffer header = update.header.buffer;
		
		int messageSize = UpdateMessage.NUM_BOOL_VARS * NetworkCons.BOOL_SIZE + UpdateMessage.NUM_FLOAT_VARS * NetworkCons.FLOAT_SIZE;
		int headerSize = NetworkCons.getHeaderLength();
		int wholeMessageSize = messageSize + headerSize;
		
		assert(headerSize == header.array().length);
		assert(messageSize == message.array().length);
		assert(wholeMessageSize == wholeMessage.array().length);
		
		// deserialize 
		header.flip();
		message.flip();
		UpdateMessage deserialUpdate = (UpdateMessage) DeserializeWomanager.deserialize(header, message);
		assert(update.equals(deserialUpdate));
		
	}

}
