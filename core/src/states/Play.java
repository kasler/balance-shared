package states;

import static definitions.B2DVars.GRAVITY;
import static definitions.B2DVars.PPM;
import gameObjects.Background;
import gameObjects.Legend;
import gameObjects.PlayerOrthographicCamera;
import handlers.GameStateManager;
import handlers.MyContactListener;
import handlers.MyInput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import main.Game;
import network.Connection;
import network.MQLocal;
import network.Messager;
import network.NetworkCons;
import network.messages.BalanceMessage;
import network.messages.KeyPressMessage;
import network.messages.UpdateMessage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import definitions.B2DVars;
import definitions.B2DVars.FACING_SIDE;
import definitions.B2DVars.POWERUP_MOVEMENT;
import definitions.B2DVars.POWERUP_TYPE;
import definitions.B2DVars.PlayerType;
import definitions.PlayerStats;
import entities.B2DSprite;
import entities.Bar;
import entities.FluffyPlayer;
import entities.Freezable;
import entities.HUD;
import entities.Player;
import entities.Powerup;
import entities.RobotPlayer;
import entities.Weight;


public abstract class Play extends GameState implements Freezable{
	
	private static final long UPDATE_CYCLE = 3000;

	private static final boolean ROBOT = false;

	private boolean debug = true;
	
	private World world;
	private Box2DDebugRenderer b2dr;

	private OrthographicCamera b2dCam;
	private MyContactListener cl;
	
	private Player ourPlayer;
	private HashMap<String, B2DSprite> Entities;
	private List<Player> Players;
	private List<Weight> Weights;
	private List<Powerup> powerups;
	private Bar bar;
	private Background background;
	private FPSLogger logger;
	private int id;
	private Legend legend;

	private boolean isFrozen;
	private boolean isFreezing;
	private boolean isThawing;

	private float freezeDelta;

	private float bgFreezeDelta;

	private boolean bgFreezeDarkening;

	private HUD hud;

	private float mapHorizontalLen;
	private float mapVerticalLen;
	private float gameSize;
	
	PlayerStats ourPlayerStats;

	protected MQLocal messager;

	protected static Play thePlay;
	
	private Connection connection;

	private World pastWorld;
	private Box2DDebugRenderer pastb2dr;
	private OrthographicCamera pastb2dCam;
	private MyContactListener pastCl;
	private Player pastPlayer;
	private Bar pastBar;

	private long lastUpdate;

	protected Play(GameStateManager gsm)
	{
		super(gsm);
		
		start();
	}
	
	public void restart()
	{
		start();
	}
	
	public void start()
	{
		logger = new FPSLogger();
		
		//messager = new Messager(connection, 0);
		messager = new MQLocal(200);
		
		logger = new FPSLogger();
		id = 1;
		gameSize = B2DVars.GAME_SIZE;
		
		isFreezing = false;
		isFrozen = false;
		isThawing = false;
		
		if (ROBOT)
			ourPlayerStats = new PlayerStats(PlayerType.ROBOT);
		else
			ourPlayerStats = new PlayerStats(PlayerType.FLUFFY);
		ourPlayerStats.loadStats();
		
		cam = new PlayerOrthographicCamera(gameSize);
		cam.setToOrtho(false, Game.V_WIDTH * Game.SCALE, Game.V_HEIGHT * Game.SCALE);
		cam.zoom = 1;
		
		// Setting map stuff
		mapHorizontalLen = 1400 * gameSize;
		mapVerticalLen = 1400 * gameSize;
		
		Players = new ArrayList<Player>();
		Weights = new ArrayList<Weight>();
		powerups = new ArrayList<Powerup>();
		Entities = new HashMap<String, B2DSprite>();
		
		// set up box2d stuff
		world = new World(new Vector2(0, GRAVITY), true);
		cl = new MyContactListener(world);
		world.setContactListener(cl);
		b2dr = new Box2DDebugRenderer();
		
		pastWorld = new World(new Vector2(0, GRAVITY), true);
		pastCl = new MyContactListener(pastWorld);
		pastWorld.setContactListener(pastCl);
		pastb2dr = new Box2DDebugRenderer();

		// create backgrounds
		Texture bg = Game.res.getTexture("bgs");
		
		TextureRegion[] bgs = new TextureRegion[3];
		
		bgs[0] = new TextureRegion(bg, 0, 0, 320, 240);
		bgs[1] = new TextureRegion(bg, 0, 241, 320, 239);
		bgs[2] = new TextureRegion(bg, 0, 481, 320, 543);
		
		background = new Background(this.id++, bgs, cam);

		CreateBar(0f);

		// create our player
		CreateOurPlayer("Serj");
		
		// Follow our player
		cam.follow(ourPlayer);
		
		// set up box2d cam
		b2dCam = new OrthographicCamera();
		b2dCam.setToOrtho(false, Game.V_WIDTH / PPM * gameSize*3, Game.V_HEIGHT / PPM * gameSize*3);
		
		b2dCam.position.x = 0f;
		b2dCam.position.y = 0f;
		b2dCam.update();
		
		pastb2dCam = new OrthographicCamera();
		pastb2dCam.setToOrtho(false, Game.V_WIDTH / PPM * gameSize*3, Game.V_HEIGHT / PPM * gameSize*3);
		
		pastb2dCam.position.x = 0f;
		pastb2dCam.position.y = 0f;
		pastb2dCam.update();

		// set up hud
		hud = new HUD(ourPlayer);
		
		// Set up legend
		//legend = new Legend(mapHorizontalLen, mapVerticalLen, gameSize, hudCam);
		legend = new Legend(bar.getWidth(), bar.getWidth(), gameSize, hudCam);
		
		lastUpdate = 0;
	}
	
	static public Play getInstance()
	{
		if (thePlay == null )
			return null;
		return thePlay;
	}
	
	public float getMapLeftBorder()
	{
		return 0 - mapHorizontalLen/2;
	}
	
	public float getMapRightBorder()
	{
		return mapHorizontalLen/2;
	}
	
	public float getMapBottomBorder()
	{
		return 0 - mapVerticalLen/2;
	}
	
	public float getMapTopBorder()
	{
		return mapHorizontalLen/2;
	}
	
	public boolean isWithinMapBorders(float x, float y)
	{
		return (x >= getMapLeftBorder() &&
				x <= getMapRightBorder() &&
				y >= getMapBottomBorder() &&
				y <= getMapTopBorder());
	}
	
	public void handleInput()
	{	
		// player jump
		if(MyInput.isPressed(MyInput.SPACE_BTN))
		{
			ourPlayer.setJump(true);
		}

		// Player moved right
		if (MyInput.isDown(MyInput.RIGHT_BTN))
		{
			ourPlayer.setMoveRight(true);
		}
		else
		{
			ourPlayer.setMoveRight(false);
		}
		
		// Player moved left
		if (MyInput.isDown(MyInput.LEFT_BTN))
		{
			ourPlayer.setMoveLeft(true);
		}
		else
		{
			ourPlayer.setMoveLeft(false);
		}
		
		// pressed w
		if (MyInput.isPressed(MyInput.W_BTN))
		{
    		float totalWeightTorque = 0;
    		float totalPlayerTorque = 0;
    		Vector2 axisPos = bar.getB2DPosition();
    		
			// Summing weight torques
			Iterator<Weight> witr = Weights.iterator();
		    while(witr.hasNext())
		    {
		    	Weight currWeight = witr.next();
		    	if (currWeight.getIsOnGround())
		    	{
		    		
		    		totalWeightTorque += ((currWeight.getB2DPosition().x - axisPos.x) * 
		    										currWeight.getMass()*GRAVITY);
		    	}
		    }
		    
		    
		    // Summing weight torques
		 	Iterator<Player> pitr = Players.iterator();
		 	while(pitr.hasNext())
		 	{
		 		Player currPlayer = pitr.next();
		 		
		 		if (currPlayer.getIsOnGround())
		 		{
		 			totalPlayerTorque += ((currPlayer.getB2DPosition().x - axisPos.x) * 
		 										   currPlayer.getMass()*GRAVITY);
		 		}
		 	}
		 	
		 	if (totalPlayerTorque - totalWeightTorque > 0)
		 	{
		 		System.out.println(bar.getLeftEdgePoint().x + " - " + bar.getRightEdgePoint().x);
		 		//createWeight(300, 600, 1);
		 		createWeight(bar.getLeftEdgePoint().x + (int)(Math.random()*(mapHorizontalLen/2)), getMapTopBorder(), 1 + (int)(Math.random() * 3 * gameSize), 1);
		 	}
		 	else
		 	{
		 		createWeight(bar.getRightEdgePoint().x - (int)(Math.random()*(mapHorizontalLen/2)), getMapTopBorder(), 1 + (int)(Math.random() * 3 * gameSize), 1);
		 	}
		}
		
		if (MyInput.isPressed(MyInput.S_BTN))
		{
			AddPlayer("Amir");
		}
		
		if (MyInput.isPressed(MyInput.E_BTN))
		{
			if (Weights.size() > 0)
			{
				Weights.get(0).Destroy();
			}
		}
		if (MyInput.isPressed(MyInput.Q_BTN))
		{
			createPowerup();
		}
		if (MyInput.isPressed(MyInput.CTRL_BTN))
		{
			ourPlayer.usePowerup();
		}
		if (MyInput.isPressed(MyInput.R_BTN))
		{
			ourPlayer.die();
		}
		if (MyInput.isPressed(MyInput.ESCAPE_BTN))
		{
			gsm.getGame().restartPlay();
		}
	}
	
	public void update(float dt)
	{
		//logger.log();
		
		// Check input
		handleInput();
		
		// Send new update about our player to the server.
		// Sending before doing the curent dt update because movements are sent as facts, not as an already updated status of location,
		// so to match that, we send all kinds of information before this dt update, for consistency
		sendUpdateToServer();
		
		// Update our player
		ourPlayer.update(dt);
		
		// Update powerups
		Iterator<Powerup> pitr = powerups.iterator();
	    while(pitr.hasNext())
	    {
	    	Powerup p = pitr.next();
	    	p.update(dt);
	    	
	    	if (p.getShouldDelete())
	    	{
	    		p.delete();
	    		pitr.remove();
	    	}
	    }
	    
		// Update weights
		Iterator<Weight> witr = Weights.iterator();
	    while(witr.hasNext())
	    {
	    	Weight w = witr.next();
	    	w.update(dt);
	    	
	    	if (w.getShouldDelete())
	    	{
	    		w.delete();
	    		witr.remove();
	    	}
	    }
	    
	    // Update bar
	    bar.update(dt);
	    
	    pastBar.update(dt);
		
		// Update all other players
		Iterator<Player> itr = Players.iterator();
	    while(itr.hasNext())
	    {
	    	Player currPlayer = itr.next();
	    	
	    	if (!currPlayer.getName().equals("Serj"))
	    	{
		    	int n = 0 + (int)(Math.random() * 5);
		    	if (n == 0)
		    	{
		    		currPlayer.setMoveLeft(true);
		    	}
		    	else if (n == 1)
		    	{
		    		currPlayer.setMoveLeft(false);
		    	}
		    	else if (n == 2)
		    	{
		    		currPlayer.setMoveRight(true);
		    	}
		    	else if (n == 3)
		    	{
		    		currPlayer.setMoveRight(false);
		    	}
		    	else if (n == 4)
		    	{
		    		if (0 + (int)(Math.random() * 8) == 4)
		    		{
		    			currPlayer.setJump(true);
		    		}
		    	}
		    	else if (n == 5)
		    	{
		    		currPlayer.setJump(false);
		    	}
		    	
		    	currPlayer.update(dt);
	    	}
	    }
	    
	    // Updating shaders
	    updateEffects();
	    
	    // Updating b2dCam
	    //b2dCam.position.x = 0f;
		//b2dCam.position.y = 0f;
		//b2dCam.update();
	    
	    networkUpdate(dt);
	    
	    pastPlayer.update(dt);
		pastWorld.step(dt, 6, 2);
	    
	    //copyToRealWorld();
	    
	    // Update physics
	 	world.step(dt, 6, 2);
	}
	
	private void networkUpdate(float dt)
	{
		// Updating pastWorld - getting all information from the server that happened in the past
	    //long timeSinceUpdateInSteps = updateByServer()
		if (updateByServer())
	    {	
	    	pastPlayer.setPredictionMode(true);
	    	
	    	//System.out.println("RRR");
	    	for (int i = 0; i < 13; i++)
	    	{
	    		pastPlayer.update(dt);
	    		pastWorld.step(dt, 6, 2);
	    	}
	    	
	    	pastPlayer.setPredictionMode(false);
	    }
	}

	public void copyToRealWorld()
	{
		bar.getBody().setTransform(pastBar.getBody().getPosition(), pastBar.getBody().getAngle());
		ourPlayer.getBody().setTransform(pastPlayer.getBody().getPosition(), pastPlayer.getBody().getAngle());
	}
	
	private void sendUpdateToServer()
	{
		// We do this once in every 2 seconds
		if (TimeUtils.timeSinceMillis(lastUpdate) > UPDATE_CYCLE)
		{
			// Update Message been updated 
//			BalanceMessage msg = new UpdateMessage(ourPlayer.getBody().getPosition().x,
//													ourPlayer.getBody().getPosition().y,
//													ourPlayer.getBody().getLinearVelocity().x,
//													ourPlayer.getBody().getLinearVelocity().y,
//													ourPlayer.getBody().getAngularVelocity());
//			messager.send(msg);
//			
//			lastUpdate = TimeUtils.millis();
		}
		
		// Send other updates
		KeyPressMessage kpmsg = ourPlayer.getNewPress();
		
		if (kpmsg != null)
		{
			messager.send(kpmsg);
		}
	}

	private boolean updateByServer()
	{
		boolean ret = false;
		
		// While we still have messages
		for (BalanceMessage msg = this.messager.recv();
			 msg != null;
			 msg = this.messager.recv())
		{
			if (!ret)
			{
				// Taking the past form
				pastPlayer.setToPast();
	    	
				ret = true;
			}

			if (msg instanceof UpdateMessage)
			{
				UpdateMessage umsg = (UpdateMessage)msg;
			
				//pastPlayer.setUpdateMessage(umsg);
				
				//System.out.println("UP " + TimeUtils.timeSinceMillis(umsg.time));
			}
			else if (msg instanceof KeyPressMessage)
			{
				KeyPressMessage kpmsg = (KeyPressMessage)msg;
				
				pastPlayer.setMoveLeft(kpmsg.isLeftPressed);
				pastPlayer.setMoveRight(kpmsg.isRightPressed);
				pastPlayer.setJump(kpmsg.isJumpPressed, kpmsg.time);
				
				//System.out.println(TimeUtils.timeSinceMillis(kpmsg.time));
			}
		}
		
		return ret;
	}

	private void updateEffects()
	{
		if (isFrozen)
		{
			updateFreezeEffects();
		}
	}

	private void updateFreezeEffects()
	{
		if (isFreezing)
		{
			if (bgFreezeDelta < 0.08f)
				bgFreezeDelta += 0.001f;
			
			if (freezeDelta < 0.8f)
				freezeDelta += 0.05f;
			
			if (bgFreezeDelta >= 0.08f && freezeDelta >= 0.8f)
			{
				isFreezing = false;
				bgFreezeDarkening = true;
			}
		}
		else if (isThawing)
		{
			if (freezeDelta > 0)
				freezeDelta -= 0.05f;
			if (freezeDelta < 0)
				freezeDelta = 0;
			
			if (bgFreezeDelta > 0)
				bgFreezeDelta -= 0.001f;
			if (bgFreezeDelta < 0)
				bgFreezeDelta = 0;
			
			if (freezeDelta == 0 && bgFreezeDelta == 0)
			{
				isThawing = false;
				isFrozen = false;
			}
		}
		else
		{
			if (bgFreezeDarkening)
			{
				bgFreezeDelta -= 0.001f;
				
				if (bgFreezeDelta <= 0.02f)
				{
					bgFreezeDarkening = false;
				}
			}
			else
			{
				bgFreezeDelta += 0.001f;
				
				if (bgFreezeDelta >= 0.08f)
				{
					bgFreezeDarkening = true;
				}
			}
		}
		
	}

	public void render() {
		
		// clear screen
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// set camera to follow player
		/*			cam.position.set(ourPlayer.getXPosition(), 
								 ourPlayer.getYPosition() + (Game.V_HEIGHT / 6 ), 0);
		*/
		// Update our cam
		cam.update();

		//logger.log();
		
		
		sb.begin();
		
		sb.setProjectionMatrix(hudCam.combined);
		//sb.setProjectionMatrix(cam.combined);
    	// draw bgs
 		background.render(sb);
 		
 		renderBGEffects(sb);
 		
 		sb.setProjectionMatrix(cam.combined);
 
 	    // render bar
 	    bar.render(sb);
 	    pastBar.render(sb);
 	    
 	    // draw weights
 	    Iterator<Weight> weightsItr = Weights.iterator();
 	    while(weightsItr.hasNext())
 	    {
 	    	weightsItr.next().render(sb);
 	    }
 	    
 	    // draw weights
 	    Iterator<Powerup> powerupsItr = powerups.iterator();
 	    while(powerupsItr.hasNext())
 	    {
 	    	powerupsItr.next().render(sb);
 	    }

		// draw player
		ourPlayer.render(sb);
		pastPlayer.render(sb);

		// Render all other players
		Iterator<Player> itr = Players.iterator();
	    while(itr.hasNext())
	    {
	    	itr.next().render(sb);
	    }
	    
	    // Drawing filters on top of our drawings
	    renderFGEffects();

		// draw hud
		sb.setProjectionMatrix(hudCam.combined);
		hud.render(sb);
		
		sb.end();

		// Drawing legend
		RenderLegend();

		// draw box2d
		if(debug) {
			b2dr.render(world, b2dCam.combined);
			pastb2dr.render(pastWorld, b2dCam.combined);
		}
		
	}
	
	private void RenderLegend()
	{
		legend.renderBackground();
		
 	    // render bar legend
 	    legend.render(bar);;
 	    
 	    // draw weights legend
 	    Iterator<Weight> weightsItr = Weights.iterator();
 	    while(weightsItr.hasNext())
 	    {
 	    	legend.render(weightsItr.next());
 	    }
 	    
 	    // draw powerups legend
 	    Iterator<Powerup> powerupsItr = powerups.iterator();
 	    while(powerupsItr.hasNext())
 	    {
 	    	legend.render(powerupsItr.next());
 	    }

		// draw player
 	    if (ourPlayer.isAlive())
 	    	legend.render(ourPlayer);

		// Render all other players
		Iterator<Player> itr = Players.iterator();
	    while(itr.hasNext())
	    {
	    	Player p = itr.next();
	    	if (p.isAlive())
	    		legend.render(p);
	    }
		
	    legend.end();
	}

	private void renderBGEffects(SpriteBatch sb)
	{
		// Draw background freeze effects
		if (isFrozen)
		{
			drawTheIcyLands();
		}
	}

	private void renderFGEffects()
	{
		// Draw foreground freeze effects
		if (isFrozen)
		{
			//drawTheIcyCubes();
		}
	}
	
	private void drawTheIcyCubes()
	{
		
		sb.setProjectionMatrix(cam.combined);
		sb.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE);
 		
 		sb.setColor(0f, 0f, freezeDelta, 0f);
 		
 	    // render bar
 	    bar.render(sb);
 	    
 	    // draw weights
 	    Iterator<Weight> weightsItr = Weights.iterator();
 	    while(weightsItr.hasNext())
 	    {
 	    	weightsItr.next().render(sb);
 	    }
	    
	    sb.setColor(1f, 1f, 1f, 1f);
        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	}

	private void drawTheIcyLands()
	{
		sb.setProjectionMatrix(hudCam.combined);
		
		sb.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE);
		
        sb.setColor(0f, 0f, bgFreezeDelta, 1f);
     	    
        // draw bgs
 		background.render(sb);
	    
	    sb.setColor(1f, 1f, 1f, 1f);
        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	}

	public void dispose() {}
	
	private void createPowerup()
	{
		String textureName;
		POWERUP_TYPE type;
		POWERUP_MOVEMENT movement;
		float x;
		float y;
		short category;

		// Figuring movement type
		int n = 0 + (int)(Math.random()*4);

		switch (n)
		{
			case 0:
			{
				movement = POWERUP_MOVEMENT.FALLING;
				x = (float) (getMapLeftBorder() + (Math.random() * mapHorizontalLen));
				y = mapVerticalLen / 2;
				break;
			}
			case 1:
			{
				movement = POWERUP_MOVEMENT.RIGHT;
				x = 0;
				y = 0 + (int)(Math.random()*400);
				break;
			}
			case 2:
			{
				movement = POWERUP_MOVEMENT.LEFT;
				x = 900;
				y = 0 + (int)(Math.random()*400);
				break;
			}
			case 3:
			default:
			{
				movement = POWERUP_MOVEMENT.FLOATING;
				x = 200 + (int)(Math.random()*700);
				y = 0 + (int)(Math.random()*400);
				break;
			}
		}
		
		// Figuring powerup type
		n = 0 + (int)(Math.random()*3);
		//n = 0;
		switch (n)
		{
			// Missile
			case 0:
			{
				textureName = "powerup_missile";
				category = B2DVars.BIT_PLAYER_MINUS_HEART;
				type = POWERUP_TYPE.MISSILE;
				break;
			}
			// SPEED
			case 1:
			{
				textureName = "powerup_speed";
				category = B2DVars.BIT_PLAYER_MINUS_HEART;
				type = POWERUP_TYPE.SPEED;
				break;
			}
			// FREEZE
			case 2:
			default:
			{
				textureName = "powerup_freeze";
				category = B2DVars.BIT_PLAYER_MINUS_HEART;
				type = POWERUP_TYPE.FREEZE;
				break;
			}
		}
		
		powerups.add(new Powerup(this.id++, world, type, textureName, category, x, y, movement));
	}
	
	private void createWeight(float x, float y, int size, int id)
	{
		System.out.println(x + ", " + y);
		Weights.add(new Weight(id++, null, x, y, size, world));
	}
	
	public void AddPlayer(String name)
	{
		Player newPlayer = new FluffyPlayer(this.id++, world, getMapLeftBorder() + (int)(Math.random()*mapHorizontalLen), 400, 
											name, FACING_SIDE.RIGHT, ourPlayerStats);
		Players.add(newPlayer);
		//Entities.put(name, newPlayer);
	}
	
	public void AddPlayer(String name, int x, int y)
	{
		Player newPlayer = new FluffyPlayer(this.id++, world, x, y, name, FACING_SIDE.RIGHT, ourPlayerStats);
		Players.add(newPlayer);
		Entities.put(name, newPlayer);
	}
	
	public void CreateOurPlayer(String name)
	{
		if (ROBOT)
		{
			ourPlayer = new RobotPlayer(this.id++, world, 0, 300, name, FACING_SIDE.RIGHT, ourPlayerStats, true);
			pastPlayer = new RobotPlayer(this.id++, pastWorld, 0, 300, name, FACING_SIDE.RIGHT, ourPlayerStats, true);
		}
		else
		{
			ourPlayer = new FluffyPlayer(this.id++, world, 0, 300, name, FACING_SIDE.RIGHT, ourPlayerStats, true);
			pastPlayer = new FluffyPlayer(this.id++, pastWorld, 0, 300, name, FACING_SIDE.RIGHT, ourPlayerStats, true);
		}
		pastPlayer.setDebug();
		
		Players.add(ourPlayer);
		Entities.put(name, ourPlayer);
	}
	
	private void CreateBar(float BarAngle)
	{
		// create bar
		bar = new Bar(this.id++, world, gameSize);
		pastBar = new Bar(this.id++, pastWorld, gameSize);
	}
	
	public List<Weight> getWeights()
	{
		return Weights;
	}
	
	public Bar getBar()
	{
		return bar;
	}
		
	private void createCrystals() {
		
		//crystals = new Array<Crystal>();
		
/*		MapLayer layer = tileMap.getLayers().get("crystals");
		
		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		
		for(MapObject mo : layer.getObjects()) {
			
			bdef.type = BodyType.StaticBody;
			
			float x = (float) mo.getProperties().get("x") / PPM;
			float y = (float) mo.getProperties().get("y") / PPM;
			
			bdef.position.set(x, y);
			
			CircleShape cshape = new CircleShape();
			cshape.setRadius(8 / PPM);
			
			fdef.shape = cshape;
			fdef.isSensor = true;
			fdef.filter.categoryBits = B2DVars.BIT_CRYSTAL;
			fdef.filter.maskBits = B2DVars.BIT_PLAYER;
			
			Body body = world.createBody(bdef);
			body.createFixture(fdef).setUserData("crystal");
			
			//Crystal c = new Crystal(body);
			//crystals.add(c);
			
			//body.setUserData(c);
			
		}*/
		
	}
	
	private void switchBlocks() {
		
		/*Filter filter = ourPlayer.getBody().getFixtureList().first()
						.getFilterData();
		short bits = filter.maskBits;
		
		// switch to next color
		// red -> green -> blue -> red
		if((bits & B2DVars.BIT_RED) != 0) {
			bits &= ~B2DVars.BIT_RED;
			bits |= B2DVars.BIT_GREEN;
		}
		else if((bits & B2DVars.BIT_GREEN) != 0) {
			bits &= ~B2DVars.BIT_GREEN;
			bits |= B2DVars.BIT_BLUE;
		}
		else if((bits & B2DVars.BIT_BLUE) != 0) {
			bits &= ~B2DVars.BIT_BLUE;
			bits |= B2DVars.BIT_RED;
		}
		
		// set new mask bits
		filter.maskBits = bits;
		ourPlayer.getBody().getFixtureList().first().setFilterData(filter);
		
		// set new mask bits for foot
		filter = ourPlayer.getBody().getFixtureList().get(1).getFilterData();
		bits &= ~B2DVars.BIT_CRYSTAL;
		filter.maskBits = bits;
		ourPlayer.getBody().getFixtureList().get(1).setFilterData(filter);*/
		
	}

	@Override
	public void freeze()
	{
		if (isThawing)
		{
			isThawing = false;
			isFreezing = true;
		}
		if (!isFrozen)
		{
			isFreezing = true;
			isFrozen = true;
		}
	}

	@Override
	public void thaw()
	{
		if (isFrozen)
		{
			isThawing = true;
		}
	}

	@Override
	public boolean isFrozen()
	{
		return isFrozen;
	}

	@Override
	public boolean isFreezing()
	{
		return isFreezing;
	}

	@Override
	public boolean isThawing()
	{
		return isThawing;
	}

	public Background getBackground()
	{
		return background;
	}
	
	public PlayerOrthographicCamera GetGameCamera()
	{
		return cam;
	}

	public int getNextId()
	{
		return this.id;
	}
	
	public float getGameSize()
	{
		return gameSize;
	}
	
	public Player getOurPlayer()
	{
		return ourPlayer;
	}
	
	public Legend getLegend()
	{
		return legend;
	}

	public MQLocal getMessageQueue()
	{
		return this.messager;
	}
}