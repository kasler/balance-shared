package gameObjects;

import handlers.BGFreezer;
import processes.BGDrawProcess;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars.SpriteType;
import entities.Freezable;
import entities.GameSprite;

/*public class Background {
	
	private TextureRegion image;
	private OrthographicCamera gameCam;
	private float scale;
	
	private float x;
	private float y;
	private int numDrawX;
	private int numDrawY;
	
	private float dx;
	private float dy;
	
	public Background(TextureRegion image, OrthographicCamera gameCam, float scale) {
		this.image = image;
		this.gameCam = gameCam;
		this.scale = scale;
		numDrawX = Game.V_WIDTH * 3/ image.getRegionWidth() + 1;
		numDrawY = Game.V_HEIGHT * 3/ image.getRegionHeight() + 1;
	}
	
	public void setVector(float dx, float dy)
	{
		this.dx = dx;
		this.dy = dy;
	}
	
	public void update(float dt)
	{
		//x += (dx * scale) * dt;
		//y += (dy * scale) * dt;
	}
	
	public void render(SpriteBatch sb)
	{
		
		float x = ((this.x + gameCam.viewportWidth / 2 - gameCam.position.x) * scale) % image.getRegionWidth();
		float y = ((this.y + gameCam.viewportHeight / 2 - gameCam.position.y) * scale) % image.getRegionHeight();
		
		int colOffset = x > 0 ? -1 : 0;
		int rowOffset = y > 0 ? -1 : 0;
		//for(int row = 0; row < numDrawY; row++) {
			//for(int col = 0; col < numDrawX; col++) {
				sb.draw(image, 0, 0); //image.getRegionWidth() , image.getRegionHeight());
			//}
		//}
	}
	
}*/


public class Background extends GameSprite implements Freezable
{
	public static final float BGStillLayer = 0f;
	public static final float BGSkyLayer = 0.1f;
	public static final float BGFrontLayer = 0.2f;
	
	
	protected BGFreezer freezer;
	
	public Background(int id, TextureRegion[] textures, OrthographicCamera gameCam)
	{
		super(id);
		
		// Adding all processes
		processManager.addProcess("BGMoveBack", new BGDrawProcess("BGMoveBack", this, textures[0], gameCam, BGStillLayer));
		processManager.addProcess("BGMoveBetween", new BGDrawProcess("BGMoveBetween", this, textures[1], gameCam, BGSkyLayer));
		processManager.addProcess("BGMoveFront", new BGDrawProcess("BGMoveFront", this, textures[2], gameCam, BGFrontLayer));
				
		processManager.addToActiveProcesses("BGMoveBack");
		processManager.addToActiveProcesses("BGMoveBetween");
		processManager.addToActiveProcesses("BGMoveFront");

		freezer = new BGFreezer();
	}
	
	
	
	public void render(SpriteBatch sb)
	{
		freezer.renderFreezeStart(sb);
		
		super.render(sb);
		
		freezer.renderFreezeEnd(sb);
	}

	@Override
	public void freeze()
	{
		freezer.freeze();
	}
	
	@Override
	public void thaw() {
		freezer.thaw();
	}
	
	@Override
	public boolean isFreezing() {
		return freezer.isFreezing();
	}
	
	@Override
	public boolean isFrozen() {
		return freezer.isFrozen();
	}
	
	@Override
	public boolean isThawing() {
		return freezer.isThawing();
	}
	
	@Override
	public SpriteType getSpriteType()
	{
		return SpriteType.OTHER;
	}

}