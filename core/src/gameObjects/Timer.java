package gameObjects;

import com.badlogic.gdx.utils.TimeUtils;

public class Timer
{
	private long time;
	private boolean loop;
	private long startTime;
	private int totalIterations;
	private int curIteration;
	private boolean isComplete;
	private boolean isRunning;
	private long pauseTime;
	private int pausedTime;
	private float timePercent;
	
	public Timer(long time, boolean isLoop, int totalIterations)
	{
		init(time, isLoop, totalIterations);
	}
	
	public void init(long time, boolean isLoop, int totalIterations)
	{
		this.time = time;
		this.loop = isLoop;
		this.totalIterations = totalIterations;
		startTime = 0;
		curIteration = 1;
		isComplete = false;
		isRunning = false;
		pauseTime = 0;
		pausedTime = 0;
		timePercent = 0;
	}
	
	public void start()
	{
		startTime = TimeUtils.millis();
		isRunning = true;
		isComplete = false;
		pauseTime = 0;
		pausedTime = 0;
		System.out.println("iteration: " + curIteration);
	}
	
	public void stop()
	{
		isRunning = false;
		curIteration = 1;
		pauseTime = 0;
		pausedTime = 0;
	}
	
	public void pause()
	{
		isRunning = false;
		pauseTime = TimeUtils.millis();
	}
	
	public void resume()
	{
		isRunning = true;
		pausedTime += TimeUtils.timeSinceMillis(pauseTime);
		pauseTime = 0;
	}
	
	public void finish()
	{
		stop();
		isComplete = true;
	}
	
	public boolean update(float dt)
	{
		float timeSince = TimeUtils.timeSinceMillis(startTime + pausedTime);
		
		timePercent = timeSince - (time / curIteration-1) / time;
		
		if (isRunning && (timeSince > time * curIteration))
		{
			curIteration = (int) ((timeSince / time) + 1);
			
			if (!loop && (getIterationsDone() >= totalIterations))
			{
				stop();
				
				isComplete = true;
				isRunning = false;
			}
			
			return true;
		}
		
		return false;
	}

	public boolean isComplete()
	{
		return isComplete;
	}

	public int getIterationsDone()
	{
		return curIteration - 1;
	}
	
	public boolean getIsRunning()
	{
		return isRunning;
	}
	
	public float getTimePercent()
	{
		return timePercent;
	}

	public void reinitialize(long newTimeMilliSecs, int newNumIterations, boolean loop)
	{
		init(newTimeMilliSecs, loop, newNumIterations);
	}
}