package gameObjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Animation {
	
	private TextureRegion[] frames;
	private float time;
	private float delay;
	private int currentFrame;
	private int timesPlayed;
	private boolean running;
	private float speedPercent;
	
	public Animation(TextureRegion[] frames)
	{
		this(frames, 1 / 12f);
	}
	
	public Animation(TextureRegion[] frames, float delay)
	{
		this.frames = frames;
		this.delay = delay;
		speedPercent = 1;
		time = 0;
		currentFrame = 0;
		timesPlayed = 0;
		running = false;
	}
	
	public void StopAnimation()
	{
		time = 0;
		currentFrame = 0;
		running = false;
	}
	
	public void StartAnimation()
	{
		StopAnimation();

		running = true;
	}
	
	public void setDelay(float delay)
	{
		this.delay = delay;
	}
	
	public void setSpeedPercent(float percent)
	{
		this.speedPercent = percent;
	}
	
	public float getDelay()
	{
		return this.delay;
	}
	
	public void update(float dt)
	{
		if(delay <= 0) return;
		time += dt;
		while(time >= delay / speedPercent)
		{
			step();
		}
	}
	
	private void step()
	{
		time -= delay / speedPercent;
		currentFrame++;
		if(currentFrame == frames.length) {
			currentFrame = 0;
			timesPlayed++;
		}
	}
	
	public TextureRegion getFrame() { return frames[currentFrame]; }
	public int getTimesPlayed() { return timesPlayed; }
}