package gameObjects;

import main.Game;

import com.badlogic.gdx.graphics.OrthographicCamera;

import entities.GameSprite;
import entities.Player;

public class PlayerOrthographicCamera  extends OrthographicCamera
{
	private boolean isOverview;
	private GameSprite followedSprite;
	private boolean zoomNormaly;
	private float zoomInPace;
	private float zoomOutPace;
	private boolean zoomInSlowly;
	private float shakeSize;
	private double shakeAngle;
	private float shakeLength;
	private float zoomSpeed;
	private float zoomGoal;
	private float gameSize;
	
	public PlayerOrthographicCamera(float gameSize)
	{
		isOverview = true;
		followedSprite = null;
		
		zoom = 1;
		zoomInPace = 0.001f;
		zoomOutPace = 0.001f;
		zoomInSlowly = false;
		zoomNormaly = true;
		this.gameSize = gameSize;
	}

	public void showOverview()
	{
		isOverview = true;
		
		position.set(0, 0, 0);
		zoom = 3 * gameSize;
	}
	
	public void follow(GameSprite followedSprite)
	{
		this.followedSprite = followedSprite;
		
		followBack();
	}
	
	public void followBack()
	{
		isOverview = false;
		zoom = 1;
		zoomInPace = 0.001f;
		zoomOutPace = 0.001f;
		zoomInSlowly = false;
		shakeAngle = 0f;
		shakeSize = 0f;
	}
	
	public void update()
	{
		// Update zoom
		updateZoom();
				
		// Following someone
		if (!isOverview && followedSprite != null)
		{
			// set camera to follow player
			position.set(followedSprite.getXPosition(), 
						 followedSprite.getYPosition() + (Game.V_HEIGHT / 6 ), 0);
		}
		
		// Update explosion
		if (shakeSize > 0)
			updateShake();
		
		super.update();
	}
	
	protected void updateShake()
	{
		// Pick a new angle
		shakeAngle += (150 + Math.random()*60);
		
		this.position.set(position.x + ((float)Math.sin(shakeAngle) * shakeSize),
					 	  position.y + ((float)Math.cos(shakeAngle) * shakeSize),
					 	  0f);
		
		shakeSize *= shakeLength;

		if (shakeSize < 0.01f)
		{
			shakeSize = 0;
			
			if (isOverview)
				showOverview();
		}
	}

	public void applyShake(float size, float length)
	{
		if (size > shakeSize)
		{
			shakeSize = size;
			shakeLength = length;
		}
	}

	private void updateZoom()
	{
		// If we are zooming a sprite, we zoom according to it's speed
		if (!isOverview && 
			followedSprite != null && 
			followedSprite instanceof Player)
		{
			if (zoomNormaly)
				updateNormalZoom();
			else
				updateSpecificZoom();
		}
	}

	private void updateSpecificZoom()
	{
		float fzoom = zoom;
		
		if (fzoom == zoomGoal)
			return;
		
		fzoom += zoomSpeed;
		if ((zoomSpeed > 0 && fzoom > zoomGoal) ||
			(zoomSpeed < 0 && fzoom < zoomGoal))
			fzoom = zoomGoal;
		
		zoom = fzoom;
	}
	
	public void switchToNormalZoom()
	{
		zoomNormaly = true;
		
		/*zoom = 1;
		zoomInPace = 0.001f;
		zoomOutPace = 0.001f;
		zoomInSlowly = false;
		shakeAngle = 0f;
		shakeSize = 0f;*/
	}
	
	public void switchToSpecificZoom(float zoomSpeed, float zoomGoal)
	{
		zoomNormaly = false;
		this.zoomSpeed = zoomSpeed;
		this.zoomGoal = zoomGoal;
	}

	private void updateNormalZoom()
	{
		float newVel = ((Player)followedSprite).getVelocity() + 1 / 2;
		
		float fzoom = zoom;
		
		// We need to zoom out
		if (fzoom < newVel && fzoom < 2.5f)
		{
			fzoom += zoomOutPace;
			zoomOutPace = zoomOutPace + zoomOutPace / 80f;
			/*if (zoom > 2.3)
			{
				zoomInPace = 0.008f;
			}
			else*/
			{
				zoomInPace = 0.001f;
			}
			
			zoomInSlowly = false;
		}
		else
		{
			if (fzoom <= 1.2f)
			{
				fzoom = 1.2f;
			}
			else
			{
				fzoom -= zoomInPace;
				if (fzoom < 1.5f)//zoomInPace > 0.006f)
				{
					zoomInSlowly = true;
				}
				
				if (zoomInSlowly)
				{
					zoomInPace = Math.max(0.001f, zoomInPace - zoomInPace / 20f);
				}
				else
				{
					zoomInPace = zoomInPace + zoomInPace / 60f;
				}
				
				zoomOutPace = 0.001f;
			}
		}
		
		// Update cam
		//if (Math.abs(cam.zoom - zoom) > 0.005f)
		{
			
			zoom = fzoom;
		}

		//cam.zoom+=0.0001f;
	}

	public void applyShakeByMass(float mass, float hitSpeed)
	{
		// Shake the cam
		float shakeSize;
		float shakeLength;
		
		shakeSize = mass / 4;
		
		if (mass < 1)
				shakeLength = 0.6f;
		else if ((mass < 4) || (hitSpeed < 11))
			shakeLength = 0.8f;
		else if (mass < 6)
			shakeLength = 0.9f;
		else if (mass < 12)
			shakeLength = 0.91f;
		else if (mass < 21)
			shakeLength = 0.92f;
		else if (mass < 33)
			shakeLength = 0.93f;
		else if (mass < 45)
			shakeLength = 0.95f;
		else
			shakeLength = 0.97f;
		
		if (hitSpeed < 2)
			shakeSize = 0;
		else if (hitSpeed < 5)
			shakeSize *= 0.3f;
		else if (hitSpeed < 11)
			shakeSize *= 0.5f;
		
		shakeSize *= hitSpeed;
		
		if (shakeSize > 230)
			shakeSize = 230;
		
		applyShake(shakeSize, shakeLength);
		
		//System.out.println("SHAKE: m-" + mass + ", s-" + hitSpeed + ", size-" + shakeSize + ", len-" + shakeLength);
	}
}
