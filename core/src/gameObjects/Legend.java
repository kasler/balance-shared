package gameObjects;

import main.Game;
import states.Play;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import definitions.B2DVars.SpriteType;
import entities.B2DSprite;
import entities.Bar;
import entities.Player;

public class Legend
{
	private float mapHorizontalLen;
	private float mapVerticalLen;
	private ShapeRenderer sr;
	private ShapeType curShapeType;
	private float legendScale;
	private float legendXPadding;
	private float legendYPadding;
	private OrthographicCamera cam;

	public Legend(float mapHorizontalLen, float mapVerticalLen, float gameSize, OrthographicCamera cam)
	{
		this.mapHorizontalLen = mapHorizontalLen;
		this.mapVerticalLen = mapVerticalLen;
		
		this.sr = new ShapeRenderer();
		this.curShapeType = null;
		this.legendScale = 20 * gameSize;
		this.cam = cam;
		cam.setToOrtho(false, Game.V_WIDTH * Game.SCALE, Game.V_HEIGHT * Game.SCALE);
		
		legendXPadding = 8;
		legendYPadding = 8;
	}
	
	public void renderBackground()
	{
		sr.setProjectionMatrix(cam.combined);
		sr.begin(ShapeType. Filled);
		sr.setColor(0.2f, 0.4f, 0.2f, 0.3f);
		
		/*sr.rect(legendXPadding + ((0 - mapHorizontalLen / 2 + mapHorizontalLen / 2) / legendScale),
				legendYPadding + ((0 - mapVerticalLen / 2 + mapVerticalLen / 2) / legendScale), 
				legendXPadding + (mapHorizontalLen / legendScale),
				legendXPadding + (mapVerticalLen / legendScale));*/
		sr.circle(legendXPadding + mapHorizontalLen/2 / legendScale,
				  legendYPadding + mapVerticalLen/2 / legendScale,
				  (mapHorizontalLen + 100)/2 / legendScale);
		
		sr.end();
	}
	
	public void render(B2DSprite sprite)
	{
		switchShapeType(sprite.getSpriteType());
		
		switch (sprite.getSpriteType())
		{
			case BAR:
			{
				sr.setColor(0.7f, 0.7f, 0.7f, 0.5f);
				Vector2 barPoint1 = ((Bar)sprite).getRightEdgePoint();
				Vector2 barPoint2 = ((Bar)sprite).getLeftEdgePoint();
				sr.line(legendXPadding + (barPoint1.x  + mapHorizontalLen/2) / legendScale,
						legendYPadding + (barPoint1.y  + mapVerticalLen/2) / legendScale, 
						legendXPadding + (barPoint2.x  + mapHorizontalLen/2) / legendScale, 
						legendYPadding + (barPoint2.y  + mapVerticalLen/2) / legendScale);
				
				/*Vector2 barPoint1 = ((Bar)sprite).getRightEdgePoint();
				Vector2 barPoint2 = ((Bar)sprite).getLeftEdgePoint();
				sr.line((barPoint1.x) / legendScale,
						(barPoint1.y) / legendScale, 
						(barPoint2.x) / legendScale, 
						(barPoint2.y) / legendScale);*/
				break;
			}
			case WEIGHT:
			{
				if (isInCircle(sprite.getXPosition(), sprite.getYPosition()))
				{
					sr.setColor(0.7f, 0.7f, 0.7f, 0.5f);
					sr.rect(legendXPadding + (sprite.getXPosition() - sprite.getWidth()/2 + mapHorizontalLen/2) / legendScale,
							legendYPadding + (sprite.getYPosition() - sprite.getHeight()/2 + mapVerticalLen/2) / legendScale,
							sprite.getWidth() / legendScale,
							sprite.getHeight() / legendScale,
							(sprite.getXPosition() + mapHorizontalLen/2) / legendScale, 
							(sprite.getYPosition() + mapVerticalLen/2) / legendScale,
							0);//(float) Math.toDegrees(sprite.getDrawingAngle()));
					
					/*sr.rect((sprite.getXPosition() - sprite.getWidth()/2) / legendScale,
							(sprite.getYPosition() - sprite.getHeight()/2) / legendScale,
							sprite.getWidth() / legendScale,
							sprite.getHeight() / legendScale,
							(sprite.getXPosition()) / legendScale, 
							(sprite.getYPosition()) / legendScale,
							(float) Math.toDegrees(sprite.getDrawingAngle()));
					
					sr.circle((sprite.getXPosition()) / legendScale, 
							 (sprite.getYPosition()) / legendScale,
							 2);*/
				}
				break;
			}
			case FLUFFY_PLAYER:
			{
				if (isInCircle(sprite.getXPosition(), sprite.getYPosition()))
				{
					sr.setColor(1f, 0.3f, 0.3f, 0.5f);
					
					float R = 16;
					
					if (((Player)sprite).isUs())
						R *= 2; 
						
					sr.circle(legendXPadding + (sprite.getXPosition() + mapHorizontalLen/2) / legendScale, 
							  legendXPadding + (sprite.getYPosition() + mapVerticalLen/2) / legendScale, 
							  R / legendScale);
				}
				break;
			}
			case ROBOT_PLAYER:
			{
				break;
			}
			case POWERUP:
			{
				if (isInCircle(sprite.getXPosition(), sprite.getYPosition()))
				{
					sr.setColor(0f, 1f, 0.3f, 0.2f);
					sr.circle(legendXPadding + sprite.getXPosition() / legendScale, 
							  legendYPadding + sprite.getYPosition() / legendScale, 
							  2 / legendScale);
				}
				break;
			}
			default:
			{
				
			}
		}
	}
	
	public void end()
	{
		if (curShapeType != null)
		{
			sr.end();
			curShapeType = null;
		}
	}
	
	private void switchShapeType(SpriteType type)
	{
		switch (type)
		{
			case BAR:
			case WEIGHT:
			case FLUFFY_PLAYER:
			case ROBOT_PLAYER:
			case POWERUP:
			default:
			{
				if (curShapeType == null)
				{
					curShapeType = ShapeType.Line;
					sr.begin(curShapeType);
				}
				
				break;
			}
		}
	}
	
	public boolean isInCircle(float x, float y)
	{
		float dx = Math.abs(x - 0);
		float dy = Math.abs(y - 0);
		
		if (dx + dy <= mapHorizontalLen/2)
			return true;
		if (dx > mapHorizontalLen/2)
			return false;
		if (dy > mapHorizontalLen/2)
			return false;
		if (dx*dx + dy*dy <= mapHorizontalLen/2*mapHorizontalLen/2 )
			return true;
		else
			return false;
	}
}
