package processes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import definitions.GameStats;
import entities.Player;

public class RespawnProcess extends TimedProcess 
{
	private boolean respawned;
	private int flicker;

	public RespawnProcess(String name, Player parent)
	{ 	
		super(name, 
			  // Respawn time is respawn time by level + (respawn time by level * respawn time factor by level * times already respawned)
			  (long) (GameStats.respawnTime[parent.getStats().typeIndex][parent.getStats().respawnLevel] + 
			  				(GameStats.respawnTime[parent.getStats().typeIndex][parent.getStats().respawnLevel] *
			  				 1.5f *	
			  																			   parent.getTimesRespawned())), 
			  2, parent, false);
		flicker = 0;
		respawned = false;
	}
	
	public void start()
	{
		respawned = false;
		Player player = (Player) parent;
		
		reinitialize((long) (GameStats.respawnTime[player.getStats().typeIndex][player.getStats().respawnLevel] + 
  									(GameStats.respawnTime[player.getStats().typeIndex][player.getStats().respawnLevel] *
  											0.25f * //GameStats.respawnFactor[player.getStats().typeIndex][player.getStats().respawnLevel] *	
  												player.getTimesRespawned())),
  					 1, false);
  																			   
		
		super.start();
		
	}
	
	public void update(float dt)
	{
		super.update(dt);
		
		if (!respawned)
		{
			// TODO update respawn time
		}
	}
		    
	public void render(SpriteBatch sb)
	{
		super.render(sb);
		
		
		if (!isRunning)
			return;
		
		if (respawned)
		{
			Process curProcess = null;
			java.util.Iterator<Process> itr = parent.getProcessManager().getActiveProcessesIterator();
			
			//iterate through HashMap values iterator
		    while(itr.hasNext())
		    {
		    	curProcess = itr.next();
		    	
		    	if (curProcess.getName() != name && (curProcess instanceof FrameDrawProcess))
		    	{
		    		Color color = new Color(sb.getColor());
		    		if (flicker > 4)
		    			color.a = 0.5f;
		    		else
		    			color.a = 0.3f;
		    		++flicker;
		    		if (flicker > 8)
		    			flicker =  0;
		    		
		    		((FrameDrawProcess)curProcess).setCustomColor(color);
		    	}
		    }
		}
	}

	@Override
	protected void onTimerStart()
	{
		if (!respawned)
			((Player)parent).disappear();
		
		// TODO Write respawn time
	}

	@Override
	protected void onTimeout()
	{
		((Player)parent).setGodMode(false);
		finish();
	}
	
	@Override
	protected void onIterationComplete()
	{
		if (!respawned)
		{
			((Player)parent).respawn();
			
			((Player)parent).setGodMode(true);
			respawned = true;
			
			// Reinitialize timer for the god mode flickerings
			reinitialize(GameStats.respawnFlickeringTime[((Player)parent).getStats().typeIndex], 1, false);
			// Starting the timer
			super.start();
		}
	}
}
