package processes;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import entities.B2DSprite;

public class RunProcess extends AnimationDrawProcess
{	
	public RunProcess(String i_name, TextureRegion[] reg, float runSpeed, B2DSprite i_parent)
	{
		super(i_name, reg, runSpeed, true, 0, i_parent);
	}
	
	public void runSlow()
	{
		setDelay(1/6f);
	}
	
	public void runFast()
	{
		setDelay(1/12f);
	}

	public void runReallyFast()
	{
		setDelay(1/18f);
	}
}
