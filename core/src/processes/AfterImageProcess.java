package processes;

import java.util.ArrayList;

import main.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars.FACING_SIDE;
import entities.B2DSprite;

public class AfterImageProcess extends FrameDrawProcess
{
	float alphaScale;
	int imagesDelay;
	int imagesHistoryAmount;
	boolean continousDelay;

	public class AfterImage
	{
		TextureRegion texture;
		FACING_SIDE side;
		float x;
		float y;
		float angle;
		
		public AfterImage(TextureRegion texture, FACING_SIDE side, float x, float y, float angle)
		{
			this.texture = texture;
			this.side = side;
			this.x = x;
			this.y = y;
			this.angle = angle;
		}
	}
	
	ArrayList<AfterImage> lastFrames;
	
	public AfterImageProcess(String name, B2DSprite parent, int imagesDelay, int imagesAmount, float alphaScale, boolean isContinousDelay)
	{
		super(name, parent, null);
		
		lastFrames = new ArrayList<AfterImage>(10);
		
		if (continousDelay)
			this.imagesHistoryAmount = imagesAmount;
		else
			this.imagesHistoryAmount = imagesAmount * imagesDelay;
		
		this.imagesDelay = imagesDelay;
		this.continousDelay = isContinousDelay;
		this.alphaScale = alphaScale;
	}
	
	public void start()
	{
		super.start();
		
		lastFrames.clear();
	}
	
	public void stop()
	{
		super.stop();
	}
	
	public void pause()
	{
		super.pause();
	}
	
	public void resume()
	{
		super.resume();
	}
	
	public void finish()
	{
		super.finish();
	}

	@Override
	public void update(float dt)
	{
		if (isCompleted || !isRunning)
		{
			return;
		}

		Process curProcess = null;
		java.util.Iterator<Process> itr = parent.getProcessManager().getActiveProcessesIterator();
		
		//iterate through HashMap values iterator
	    while(itr.hasNext())
	    {
	    	curProcess = itr.next();
	    	
	    	if (curProcess.getName() != name && (curProcess instanceof FrameDrawProcess))
	    	{
	    		if (((FrameDrawProcess)curProcess).frame != null)
	    			lastFrames.add(new AfterImage(
	    								new TextureRegion(((FrameDrawProcess) curProcess).frame),
	    								  				  ((FrameDrawProcess) curProcess).parent.getSide() ,
	    												  ((FrameDrawProcess) curProcess).parent.getXPosition(),
	    										  		  ((FrameDrawProcess) curProcess).parent.getYPosition(),
	    										  		  ((FrameDrawProcess) curProcess).parent.getDrawingAngle()));
	    	}
	    }
	}

	@Override
	public void render(SpriteBatch sb)
	{
		if (isCompleted || !isRunning)
		{
			return;
		}
		
		Color color = sb.getColor();
		float oldAlpha = color.a;
		
		// We want the frame from the parent's current drawing processes,
		// we will save it in a queue, and render it later
		if (continousDelay || lastFrames.size() > imagesDelay)
		{
			if (lastFrames.size() > 0)
			{
				int nextIndex;
				
				if (continousDelay)
					nextIndex = lastFrames.size() / 2;
				else
					nextIndex = lastFrames.size() - imagesDelay;
				while ((nextIndex >= 0 && !continousDelay) || (nextIndex >= 1 && continousDelay))
				{
					color.a *= alphaScale;
					sb.setColor(color);
					
					AfterImage curImage = lastFrames.get(nextIndex);
					
					drawSpecific(sb, curImage.texture, curImage.side,
								 curImage.x, curImage.y, curImage.angle);
					
					if (continousDelay)
						nextIndex /= 2;
					else
						nextIndex -= imagesDelay;
				}
			}
			
			if (lastFrames.size() == imagesHistoryAmount)
			{
				lastFrames.remove(0);
			}
			
			color.a = oldAlpha;
			sb.setColor(color);
		}
	}

	private void drawSpecific(SpriteBatch sb, TextureRegion frame, FACING_SIDE side, float x,
							  float y, float angle)
	{
		float frameHeight = frame.getRegionHeight() * YScale / Game.OBJECT_SCALE;
		float frameWidth = frame.getRegionWidth() * XScale / Game.OBJECT_SCALE;
		
		if (side == FACING_SIDE.LEFT && !frame.isFlipX())
		{
			frame.flip(true, false);
		}
		else if (side == FACING_SIDE.RIGHT && frame.isFlipX())
		{
			frame.flip(true, false);
		}
		
		sb.draw(frame,
				x - frameWidth / 2,
				y - frameHeight / 2,
				frameWidth / 2f,
				frameHeight / 2f,
				frameWidth,
				frameHeight,
				1f,
				1f,
				(float)Math.toDegrees(angle));
	}
}
