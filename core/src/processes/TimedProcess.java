package processes;

import gameObjects.Timer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import entities.B2DSprite;

public abstract class TimedProcess extends Process
{	
	protected Timer timer;
	protected boolean isFinishedIteration;
	private boolean isDone;

	public TimedProcess(String name, long milliSecs, int numIterations, B2DSprite parent, boolean loop)
	{
		super(name, parent, loop);
		
		timer = new Timer(milliSecs, loop, numIterations);
		isDone = false;
	}
	
	protected void reinitialize(long newTimeMilliSecs, int newNumIterations, boolean loop)
	{
		timer.reinitialize(newTimeMilliSecs, newNumIterations, loop);
	}
	
	public void start()
	{
		super.start();

		timer.start();
		
		onTimerStart();
	}
	
	public void stop()
	{
		timer.stop();
		super.stop();
	}
	
	public void pause()
	{
		timer.pause();
		super.pause();
	}
	
	public void resume()
	{
		timer.resume();
		super.resume();
	}
	
	public void finish()
	{
		super.finish();
		timer.finish();
	}

	@Override
	public void update(float dt)
	{
		if (isCompleted || !isRunning || timer.isComplete())
			return;
		
		isFinishedIteration = timer.update(dt);
		
		if (isFinishedIteration)
		{
			onIterationComplete();
		}
		
		if (timer.isComplete())
		{
			onTimeout();
			
			finish();
		}
	}

	@Override
	public void render(SpriteBatch sb) {}
	
	public boolean isIterationFinished()
	{
		return isFinishedIteration;
	}
	
	public boolean isTimerComplete()
	{
		return timer.isComplete();
	}
	
	protected abstract void onTimerStart();

	protected abstract void onIterationComplete();
	
	protected abstract void onTimeout();
}