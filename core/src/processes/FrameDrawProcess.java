package processes;

import main.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars.FACING_SIDE;
import entities.B2DSprite;

public class FrameDrawProcess extends Process
{	
	protected TextureRegion frame;
	protected FACING_SIDE side;
	protected boolean flip;
	protected float frameHeight;
	protected float frameWidth;
	protected float XScale;
	protected float YScale;
	private Color customColor;
	private Color serverColor;
	private Color pastColor;
	
	
	public FrameDrawProcess(String i_name, B2DSprite i_parent, TextureRegion i_frame, float gameSize, int YScale)
	{
		super(i_name, i_parent, false);
		this.XScale = gameSize;
		this.YScale = YScale;
		this.customColor = null;
		
		serverColor = new Color(0, 1, 1, 0.1f);
		pastColor = new Color(0, 1, 1, 0.3f);

		setFrame(i_frame);
		
	}
	
	public FrameDrawProcess(String i_name, B2DSprite i_parent, TextureRegion i_frame)
	{
		this(i_name, i_parent, i_frame, 1, 1);
	}
	
	public void setFrame(TextureRegion i_frame)
	{
		if (i_frame != null)
		{
			frame = i_frame;
			frameHeight = frame.getRegionHeight() * YScale / Game.OBJECT_SCALE;
			frameWidth = frame.getRegionWidth() * XScale / Game.OBJECT_SCALE;
			
			if (parent.getSide() == FACING_SIDE.LEFT)
			{
				frame.flip(true, false);
			}
		}
	}
	
	public void update(float dt)
	{
		if (frame == null)
		{
			// TODO ERROR
			return;
		}

		if (parent.getSide() == FACING_SIDE.LEFT && !frame.isFlipX())
		{	
			frame.flip(true, false);
		}
		else if (parent.getSide() == FACING_SIDE.RIGHT && frame.isFlipX())
		{	
			frame.flip(true, false);
		}
	}
	
	public void render(SpriteBatch sb)
	{
		if (frame == null)
		{
			// TODO ERROR
			return;
		}

		frame.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

		Color originalColor = null;
		
		// If we use a different color for this
		if (customColor != null)
		{
			originalColor = sb.getColor();
			sb.setColor(customColor);
		}
		
		sb.draw(frame,
				parent.getXPosition() - frameWidth / 2,
				parent.getYPosition() - frameHeight / 2,
				frameWidth / 2f,  
				frameHeight / 2f,
				frameWidth,
				frameHeight,
				1f,
				1f,
				(float)Math.toDegrees(parent.getDrawingAngle()));
		
		if (customColor != null)
		{
			sb.setColor(originalColor);
			customColor = null;
		}
	}
	
	public void pastRender(SpriteBatch sb)
	{
		if (frame == null)
		{
			// TODO ERROR
			return;
		}

		frame.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		Color oc = sb.getColor();
		sb.setColor(pastColor);

		sb.draw(frame,
				((B2DSprite)parent).getPastXPosition() - frameWidth / 2,
				((B2DSprite)parent).getPastYPosition() - frameHeight / 2,
				frameWidth / 2f,  
				frameHeight / 2f,
				frameWidth,
				frameHeight,
				1f,
				1f,
				(float)Math.toDegrees(parent.getDrawingAngle()));
		
		sb.setColor(oc);
	}
	
	public void serverRender(SpriteBatch sb)
	{
		if (frame == null)
		{
			// TODO ERROR
			return;
		}

		frame.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		Color oc = sb.getColor();
		sb.setColor(serverColor);

		sb.draw(frame,
				parent.getXPosition() - frameWidth / 2,
				parent.getYPosition() - frameHeight / 2,
				frameWidth / 2f,  
				frameHeight / 2f,
				frameWidth,
				frameHeight,
				1f,
				1f,
				(float)Math.toDegrees(parent.getDrawingAngle()));
		
		sb.setColor(oc);
	}
	
	public void setCustomColor(Color customColor)
	{
		this.customColor = customColor;
	}
	
	public void setOriginalColor()
	{
		this.customColor = null;
	}
	
	public Color getCustomColor()
	{
		return this.customColor;
	}
}