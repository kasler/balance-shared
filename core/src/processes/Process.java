package processes;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import entities.GameSprite;

public abstract class Process
{
	protected GameSprite parent;
	protected boolean isRunning;
	protected boolean isLoop;
	protected boolean isCompleted;
	protected String name; 
	protected float width;
	protected float height;
	
	public Process(String i_name, GameSprite i_parent, boolean looping)
	{
		name = i_name;
		parent = i_parent;
		isLoop = looping;
		isCompleted = false;
		isRunning = false;
	}
	
	public boolean isProcessComplete()
	{
		return isCompleted;
	}
	
	public boolean isRunning()
	{
		return isRunning;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void finish()
	{
		isRunning = false;
		isCompleted = true;
	}
	
	public void start()
	{
		isRunning = true;
		isCompleted = false;
	}
	
	public void pause()
	{
		isRunning = false;
	}
	
	public void stop()
	{
		isRunning = false; 
	}
	
	public void resume()
	{
		isRunning = true;
	}
	
	public abstract void update(float dt);
	
	public abstract void render(SpriteBatch sb);

	public boolean isDebugParent()
	{
		return parent.isDebug();
	}
}
