package processes;

import java.util.ArrayList;

import main.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import definitions.B2DVars.FACING_SIDE;
import entities.B2DSprite;

public class FreezeProcess extends FrameDrawProcess
{
	public class AfterImage
	{
		TextureRegion texture;
		FACING_SIDE side;
		float x;
		float y;
		float angle;
		
		public AfterImage(TextureRegion texture, FACING_SIDE side, float x, float y, float angle)
		{
			this.texture = texture;
			this.side = side;
			this.x = x;
			this.y = y;
			this.angle = angle;
		}
		
	}
	
	ArrayList<AfterImage> lastFrames;
	AfterImage prevFrame;
	
	private float alpha;
	private boolean thaw;
	private int slow;
	private float originalAngDamping;
	private float originalLinDamping;
	private Body bar;
	private float alphaScale;

	public FreezeProcess(String name, B2DSprite parent, TextureRegion frame)
	{
		this(name, parent, frame, 1, 1);
	}
	
	public FreezeProcess(String name, B2DSprite parent, TextureRegion frame, float gameSize, int YScale)
	{
		super(name, parent, frame, gameSize, YScale);
		
		alpha = 0;
		thaw = false;
		slow = 0;
		originalAngDamping = 0;
		originalLinDamping = 0;
		bar = null;
		alphaScale = 0;
		
		if (parent.getName().equals("bar"))
		{
			bar = ((B2DSprite)parent).getBody();
		}
		
		lastFrames = new ArrayList<AfterImage>(10);
		prevFrame = null;
	}
	
	public void start()
	{
		super.start();
		alpha = 0;
		alphaScale = 0;
		slow = 0;
		thaw = false;
		isRunning = true;
		originalAngDamping = ((B2DSprite)parent).getBody().getAngularDamping();
		originalLinDamping = ((B2DSprite)parent).getBody().getLinearDamping();
		
		lastFrames.clear();
		prevFrame = null;
	}

	public void update(float dt)
	{
		if (isRunning)
		{
			if (thaw)
			{
				if (alphaScale > 0)
					alphaScale -= 0.02f;
				if (alphaScale < 0)
					alphaScale = 0;
			}
			else
			{
				if (alphaScale < 0.5f)
				{
					alphaScale += 0.005;
				}
			}
			
			// We want the frame from the player's current drawing process,
			// we will save it in a queue, and render it later
			
				
			
			if (lastFrames.size() >= 100)
			{
				
				lastFrames.remove(0);
				
				if (thaw && lastFrames.size() > 1)
					lastFrames.remove(0);
			}
			if (lastFrames.size() > 0)
			{
				prevFrame = lastFrames.get(lastFrames.size() / 2);
			}
				

			Process curProcess = null;
			java.util.Iterator<Process> itr = parent.getProcessManager().getActiveProcessesIterator();
			
			//iterate through HashMap values iterator
		    while(itr.hasNext())
		    {
		    	curProcess = itr.next();
		    	
		    	if (curProcess.getName() != name && (curProcess instanceof FrameDrawProcess))
		    	{
		    		lastFrames.add(new AfterImage(((FrameDrawProcess) curProcess).frame,
		    									  ((FrameDrawProcess) curProcess).parent.getSide() ,
		    									  ((FrameDrawProcess) curProcess).parent.getXPosition(),
		    									  ((FrameDrawProcess) curProcess).parent.getYPosition(),
		    									  ((FrameDrawProcess) curProcess).parent.getDrawingAngle()));
		    	}
		    	
		    	if (curProcess.getName() != name && (curProcess instanceof AnimationDrawProcess))
		    	{
		    		((AnimationDrawProcess)curProcess).setSpeedPercent(0.1f);
		    	}
		    }
		    
		    if (slow < 50)
		    {
		    	slow += 2;
		    	
			    if (bar != null)
			    {
			    	//((B2DSprite)parent).getBody().setAngularVelocity(0);
			    	bar.setAngularDamping(originalAngDamping + slow/8);	
			    }
			    
			    //((B2DSprite)parent).getBody().setLinearVelocity(0, 0);
			    ((B2DSprite)parent).getBody().setLinearDamping(originalLinDamping + slow);
		    }
		}
	}
	
	public void render(SpriteBatch sb)
	{
		if (prevFrame != null)
		{
			Color color = sb.getColor();//get current Color, you can't modify directly
			float oldAlpha = color.a; //save its alpha
	
			//From here you can modify alpha however you want
			color.a = oldAlpha*alphaScale; //ex. scale = 0.5 will make alpha halved
			sb.setColor(color); //set it
			
			drawSpecific(sb,
						 new TextureRegion(prevFrame.texture),
	    				 prevFrame.side,
	    				 prevFrame.x,
	    				 prevFrame.y,
	    				 prevFrame.angle);
	
			//Set it back to original alpha when you're done with your alpha manipulation
			color.a = oldAlpha;
			sb.setColor(color);
		}
		
		Color c = sb.getColor();
		
		if (thaw)
		{
			if (alpha > 0)
			{
				alpha -= 0.02f;
			}
			else
			{
				end();
			}
			
			if (alpha < 0)
				alpha = 0;
		}
		else
		{
			if (alpha < c.a)
			{
				alpha += 0.02f;
			}
			if (alpha > c.a)
				alpha = c.a;
		}
		
		float a = c.a;
		c.a = alpha;

		sb.setColor(c);
		
		super.render(sb);
		
		c.a = a;
		sb.setColor(c);
	}

	private void drawSpecific(SpriteBatch sb, TextureRegion frame, FACING_SIDE side, float x,
							  float y, float angle)
	{
		float frameHeight = frame.getRegionHeight() * YScale / Game.OBJECT_SCALE;
		float frameWidth = frame.getRegionWidth() * XScale / Game.OBJECT_SCALE;
		
		if (side == FACING_SIDE.LEFT && !frame.isFlipX())
		{
			frame.flip(true, false);
		}
		else if (side == FACING_SIDE.RIGHT && frame.isFlipX())
		{
			frame.flip(true, false);
		}
		
		sb.draw(frame,
				x - frameWidth / 2,
				y - frameHeight / 2,
				frameWidth / 2f,
				frameHeight / 2f,
				frameWidth,
				frameHeight,
				1f,
				1f,
				(float)Math.toDegrees(angle));
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop()
	{
		java.util.Iterator<Process> itr = parent.getProcessManager().getActiveProcessesIterator();
		Process curProcess = null;
		
		//iterate through HashMap values iterator
	    while(itr.hasNext())
	    {
	    	curProcess = itr.next();
	    	
	    	if (curProcess.getName() != name && (curProcess instanceof AnimationDrawProcess))
	    	{
	    		((AnimationDrawProcess)curProcess).setSpeedPercent(1f);
	    	}
	    }
	    
	    end();
	}

	private void end()
	{
		if (bar != null)
	    {
	    	//((B2DSprite)parent).getBody().setAngularVelocity(0);
	    	bar.setAngularDamping(originalAngDamping);
	    }
	    
	    //((B2DSprite)parent).getBody().setLinearVelocity(0, 0);
	    ((B2DSprite)parent).getBody().setLinearDamping(originalLinDamping);
	    
	    isRunning = false;
	    isCompleted = true;
	    alphaScale = 0;
	    
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	public void finish()
	{
		thaw = true;
	}
}
