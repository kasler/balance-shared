package processes;

import gameObjects.Background;
import states.Play;
import main.Game;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import entities.GameSprite;

public class BGDrawProcess extends Process
{
	private OrthographicCamera gameCam;
	private float scale;
	private int numDrawX;
	private int numDrawY;
	private float dx;
	private float dy;
	private float x;
	private float y;
	private TextureRegion image;

	public BGDrawProcess(String name, GameSprite parent,
									  TextureRegion image, OrthographicCamera gameCam, float scale)
	{
		super(name, parent, true);
		
		this.gameCam = gameCam;
		this.scale = scale;
		this.image = image;
		numDrawX = Game.V_WIDTH / image.getRegionWidth() + 1;
		numDrawY = Game.V_HEIGHT / image.getRegionHeight() + 1;
	}

	public void setVector(float dx, float dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public void update(float dt)
	{
		x += (dx * scale) * dt;
		y += (dy * scale) * dt;
	}
	
	@Override
	public void render(SpriteBatch sb)
	{	
		float x = ((this.x + gameCam.viewportWidth / 2 - gameCam.position.x) * scale) % (image.getRegionWidth());
		float y = ((this.y + gameCam.viewportHeight / 2 - gameCam.position.y) * scale);// % image.getRegionHeight();
        
//		if (//TODO ISGROUND DRAWING)
		if (scale == Background.BGFrontLayer)
		{
			y -= 310;
			if (y > 0)
				y = 0;
		}
		
		int colOffset = x > 0 ? -1 : 0;
		//int rowOffset = y > 0 ? -1 : 0;
		//for(int row = 0; row < numDrawY; row++) {
			for(int col = 0; col < numDrawX; col++) {
				sb.draw(image, x + (col + colOffset) * image.getRegionWidth(), y /*+ (rowOffset + row) * image.getRegionHeight()*/);
				
				/*sb.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE);
		        sb.setColor(0.3f, 0.3f, 0.3f, 1f);
		        sb.draw(image, x + (col + colOffset) * image.getRegionWidth(), y);
		        sb.setColor(1f, 1f, 1f, 1f);
		        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);*/
			}
		//}
	}

}
