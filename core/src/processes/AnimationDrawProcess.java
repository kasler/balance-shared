package processes;

import gameObjects.Animation;
import gameObjects.Timer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import entities.B2DSprite;

public class AnimationDrawProcess extends FrameDrawProcess
{
	protected Animation animation;
	protected int timesToPlay;
	private Timer timer;
	
	public AnimationDrawProcess(String i_name, TextureRegion[] reg, boolean i_isLoop, int timesToPlay, B2DSprite i_parent)
	{
		this(i_name, reg, 1/12f, i_isLoop, timesToPlay, i_parent);
	}
	
	public AnimationDrawProcess(String i_name, TextureRegion[] reg, B2DSprite i_parent)
	{
		this(i_name, reg, 1/12f, true, 0, i_parent);
	}
	
	public AnimationDrawProcess(String i_name, TextureRegion[] reg, float delay, boolean i_isLoop, int i_timesToPlay, B2DSprite i_parent)
	{
		this(i_name, reg, delay, i_isLoop, i_timesToPlay, i_parent, 0);
	}
	
	public AnimationDrawProcess(String i_name, TextureRegion[] reg, float delay, boolean i_isLoop, int i_timesToPlay, B2DSprite i_parent, long millis)
	{
		super(i_name, i_parent, reg[0]);
		
		isLoop = i_isLoop;
		timesToPlay = i_timesToPlay;
		if (millis > 0)
			timer = new Timer(millis, false, 1);
		else
			timer = null;
		
		animation = new Animation(reg, delay);
	}
	
	public void setDelay(float delay)
	{
		animation.setDelay(delay);
	}
	
	public void start()
	{
		super.start();
		
		animation.StartAnimation();
		
		if (timer != null)
			timer.start();
	}
	
	public void pause()
	{
		// TODO
		
		if (timer != null)
			timer.pause();
	}
	
	public void resume()
	{
		// TODO REDO
		animation.StartAnimation();
		
		if (timer != null)
			timer.resume();
	}
	
	public void stop()
	{
		super.stop();
		
		animation.StopAnimation();
		
		if (timer != null)
			timer.stop();
	}
	
	public void finish()
	{
		super.finish();
		
		stop();
		
		if (timer != null)
			timer.stop();
	}
	
	public void update(float dt)
	{
		if (isCompleted)
		{
			return;
		}
		
		animation.update(dt);

		setFrame(animation.getFrame());
		
		super.update(dt);

		if (timer != null)
		{
			if (timer.update(dt))
				finish();
		}
		else if (animation.getTimesPlayed() >= timesToPlay && !isLoop)
		{
			finish();
		}
	}
	
	public void render(SpriteBatch sb)
	{
		super.render(sb);
	}
	
	public int getTimesPlayed()
	{
		return animation.getTimesPlayed();
	}

	public void setSpeedPercent(float percent)
	{
		animation.setSpeedPercent(percent);
	}
	
	public float getCompletionPercent()
	{
		// TODO
		return 0;
	}
}
