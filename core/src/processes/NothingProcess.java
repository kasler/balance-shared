package processes;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import entities.B2DSprite;

public class NothingProcess extends Process
{	
	public NothingProcess(B2DSprite i_parent)
	{
		super("Nothing", i_parent, true);
	}
	
	public boolean isProcessComplete()
	{
		return isCompleted;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void start() {}
	
	public void pause() {}
	
	public void resume() {}
	
	public void stop() {}
	
	public void update(float dt) {}
	
	public void render(SpriteBatch sb) {}
}

