package definitions;

public class B2DVars {
	
	public static final int FLYING_PRESS_FORCE = 0;
	
	// pixel per meter ratio
	public static final float PPM = 60;
	
	// The size scale of this game
	public static final float GAME_SIZE = 6;
	
	// The camera's zoom.. how much bigger do we show the camera then base size
	public static final float ZOOM = 3;
	
	public static final float GRAVITY = -9.81f;
	
	// Player facing directions
	public enum FACING_SIDE
	{
		LEFT , RIGHT;
	}
	
	// Sprite types
	public enum SpriteType
	{
		FLUFFY_PLAYER,
		ROBOT_PLAYER,
		WEIGHT,
		POWERUP,
		BAR,
		OTHER
	}
	
	// Powerups
	public enum POWERUP_TYPE
	{
		MISSILE,
		SPEED,
		FREEZE,
		AIR_SMASH,
		WEIGHT,
		NONE;
	}
	
	public enum POWERUP_MOVEMENT
	{
		FLOATING , RIGHT, LEFT, FALLING;
	}
	
	// category bits
	public static final short BIT_NONE = 0;
	public static final short BIT_BAR = 1;
	public static final short BIT_WEIGHT = 2;
	public static final short BIT_POWERUP = 4;
	public static final short BIT_FLUFFY_PLAYER_MINUS_HEART = 8;
	public static final short BIT_ROBOT_PLAYER_MINUS_HEART = 16;
	public static final short BIT_IGHOST = 32;
	public static final short BIT_BAR_GHOST = 64;
	public static final short BIT_PLAYER_HEART = 128;
	
	public static final short BIT_ROBOT_PLAYER =  BIT_ROBOT_PLAYER_MINUS_HEART | BIT_PLAYER_HEART;
	public static final short BIT_FLUFFY_PLAYER =  BIT_FLUFFY_PLAYER_MINUS_HEART | BIT_PLAYER_HEART;
	public static final short BIT_PLAYER_MINUS_HEART = BIT_FLUFFY_PLAYER_MINUS_HEART | BIT_ROBOT_PLAYER_MINUS_HEART;
	public static final short BIT_PLAYER = BIT_PLAYER_MINUS_HEART | BIT_PLAYER_HEART;

	public static final short BIT_ALL = BIT_PLAYER | BIT_BAR | BIT_WEIGHT | BIT_IGHOST | BIT_POWERUP;
	
	public static final short BIT_WEIGHT_WALL = BIT_WEIGHT | BIT_IGHOST; 
	
	public enum PlayerType
	{
		FLUFFY,
		ROBOT
	}
	
	// Skin types
	public enum SkinType
	{
		BUNNY
	}
	
	// Hat types
	public enum HatType
	{
		NONE
	}
	
	// Costume types 
	public enum CostumeType
	{
		NONE
	}
	
	// Costume types 
	public enum ObjectiveType
	{
		NONE
	}
	
	// Costume types 
	public enum ItemType
	{
		JETPACK,
		NONE,
		INVISIBILITY,
		SELF_DESTRUCT
	}
}
