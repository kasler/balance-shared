package definitions;

public class GameStats
{
	public static final int FluffyIndex = 0;
	public static final int RobotIndex = 1;
	
	public static final float[] density = {/* Fluffy */ 5, 
										   /* Robot */  4};
	
	public static final int speedMaxLevel = 4;
	public static final float[][] topSpeed = {// Level	    1     2     3     4     5
			 								  /* Fluffy */ {4,    5,    6,    7,    8},
			 								  /* Robot */  {2, 3.5f, 4.5f, 5.5f, 6.5f}};
	public static final float[][] accelerationForce = {// Level	     1     2     3     4     5
													   /* Fluffy */ {15,   17,   25,   35,   40},
													   /* Robot */  {60,   30,   30,   30,   30}};
	public static final float[][] airDamping = {// Level	  1     2     3     4     5
		   										/* Fluffy */ {0,    0,    0,    0,    0},
		   										/* Robot */  {0,    0,    0,    0,    0}};
	public static final float[][] brakes = {// Level	  1     2     3     4     5
											/* Fluffy */ {0,    0,    1,    2,    4},
											/* Robot */  {0,    0,    1,    2,    2}};
	
		
	public static final int jumpForceMaxLevel = 4;
	public static final float[][] jumpForce = {// Level	     1     2     3     4     5
		   									   /* Fluffy */ {550,  600,  700,  750,  800},
		   									   /* Robot */  {3000,  550,  580,  600,  650}};
	
	public static final int hitForceMaxLevel = 4;
	public static final float[] hitForceForce = {// Level	 1     2     3     4     5
		   									   	 /* Robot */ 4,    5,    6,    7,    8};
	
	public static final int respawnMaxLevel = 4;
	public static final long [][] respawnTime = {// Level	   1     2     3     4     5
											   	 /* Fluffy */ {4000, 3000, 2000, 1000, 500}, 
											   	 /* Robot */  {4000, 3000, 2000, 1000, 500}};	
	public static final float[][] respawnFactor = {// Level	  	 1	   2	 3	   4	 5
	   											   /* Fluffy */ {1.25f,1.25f, 1.6f, 1.4f, 1.2f}, 
	   											   /* Robot */  {2,    1.8f, 1.6f, 1.4f, 1.2f}};
	public static final long[] respawnFlickeringTime = {/* Fluffy */ 2000, 
		   												/* Robot */  2000};
	
	public static final int livesMaxLevel = 4;
	public static final int[][] lives = {// Level	   1     2     3     4     5
		   								 /* Fluffy */ {3,    4,    5,    6,    7},
		   								 /* Robot */  {4,    5,    6,    7,    8}};
	
	public static final int powerupsCapacityMaxLevel = 2;
	public static final int[][] powerupsCapacity = {// Level      1     2     3
			 										/* Fluffy */ {1,    2,    3},
			 										/* Robot */  {1,    2,    3}};
	
	public static final int jetpackMaxLevel = 4;
	public static final long[][] jetpackTime = {// Level	  1     2     3     4     5
			 							 	    /* Fluffy */ {8000, 11000,14000,17000,21000},
			 							 	    /* Robot */  {8000, 11000,14000,17000,21000}};

	public static final int invisibilityMaxLevel = 4;
	public static final long[][] invisibilityTime = {// Level	   1     2     3     4     5
			 							 	   		 /* Fluffy */ {8000, 11000,14000,17000,21000},
			 							 	   		 /* Robot */  {8000, 11000,14000,17000,21000}};
	
	public static final int speedPowerupMaxLevel = 3;
	public static final long[][] speedPowerupTime = {// Level	   1     2     3     4
			 							 	   		 /* Fluffy */ {5000, 7000, 10000,12000},
			 							 	   		 /* Robot */  {5000, 7000, 10000,12000}};
	public static final float[][] speedPowerupSpeedFactor = {// Level	   1     2     3     4
	   													     /* Fluffy */ {1.5f, 2,    2.4f, 2.4f},
	   													     /* Robot */  {1.2f, 1.4f, 1.8f, 1.8f}};
	public static final float[][] speedPowerupAccelerationFactor = {// Level	  1     2     3     4
																	/* Fluffy */ {30,   30,   40,   40},
																	/* Robot */  {30,   30,   40,   40}};
	public static final float[][] speedPowerupAirDamping = {// Level	  1     2     3     4
		     												/* Fluffy */ {30,   30,   40,   40},
		     												/* Robot */  {30,   30,   40,   40}};
	public static final float[][] speedPowerupGroundDamping = {// Level	  1     2     3     4
															   /* Fluffy */ {30,   30,   40,   40},
															   /* Robot */  {30,   30,   40,   40}};
	
	public static final int airSmashPowerupMaxLevel = 3;
	public static final int[] airSmashPowerupForce = {// Level	   1     2     3     4
			 							 	   		  /* Fluffy */ 1500, 2000, 2500, 3000};
	
	public static final int freezePowerupMaxLevel = 3;
	public static final int[][] freezePowerupTime = {// Level	  1     2     3     4
			 							 	   		/* Fluffy */ {3000, 5500, 7000, 10000},
			 							 	   		/* Robot */  {3000, 5500, 7000, 10000}};
	
	public static final int missilePowerupMaxLevel = 3;
	public static final int[] missilePowerupForce = {// Level	 1     2     3     4
			 							 	   		 /* Robot */ 1500, 2000, 2500, 3000};
	
	public static final int weightPowerupMaxLevel = 3;
	public static final int[] weightPowerupForce = {// Level	 1     2     3     4
			 							 	   		/* Fluffy */ 1500, 2000, 2500, 3000};
	
	public static final int lightPowerupMaxLevel = 3;
	public static final int[] lightPowerupForce = {// Level	   1     2     3     4
			 							 	   	   /* Robot */ 1500, 2000, 2500, 3000};
}