package definitions;

import com.badlogic.gdx.graphics.Color;

import definitions.B2DVars.CostumeType;
import definitions.B2DVars.HatType;
import definitions.B2DVars.ItemType;
import definitions.B2DVars.ObjectiveType;
import definitions.B2DVars.POWERUP_TYPE;
import definitions.B2DVars.PlayerType;
import definitions.B2DVars.SkinType;
import definitions.GameStats;

public class PlayerStats
{
	private static final int DEBUG_LEVEL = 0;
	
	// Personal
	public String name;
	public PlayerType curType;
	public int level;
	public int exp;
	public int expEarned;
	public int money;
	public int moneyEarned;
	public ObjectiveType[] objectivesCompleted;
	
	// Physics
	public float topSpeed;
	public float accelerationForce;
	public float jumpForce;
	public float airDamping;
	public float brakes;
	
	// Look
	public Color color;
	public SkinType skin;
	public HatType hat;
	public CostumeType costume;
	
	// Gameplay
	public int lives;
	public ObjectiveType[] objectives;

	// Inventory
	public ItemType item;
	public POWERUP_TYPE[] availPowerups;
	public int powerupsCapacity;
	public int powerupsCapacityLevel;
	
	// Item levels
	public int jetPackItemLevel;
	public int invisibilityItemLevel;

	// Powerup levels
	public int airSmashPowerupLevel;
	public int speedPowerupLevel;
	public int weightPowerupLevel;
	public int lightPowerupLevel;
	public int freezePowerupLevel;
	public int missilePowerupLevel;
	
	// Trap levels
	public int slowPowerupLevel;
	
	// Personal levels
	public int jumpLevel;
	public int speedLevel;
	public int respawnLevel;

	public int typeIndex;
	
	public PlayerStats(PlayerType type)
	{
		switch (type)
		{
		case FLUFFY:
			this.typeIndex = GameStats.FluffyIndex;
			break;
		case ROBOT:
			this.typeIndex = GameStats.RobotIndex;
			break;
		default:
			// TODO ERROR
			return;
		}
	}
	
	public void loadMenuChoices(Color color, SkinType skin, HatType hat, CostumeType costume,
			   					ObjectiveType[] objectives,
			   					ItemType item,
			   					POWERUP_TYPE[] powerups)
	{
		this.color = color;
		this.skin = skin;
		this.hat = hat;
		this.costume = costume;
		
		this.objectives = objectives;
		
		this.item = item;
		this.availPowerups = powerups;
	}

	public void loadLevels()
	{
		this.level = 0; // TODO DB

		this.speedLevel = 0; // TODO DB
		this.jumpLevel = 0; // TODO DB
		
		this.respawnLevel = 0; // TODO DB

		this.powerupsCapacityLevel = 0;
		
		// Item levels
		this.jetPackItemLevel = 0; // TODO DB
		this.invisibilityItemLevel = 0; // TODO DB

		// Powerup levels
		this.airSmashPowerupLevel = 0; // TODO DB
		this.speedPowerupLevel = 0; // TODO DB
		this.weightPowerupLevel = 0; // TODO DB
		this.lightPowerupLevel = 0; // TODO DB
		this.freezePowerupLevel = 0; // TODO DB
		this.missilePowerupLevel = 0; // TODO DB
		
		// Trap levels
		this.slowPowerupLevel = 0; // TODO DB
	}
	
	public void loadLevelsDebug(int level)
	{
		this.level = level;

		this.speedLevel = Math.min(level, GameStats.speedMaxLevel);
		this.jumpLevel = Math.min(level, GameStats.jumpForceMaxLevel);
		
		this.respawnLevel = Math.min(level, GameStats.respawnMaxLevel);

		this.powerupsCapacityLevel = Math.min(level, GameStats.powerupsCapacityMaxLevel);
		
		// Item levels
		this.jetPackItemLevel = Math.min(level, GameStats.jetpackMaxLevel);
		this.invisibilityItemLevel = Math.min(level, GameStats.invisibilityMaxLevel);

		// Powerup levels
		this.airSmashPowerupLevel = Math.min(level, GameStats.airSmashPowerupMaxLevel);
		this.speedPowerupLevel = Math.min(level, GameStats.speedPowerupMaxLevel);
		this.weightPowerupLevel = Math.min(level, GameStats.weightPowerupMaxLevel);
		this.weightPowerupLevel = Math.min(level, GameStats.lightPowerupMaxLevel);
		this.freezePowerupLevel = this.weightPowerupLevel = Math.min(level, GameStats.freezePowerupMaxLevel);
		this.missilePowerupLevel = this.weightPowerupLevel = Math.min(level, GameStats.missilePowerupMaxLevel);
		
		// Trap levels
		//this.slowPowerupLevel = 0; // TODO DB
	}
	
	public void loadStats()
	{
		loadLevelsDebug(DEBUG_LEVEL);
		
		this.name = "serj"; // TODO DB
		this.exp = 0; // TODO DB
		this.expEarned = 0;
		this.money = 0; // TODO DB
		this.moneyEarned = 0;
		this.objectivesCompleted = new ObjectiveType[3];
		//this.objectivesCompleted = {ObjectiveType.NONE, ObjectiveType.NONE, ObjectiveType.NONE};
		
		this.topSpeed = GameStats.topSpeed[typeIndex][this.speedLevel];
		this.accelerationForce = GameStats.accelerationForce[typeIndex][this.speedLevel];
		this.jumpForce = GameStats.jumpForce[typeIndex][this.jumpLevel];
		this.airDamping = GameStats.airDamping[typeIndex][this.speedLevel];
		this.brakes = GameStats.brakes[typeIndex][this.speedLevel];
		
		this.lives = 999; // TODO Do we have finite lives?

		this.powerupsCapacity = GameStats.powerupsCapacity[typeIndex][this.powerupsCapacityLevel];
	}
}
