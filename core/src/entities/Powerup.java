package entities;

import static definitions.B2DVars.PPM;
import powerupProcesses.PowerupProcess;
import processes.AnimationDrawProcess;
import main.Game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import definitions.B2DVars;
import definitions.B2DVars.FACING_SIDE;
import definitions.B2DVars.POWERUP_MOVEMENT;
import definitions.B2DVars.POWERUP_TYPE;
import definitions.B2DVars.SpriteType;

public class Powerup extends B2DSprite
{
	protected float x;
	protected float y;
	protected POWERUP_MOVEMENT movement;
	protected POWERUP_TYPE type;
	protected boolean obtained;

	public Powerup(int id, World world, POWERUP_TYPE i_type, String textureName, short player_category_bits, 
				   float i_x, float i_y, POWERUP_MOVEMENT i_movement)
	{
		super(id);
		
		x = i_x;
		y = i_y;
		type = i_type;
		movement = i_movement;
		obtained = false;
		
		Texture tex = Game.res.getTexture(textureName);
		TextureRegion[] sprites = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		
		TextureRegion[] glowingSprites;
		
		// Get correct glowing by movement
		switch (i_movement)
		{
			case LEFT:
				setSide(FACING_SIDE.LEFT);
			case RIGHT:
				glowingSprites = TextureRegion.split(Game.res.getTexture("powerup_moving_glow"), 
													 32 * Game.OBJECT_SCALE, 
													 32 * Game.OBJECT_SCALE)[0];
				break;
			case FALLING:
			case FLOATING:
			default:
				glowingSprites = TextureRegion.split(Game.res.getTexture("powerup_glow"), 
													 32 * Game.OBJECT_SCALE, 
													 32 * Game.OBJECT_SCALE)[0];
				break;
		}
				
		// Adding all processes
		processManager.addProcess("Flashing", new PowerupProcess("Flashing", this, sprites[0], glowingSprites));
		
		Texture obtainedTex = Game.res.getTexture("powerup_creation");
		TextureRegion[] obtainedSprites = TextureRegion.split(obtainedTex, 
															  32 * Game.OBJECT_SCALE, 
															  32 * Game.OBJECT_SCALE)[0];
		
		processManager.addProcess("Obtained", new AnimationDrawProcess("Obtained", obtainedSprites, false, 1, this));
				
		processManager.addToActiveProcesses("Flashing");
		
		// Creating powerup's box2d stuff
		BodyDef powerupdef = new BodyDef();

		// If it's a floating, or moving powerup, it has a static body
		if (movement != POWERUP_MOVEMENT.FALLING)
		{
			powerupdef.type = BodyType.KinematicBody;
		}
		else
		{
			powerupdef.type = BodyType.DynamicBody;
		}
		
		powerupdef.position.set(x / PPM, y / PPM);
		Body body = world.createBody(powerupdef);
		FixtureDef powerupfdef = new FixtureDef();
		CircleShape cshape = new CircleShape();
		cshape.setRadius(8 / PPM);
		powerupfdef.shape = cshape;
		powerupfdef.filter.categoryBits = B2DVars.BIT_POWERUP;
		powerupfdef.density = 0;
		powerupfdef.friction = 0;
		
		if (movement != POWERUP_MOVEMENT.FALLING)
		{
			powerupfdef.isSensor = true;
			powerupfdef.filter.maskBits = player_category_bits;
		}
		else
		{
			powerupfdef.isSensor = false;
			powerupfdef.filter.maskBits = (short) (player_category_bits | B2DVars.BIT_BAR |
										  B2DVars.BIT_IGHOST);
		}
		
		body.createFixture(powerupfdef).setUserData("powerup");
		body.setUserData(this);
		cshape.dispose();
		
		// If its moving
		if (movement == POWERUP_MOVEMENT.LEFT)
		{
			body.setLinearVelocity(0f - (float)Math.random()*2, 0f);
		}
		else if (movement == POWERUP_MOVEMENT.RIGHT)
		{
			body.setLinearVelocity(0f + (float)Math.random()*2f, 0f);
		}
		// If it's freefalling, we will make it fall a bit slow using air friction
		else if (movement == POWERUP_MOVEMENT.FALLING)
		{
			body.setLinearDamping(0f + (float)Math.random()*10f);
		}
		
		this.body = body;
	}
	
	public void update(float dt)
	{
		// Switch to the "obtained" animation
		if (obtained && !processManager.isProcessActive("Obtained"))
		{
			processManager.removeAllActiveProcesses();
			processManager.addToActiveProcesses("Obtained");
			body.getFixtureList().get(0).setSensor(true);
		}

		super.update(dt);
		
		// We are done when either flashing or obtaining process end
		if (!prepareForDelete && 
			(!processManager.isProcessActive("Flashing") && processManager.getProcess("Flashing").isProcessComplete() ||
			 !processManager.isProcessActive("Obtained") && processManager.getProcess("Obtained").isProcessComplete()))
		{
			prepareForDelete();
		}
		
	}
	
	public POWERUP_TYPE getPowerupType()
	{
		return type;
	}
	
	public void setObtained()
	{
		obtained = true;
	}
	
	@Override
	public SpriteType getSpriteType()
	{
		return SpriteType.POWERUP;
	}
}
