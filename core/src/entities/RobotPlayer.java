package entities;

import static definitions.B2DVars.PPM;

import java.util.Arrays;

import powerupProcesses.FlyingPressPowerup;
import powerupProcesses.FreezePowerupProcess;
import powerupProcesses.SpeedPowerupProcess;
import processes.FrameDrawProcess;
import processes.RespawnProcess;
import processes.RunProcess;
import states.Play;
import main.Game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import definitions.B2DVars;
import definitions.GameStats;
import definitions.PlayerStats;
import definitions.B2DVars.FACING_SIDE;
import definitions.B2DVars.POWERUP_TYPE;
import definitions.B2DVars.SpriteType;

public class RobotPlayer extends Player
{
	protected static final float airAngleCorrectionSpeed = 0f;
	protected static final float groundAngleCorrectionSpeed = 0.03f;
	
	private static final int MAX_SLOW_RUN_SPEED = 2;
	private static final int MAX_FAST_RUN_SPEED = 6;
	public static final float defaultDensity = 5f;
	public static final float defaultPlayerRadius = 39f;
	public static final float defaultFootRadius = 40f;
	public static final float defaultFriction = 2f;
	public static final float defaultHeartSize = 1f;
	public static final float maxWalkSpeed = 0.5f;
	
	public RobotPlayer(int id, World world, float x, float y, String i_name, FACING_SIDE i_side, PlayerStats stats)
	{
		this(id, world, x, y, i_name, i_side, stats, false);
	}
	
	public RobotPlayer(int id, World world, float x, float y, String i_name, FACING_SIDE i_side, PlayerStats stats, boolean isUs)
	{
		super(id, world, x, y, i_name, i_side, stats, isUs);

		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		
		// create player
		bdef.position.set(x / PPM, y / PPM);
		bdef.type = BodyType.DynamicBody;
		bdef.angle = 0;

		body = world.createBody(bdef);
		
		shape.setRadius(defaultPlayerRadius / PPM);
		fdef.shape = shape;
		fdef.density = 0;
		fdef.friction = defaultFriction;
		
		fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
		fdef.filter.maskBits = B2DVars.BIT_PLAYER_MINUS_HEART | B2DVars.BIT_BAR | B2DVars.BIT_IGHOST | B2DVars.BIT_POWERUP;
		body.createFixture(fdef).setUserData("player");
		
		// create foot sensor
		shape.setRadius(defaultFootRadius / PPM);
		fdef.shape = shape;
		fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
		fdef.filter.maskBits = B2DVars.BIT_PLAYER_MINUS_HEART | B2DVars.BIT_BAR | B2DVars.BIT_IGHOST | B2DVars.BIT_POWERUP;
		fdef.isSensor = true;
		fdef.density = defaultDensity;
		fdef.friction = 0f;
		body.createFixture(fdef).setUserData("foot");
		
		//create squish sensor
		shape.setRadius(defaultHeartSize / PPM);
		fdef.shape = shape;
		fdef.filter.categoryBits = B2DVars.BIT_PLAYER_HEART;
		fdef.filter.maskBits = B2DVars.BIT_ALL;
		fdef.isSensor = true;
		fdef.friction = 0f;
		fdef.density = 0f;
		this.heartFixture = body.createFixture(fdef);
		heartFixture.setUserData("squish");
		
		body.setUserData(this);
		
		ground = null;
		
		/*Texture tex = Game.res.getTexture("bunny");
		TextureRegion[] sprites = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];*/
		
		TextureRegion[] sprites = Game.atlas.getTextureAnimation("robot");
		
		Texture tex = Game.res.getTexture("powerup_missile");
		TextureRegion[] smashSprites = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		
		// Adding all processesa
		processManager.addProcess("Standing", new FrameDrawProcess("Standing", this, sprites[1]));
		processManager.addProcess("Jumping", new FrameDrawProcess("Jumping", this, sprites[0]));
		processManager.addProcess("Running", new RunProcess("Running", sprites, 1/6f, this));
		
		processManager.addProcess("Respawn", new RespawnProcess("Respawn", this));
		
		processManager.addProcess("SpeedPowerup", new SpeedPowerupProcess("SpeedPowerup", this));
		processManager.addProcess("FreezePowerup", new FreezePowerupProcess("FreezePowerup", this));
		processManager.addProcess("SmashPowerup", new FlyingPressPowerup("SmashPowerup", this, smashSprites, smashSprites));
		
		processManager.addToActiveProcesses("Jumping");
		processManager.startProcess(i_name);
	}

	public void update(float dt)
	{	
		super.update(dt);
	}
	
	protected void updateMovement()
	{
		if (getIsOnGround() && !right && !left)
		{
			// We apply force so it won't slide too much
			body.setFixedRotation(true);
			
			// And setting the air damping to be the the brakes
			// but only if we are stopping, as opposed to 
			body.setLinearDamping(this.brakes);
		}
		else
		{
			body.setFixedRotation(false);
			body.setLinearDamping(this.airDamping);
		}
		
		float curXVel = body.getLinearVelocity().x;
		float curYVel = body.getLinearVelocity().y;

		if (!lockedMovements && (right || left) && !(right && left))
		{
			ApplyDirection(right);

			// Face the right side
			if (right)
			{
				setSide(FACING_SIDE.RIGHT);
			}
			else if (left)
			{
				setSide(FACING_SIDE.LEFT);
			}
		}
		
		// Anyway, even if nothing was pressed, we want to remember our latest air speed
		if (!getIsOnGround())
		{
			if (!processManager.isProcessActive("Jumping"))
			{
				processManager.addToActiveProcesses("Jumping");
				processManager.removeFromActiveProcesses("Running");
				processManager.removeFromActiveProcesses("Standing");
			}
		}
		else
		{
			// If we are running
			if (((right || left) && !(right && left)) ||
				(processManager.isProcessActive("Running") && Math.abs(curXVel) > maxWalkSpeed))
			{
				if (!processManager.isProcessActive("Running"))
				{
					processManager.addToActiveProcesses("Running");
					processManager.removeFromActiveProcesses("Standing");
					processManager.removeFromActiveProcesses("Jumping");
				}

				if (Math.abs(curXVel) < MAX_SLOW_RUN_SPEED)
					((RunProcess)processManager.getActiveProcess("Running")).runSlow();
				else if (Math.abs(curXVel) < MAX_FAST_RUN_SPEED)
					((RunProcess)processManager.getActiveProcess("Running")).runFast();
				else
					((RunProcess)processManager.getActiveProcess("Running")).runReallyFast();
			}
			// If we are standing still
			else
			{
				if (getIsOnGround() && !processManager.isProcessActive("Standing"))
				{
					processManager.addToActiveProcesses("Standing");
					processManager.removeFromActiveProcesses("Running");
					processManager.removeFromActiveProcesses("Jumping");
				}
			}
		}
		if (jump && !lockedMovements)
		{
			// If we are on the ground or just left it, but we didn't already jump a lil time before
			if ((getIsOnGround() || !isEnoughTimeSinceLeftGround())) //&& isEnoughTimeSinceLastSuccessfulJump())
			{
				body.setLinearDamping(airDamping);
				
				// TODO The force we put when the player jumps varies so it will not jump to high.
				// It depends on his current Y speed
				if (curYVel > maxSlowJumpSpeed && !isEnoughTimeSinceLastSuccessfulJump() && getIsOnGround())
				{
					// TODO if the ground is a wall we do wall jump
					getBody().applyForceToCenter(0, this.jumpForce * jumpChainedFactor, true);
					
					// We jumped
					lastJumpSuccessTime = TimeUtils.millis();
					jump = false;
				}
				else if (isEnoughTimeSinceLastSuccessfulJump())
				{
					getBody().applyForceToCenter(0, this.jumpForce, true);
				
					// We jumped
					lastJumpSuccessTime = TimeUtils.millis();
					jump = false;
				}
			}
			// If enough time passed since we tried jumping but we didn't hit a ground to jump from,
			// we will not jump
			else if (isEnoughTimeSinceTriedJumping())
				jump = false;
		}
	}
	
	public float getDrawingAngle()
	{
		float desiredAngle = 0f;
		float angleCorrectionSpeed = 0f;
		
		if (ground != null)
		{
			Body groundBody = ground.getBody();
			int wallNum = 0;
			
			if (((B2DSprite)groundBody.getUserData()).getSpriteType() == SpriteType.WEIGHT)
			{
				float rx = getXPosition();
				float ry = getYPosition();
				
				float Rx =  groundBody.getLocalCenter().x;
				float Ry =  groundBody.getLocalCenter().y;
				
				float impactAngle = (float) (Math.atan((ry-Ry)/(rx-Rx) ) - ground.getBody().getAngle());
	
				if (impactAngle >= Math.PI / 4 && impactAngle < 3 * Math.PI / 4 )
					wallNum = 1;
				else if (impactAngle >= 3 * Math.PI / 4 && impactAngle < 5 * Math.PI / 4)
					wallNum = 2;
				else if (impactAngle >= 5 * Math.PI / 4 && impactAngle < 0 - Math.PI / 4)
					wallNum = 3;
				else if (impactAngle >= 0 - Math.PI / 4 && impactAngle < Math.PI / 4)
					wallNum = 4;
	
				desiredAngle = (float) (groundBody.getAngle() + (wallNum - 1) * Math.PI / 2);
				//System.out.println(Math.toDegrees(desiredAngle));
				
				desiredAngle =  Play.getInstance().getBar().getBodyAngle();
			}
			else if (((B2DSprite)groundBody.getUserData()).getSpriteType() == SpriteType.BAR)
			{
				desiredAngle =  Play.getInstance().getBar().getBodyAngle();
			}
			
			angleCorrectionSpeed = groundAngleCorrectionSpeed;
		}
		else if (isEnoughTimeSinceLeftGround())
			angleCorrectionSpeed = airAngleCorrectionSpeed;
		
		float angleDelta = Math.min(Math.abs(drawingAngle - desiredAngle), angleCorrectionSpeed);

		if (drawingAngle > desiredAngle)
			drawingAngle -= angleDelta;
		else
			drawingAngle += angleDelta;
//		}
//		else
//		{
			// Recording new angle from the ground
			/*if (Math.abs(ground.getBody().getAngle()) < 80)
			{
				drawingAngle = ground.getBody().getAngle();
			}*/
			
//			drawingAngle = Play.getInstance().getBar().getBodyAngle();
//		}
		
		return drawingAngle;
	}

	protected void activatePowerup()
	{
		// Just activated
		activatePowerup = false;
		activePowerup = availPowerups.removeLast();
				
		switch (activePowerup)
		{
		case FREEZE:
			activateFreeze();
			break;
		case SPEED:
			activateSpeed();
			break;
		case MISSILE:
			activateSmash();
			break;
		case NONE:
		default:
			break;
		}
	}

	private void activateSmash()
	{
		processManager.addToActiveProcessesBack("SmashPowerup");
	}

	private void activateSpeed()
	{
		processManager.addToActiveProcesses("SpeedPowerup");
	}

	private void activateFreeze()
	{
		processManager.addToActiveProcesses("FreezePowerup");
	}

	@Override
	public SpriteType getSpriteType()
	{
		return SpriteType.FLUFFY_PLAYER;
	}
}