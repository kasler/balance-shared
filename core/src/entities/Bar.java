package entities;

import static definitions.B2DVars.PPM;

import java.util.Iterator;

import processes.FrameDrawProcess;
import processes.FreezeProcess;
import states.Play;
import main.Game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

import definitions.B2DVars;
import definitions.B2DVars.SpriteType;

public class Bar extends WeldedableB2DSprite implements Freezable
{
	protected boolean barFlipped;
	protected SpriteFreezer freezer;
	private float breaks;
	private boolean isStickyAlready;
	private Vector2 edge1;
	private Vector2 edge2;
	private Vector2 center;
	private float prevAngVel;
	private float curAngVel;
	private float savedAngVel;
	private int reverts;
	private Body gBody;
	private int ghostBarInteractions;
	
	public Bar(int id, World world, float gameSize)
	{
		super(id);
		
		width = 800 * gameSize;
		height = 26;
		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		
		// create bar
		// TODO pos was 300,150
		bdef.position.set(0 / PPM, 0 / PPM);
		bdef.type = BodyType.KinematicBody;
		bdef.angle = 0;
		body = world.createBody(bdef);
		shape.setAsBox(width / 2 / PPM, height / 2 / PPM);
		fdef.shape = shape;
		fdef.friction = 5f;
		fdef.density = 80f;
		fdef.restitution = 0.1f;
		fdef.filter.categoryBits = B2DVars.BIT_BAR;
		fdef.filter.maskBits = B2DVars.BIT_ALL;
		fdef.filter.maskBits &= ~B2DVars.BIT_PLAYER_HEART;
		body.createFixture(fdef).setUserData("bar");

		BodyDef bgBody = new BodyDef();
		bgBody.type = BodyType.StaticBody;
		
		RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.localAnchorA.set(0,0);
        revoluteJointDef.localAnchorB.set(0 / PPM, 0 / PPM);
        revoluteJointDef.bodyA = body;
        
        revoluteJointDef.bodyB = world.createBody(bgBody);
        revoluteJointDef.enableMotor=false;
        revoluteJointDef.maxMotorTorque = 1000000;
        RevoluteJoint joint = (RevoluteJoint) world.createJoint(revoluteJointDef);
		
		body.setUserData(this);
		
		// create ghost bar
		// TODO pos was 300,150
		bdef.type = BodyType.KinematicBody;
		gBody = world.createBody(bdef);
		fdef.filter.categoryBits = B2DVars.BIT_BAR_GHOST;
		fdef.filter.maskBits = B2DVars.BIT_PLAYER_MINUS_HEART;
		gBody.createFixture(fdef).setUserData("barGhost");
		gBody.setActive(false);
		
		this.name = "bar";
		this.center = new Vector2(getXPosition(), getYPosition());
		this.breaks = 0;
		this.isStickyAlready = false;
		this.edge1 = new Vector2(0, 0);
		this.edge2 = new Vector2(0, 0);
		
		Texture tex = Game.res.getTexture("bar");
		Texture tex2 = Game.res.getTexture("barf");
		//tex.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		TextureRegion[] sprites = TextureRegion.split(tex, 800 * Game.OBJECT_SCALE, 26 * Game.OBJECT_SCALE)[0];
		TextureRegion[] spritesf = TextureRegion.split(tex2, 800 * Game.OBJECT_SCALE, 26 * Game.OBJECT_SCALE)[0];
		
		// Adding all processes
		processManager.addProcess("Still", new FrameDrawProcess("Still", this, sprites[0], gameSize, 1));
		processManager.addProcess("Freeze", new FreezeProcess("Freeze", this, spritesf[0], gameSize, 1));
		
		processManager.addToActiveProcesses("Still");
		
		barFlipped = false;
		
		this.ghostBarInteractions = 0;
		
		freezer = new SpriteFreezer();
	}
	
	public Vector2 getRightEdgePoint()
	{
		double a = getDrawingAngle();
		double h = Math.cos(a) * (width / 2);
		edge1.x = center.x + (float) h;
		edge1.y = center.y + (float) Math.sin(getDrawingAngle()) * (width / 2);
		
		return edge1;
	}
	
	public Vector2 getLeftEdgePoint()
	{
		// Filling edge1 with right info
		getRightEdgePoint();
		
		edge2.x = edge1.x - (2 * (edge1.x - center.x));
		edge2.y = edge1.y - (2 * (edge1.y - center.y));
		
		return edge2;
	}
	
	public void update(float dt)
	{
		float degAngle = (float)Math.toDegrees(getBodyAngle());
		prevAngVel = curAngVel;
		
		if (reverts > 0)
		{
			--reverts;
			body.setAngularVelocity(savedAngVel);
		}
		
		curAngVel = body.getAngularVelocity();
		//System.out.println("hey " + curAngVel);
		
		// Did we lose?
		if (Math.abs(degAngle) > 90)
		{
			flip();
		}
		
		if (breaks > 0)
		{
			body.setAngularVelocity(body.getAngularVelocity() * breaks);
			if (Math.abs(body.getAngularVelocity()) < 0.01f)
				breaks = 0;
		}
		
		if (shouldWeightsStick())
			setStickyWeights(true);
		else
			setStickyWeights(false);
		
		if (ghostBarInteractions > 0)
		{
			// Its active only when somebody needs it, since it is rarely needed
			gBody.setActive(true);
			
			// Calculating ghost bar's position
			gBody.setTransform(body.getPosition().x, body.getPosition().y , body.getAngle());
			gBody.setLinearVelocity(body.getLinearVelocity());
			gBody.setAngularVelocity(body.getAngularVelocity());
			gBody.setAngularDamping(body.getAngularDamping());
			gBody.setLinearDamping(body.getLinearDamping());
			
			System.out.println("GHOSTINGGG");
		}
		else if (gBody.isActive())
			gBody.setActive(false);

		super.update(dt);
	}
	
	public boolean shouldWeightsStick()
	{
		return Math.abs((float)Math.toDegrees(getBodyAngle())) > 40;
	}
	
	private void setStickyWeights(boolean shouldStick)
	{
		if ((isStickyAlready && shouldStick) ||
			(!isStickyAlready && !shouldStick))
			return;
		
		Iterator<Fixture> itr = touchingFixtures.values().iterator();
		
		while (itr.hasNext())
		{
			B2DSprite cur = ((B2DSprite)itr.next().getBody().getUserData());
			
			if (cur instanceof Weight)
				((Weight)cur).setSticky(shouldStick);
		}
	}

	public void render(SpriteBatch sb)
	{
		freezer.renderFreezeStart(sb);
		
		super.render(sb);
		
		freezer.renderFreezeEnd(sb);
	}
	
	public void flip()
	{
		// Remove all joints welded to us
		removeJointsWeldedToUs();
		
		// Become transparent
		Filter f = new Filter();
		f.categoryBits = B2DVars.BIT_BAR;
		f.maskBits = B2DVars.BIT_NONE;
		body.getFixtureList().first().setFilterData(f);
		barFlipped = true;
		
		// :(
	}
	
	public boolean isBarFlipped()
	{
		return barFlipped;
	}
	
	@Override
	public void freeze()
	{
		freezer.freeze();
	}

	@Override
	public void thaw() {
		freezer.thaw();
	}

	@Override
	public boolean isFreezing() {
		return freezer.isFreezing();
	}

	@Override
	public boolean isFrozen() {
		return freezer.isFrozen();
	}

	@Override
	public boolean isThawing() {
		return freezer.isThawing();
	}

	public void breakMomentum(float breaks)
	{
		this.breaks = breaks;
	}
	
	@Override
	public SpriteType getSpriteType()
	{
		return SpriteType.BAR;
	}

	public float getPrevAngVel()
	{
		return prevAngVel;
	}
	
	public void revertToPrevAngVel()
	{
		savedAngVel = prevAngVel;
		reverts = 3;
	}
	
	public void addGhostBarInteraction()
	{
		++ghostBarInteractions;
	}
	
	public void removeGhostBarInteraction()
	{
		if (ghostBarInteractions == 0)
			return;
		
		--ghostBarInteractions;
	}
}
