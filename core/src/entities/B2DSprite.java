package entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import network.messages.UpdateMessage;
import states.Play;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import definitions.B2DVars;
import definitions.B2DVars.FACING_SIDE;

public abstract class B2DSprite extends GameSprite
{
	private static final int PAST_IMAGES = 13;

	protected class PastImage
	{
		public float posX;
		public float posY;
		public float velX;
		public float velY;
		
		public PastImage(float posX, float posY, float velX, float velY)
		{
			this.posX = posX;
			this.posY = posY;
			this.velX = velX;
			this.velY = velY;
		}
	}
	
	protected Body body;
	protected Fixture ground;
	protected HashMap<String, Fixture> touchingFixtures;
	protected HashMap<String, Fixture> touchingGrounds;
	protected Vector2 forceToApply;
	protected LinkedList<PastImage> pastImages;
	protected UpdateMessage updateMessage;
	private int predictionMode; 
	
	public B2DSprite( int id)
	{
		this(id, FACING_SIDE.RIGHT);
	}
	
	public B2DSprite(int id, FACING_SIDE i_side)
	{
		super(id, i_side);
		
		this.body = null;
		ground = null;
		
		forceToApply = new Vector2(0, 0);
		
		touchingFixtures = new HashMap<String, Fixture>();
		touchingGrounds = new HashMap<String, Fixture>();
		
		pastImages = new LinkedList<PastImage>();
		updateMessage = null;
		predictionMode = -1;
	}
	
	public void update(float dt)
	{
		if (!shouldUpdate)
			return;
		
		// Adding new past image
		addPastImage();
		
		// Applying a messaged update if there is one.
		// THIS CAN BE NULL ALSO IF SOME INHERITANCE DECIDED TO APPLY THE MESSAGES BEFORE HERE
		applyUpdateMessage();
		
		super.update(dt);
		
		if (forceToApply.x > 0 || forceToApply.y > 0)
			body.applyForceToCenter(forceToApply, true);
	
	}

	private void addPastImage()
	{
		// If we are in prediction mode, we want to replace past images instead of adding new ones, since we are rewriting history
		if (predictionMode > -1)
		{
			updatePastImage(predictionMode);
			++predictionMode;
		}
		else
		{
			pastImages.addLast(new PastImage(body.getPosition().x, body.getPosition().y,
							   				 body.getLinearVelocity().x, body.getLinearVelocity().y));
			
			if (pastImages.size() > PAST_IMAGES)
				pastImages.removeFirst();
		}
	}

	public void updatePastImage(int index)
	{
		if (index >= pastImages.size())
			return;
		
		PastImage toUpdate = pastImages.get(index);
		
		// Rewriting history
		toUpdate.posX = body.getPosition().x;
		toUpdate.posY = body.getPosition().y;
		toUpdate.velX = body.getLinearVelocity().x;
		toUpdate.velY = body.getLinearVelocity().y;
	}
	
	public void setToPast()
	{
		if (pastImages.size() < PAST_IMAGES)
			return;
		
		PastImage pastImage = pastImages.getFirst();
		
		// Rewriting history
		body.setTransform(pastImage.posX, pastImage.posY, body.getAngle());
		body.setLinearVelocity(pastImage.velX, pastImage.velY);
	}
	
	public float getPastXPosition()
	{
		if (pastImages.size() < PAST_IMAGES)
			return 0;
		
		return pastImages.getFirst().posX * B2DVars.PPM;
	}
	
	public float getPastYPosition()
	{
		if (pastImages.size() < PAST_IMAGES)
			return 0;
		
		return pastImages.getFirst().posY * B2DVars.PPM;
	}
	
	public void isPastMatching(UpdateMessage umsg)
	{
		// Checking if 
	}

	public boolean getIsOnGround()
	{
		return (ground != null);
	}

	public float getBodyAngle()
	{
		return body.getAngle();
	}
	
	public float getDrawingAngle()
	{
		return getBodyAngle();
	}
	
	public void applyForce(float x, float y)
	{
		forceToApply.x = x;
		forceToApply.y = y;
	}
	
	public void addForce(float x, float y)
	{
		forceToApply.add(x, y);
	}

	public boolean isTouchingAnything()
	{
		return !touchingFixtures.isEmpty();
	}
	
	public HashMap<String, Fixture> getTouchingFixtures()
	{
		return this.touchingFixtures;
	}

	public String getFixtureFullName(Fixture fix)
	{
		// Its like bodyname-fixturename
		return  ((B2DSprite)fix.getBody().getUserData()).getName() + "-" + fix.getUserData();
	}
	
	public void addTouch(Fixture touchingFixture, boolean isGroundFixture)
	{
		if (isGroundFixture)
		{
			//prevGroundFixture = groundFixture;
			
			// this is for switching only for first touching ground after air
			//if (touchingGrounds.size() == 0)
			{
				ground = touchingFixture;
			}
			
			touchingGrounds.put(getFixtureFullName(touchingFixture), touchingFixture);
		}
		
		touchingFixtures.put(getFixtureFullName(touchingFixture), touchingFixture);
	}
	
	public void removeTouch(Fixture touchingFixture)
	{
		touchingGrounds.remove(getFixtureFullName(touchingFixture));
		
		// If we lost our ground..
		if (ground == touchingFixture)
		{
			// prev stuff
			
			// Get another one from the ones we already touch
			if (touchingGrounds.size() != 0)
			{
				java.util.Iterator<Fixture> itr = touchingGrounds.values().iterator();
				ground = itr.next();
			}
			else
			{
				ground = null;
			}
		}

		touchingFixtures.remove(getFixtureFullName(touchingFixture));
	}
	
	protected void removeAllTouches()
	{
		// We use a copy of the list because remove touch is going to remove elements from the list while we iterate
		List<Fixture> copyList = new ArrayList<Fixture>(touchingFixtures.values());
		
		for (Fixture fix : copyList)
		    removeTouch(fix);
	}
	
	public void delete()
	{
		super.delete();

		body.getWorld().destroyBody(body);
	}

	public float getXPosition() { return body.getPosition().x * B2DVars.PPM ; }
	public float getYPosition() { return body.getPosition().y * B2DVars.PPM ; }
	
	public Vector2 getB2DPosition() { return body.getPosition(); }
	
	public Body getBody() { return body; }
	public float getMass() { return body.getMass(); }

	public Fixture getTouchedFixtureRecursively(String name)
	{
		boolean checkedBodies[] = new boolean[Play.getInstance().getNextId()];
		
		return getTouchedFixtureRecursively(name, checkedBodies);
		
		// TODO dispose checkedBodies		
	}

	private Fixture getTouchedFixtureRecursively(String name, boolean[] checkedBodies)
	{
		Iterator<String> itr = touchingFixtures.keySet().iterator();

		while (itr.hasNext())
		{
			String curName = itr.next();
			Fixture curFix = touchingFixtures.get(curName);
			B2DSprite curSprite = ((B2DSprite)curFix.getBody().getUserData());
			
			if (curName.equals(name))
			{
				return curFix;
			}
			// If we didn't do this recursion for this body yet
			else if (checkedBodies[curSprite.getId()] == false)
			{				
				checkedBodies[curSprite.getId()] = true;

				Fixture res = curSprite.getTouchedFixtureRecursively(name, checkedBodies);				
				
				// Found it. else, continue searching
				if (res != null)
					return res;
				
			}
		}
		
		return null;
	} 
	
	public boolean hasFallen()
	{
		return !Play.getInstance().isWithinMapBorders(getXPosition(), getYPosition());
	}
	
	public void setUpdateMessage(UpdateMessage umsg)
	{
		// Applying at the start of the update
		this.updateMessage = umsg;
	}
	
	protected void applyUpdateMessage()
	{
		if (this.updateMessage == null)
			return;
		
		this.body.setTransform(this.updateMessage.posX, this.updateMessage.posY, this.body.getAngle());
		this.body.setLinearVelocity(this.updateMessage.velX, this.updateMessage.velY);
		
		this.updateMessage = null;
	}
	
	public void setPredictionMode(boolean isPredicting)
	{
		if (isPredicting)
			this.predictionMode = 0;
		else
			this.predictionMode = -1;
	}
}