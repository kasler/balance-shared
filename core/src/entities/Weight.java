package entities;

import static definitions.B2DVars.PPM;
import processes.FrameDrawProcess;
import processes.FreezeProcess;
import gameObjects.Timer;
import states.Play;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;

import definitions.B2DVars;
import definitions.B2DVars.SpriteType;
import main.Game;

public class Weight extends WeldedableB2DSprite implements Freezable
{
	protected boolean touchedGround;
	float angle;
	float type;
	float mass;
	int id;
	Bar bar;
	boolean createdJoint;
	long jointTimerStartTime;
	protected boolean createJointInstantly;
	protected boolean timerIsRunning;
	protected Joint joint;
	boolean fixedAngle;
	protected SpriteFreezer freezer;
	private Body igbody;
	private int size;
	private float stickyAngularDamping;
	private float normalAngularDamping;
	private Timer weldTimer;
	
	public Weight(int id, String name, float x, float y, int size, World world)
	{
		super(id);
		
		x = Play.getInstance().getOurPlayer().getXPosition();
		this.name = name;
		if (this.name == null)
			this.name = "Weight" + String.valueOf(this.id);
		
		this.size = size;
		this.width = size * 28;
		this.height = size * 28;
		touchedGround = false;
		bar = Play.getInstance().getBar();
		
		//createdJoint = false;
		//jointTimerStartTime = 0;
		//createJointInstantly = false;
		//timerIsRunning = false;
		//joint = null;
		
		fixedAngle = true;
		
		freezer = new SpriteFreezer();
		
		normalAngularDamping = 0f;
		stickyAngularDamping = 2000000000f;
		
		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		  
		// create weight
		bdef.position.set(x / PPM, y / PPM);
		bdef.type = BodyType.DynamicBody;
		bdef.angle = bar.getBodyAngle();
		bdef.angularDamping = normalAngularDamping;
	
		body = world.createBody(bdef);
		
		shape.setAsBox(width/ 2 / PPM, height/ 2 / PPM);
		fdef.shape = shape;
		fdef.filter.categoryBits = B2DVars.BIT_WEIGHT;
		fdef.filter.maskBits = B2DVars.BIT_WEIGHT | B2DVars.BIT_BAR | B2DVars.BIT_PLAYER;
		fdef.friction = 500f;
		fdef.density = 1.5f;
		
		body.createFixture(fdef).setUserData("weight");

		// create weight inertia ghost
		BodyDef igbdef = new BodyDef();
		FixtureDef igfdef = new FixtureDef();
		igbdef.position.set(body.getPosition().x ,body.getPosition().y);
		//igbdef.type = BodyType.KinematicBody;
		igbdef.type = BodyType.DynamicBody;
		igbdef.angle = body.getAngle();
		bdef.angularDamping = normalAngularDamping;
	
		igbody = world.createBody(igbdef);
		
		shape.setAsBox(width / 2 / PPM, height / 2 / PPM);
		igfdef.shape = shape;
		igfdef.filter.categoryBits = B2DVars.BIT_IGHOST;
		igfdef.filter.maskBits = B2DVars.BIT_PLAYER | B2DVars.BIT_POWERUP;
		igfdef.friction = 5f;
		igfdef.density = 9999999999999999999999f;
		
		igbody.createFixture(igfdef).setUserData("inertiaGhost");
		
		body.setUserData(this);
		igbody.setUserData(this);
		
		Texture tex = Game.res.getTexture("weight");
		Texture tex2 = Game.res.getTexture("weightf");
		TextureRegion[] sprites = TextureRegion.split(tex, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		TextureRegion[] spritesf = TextureRegion.split(tex2, 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		processManager.addProcess("Standing", new FrameDrawProcess("Standing", this, sprites[0], size, size));
		processManager.addProcess("Freeze", new FreezeProcess("Freeze", this, spritesf[0], size, size));

		processManager.addToActiveProcesses("Standing");
	}
	
	public void update(float dt)
	{
		if (fixedAngle)
		{
			body.setTransform(body.getTransform().getPosition(), bar.getBodyAngle());
		}
		
		//System.out.println("ANG " + body.getAngularVelocity());
		
		// If we fell, we remove this weight from the world 
		if (hasFallen())
			Destroy();
		
		else if (!createdJoint && timerIsRunning)
		{
			CreateJointByTimer();
		}

		super.update(dt);

		igbody.setTransform(body.getPosition().x, body.getPosition().y , body.getAngle());
		igbody.setLinearVelocity(body.getLinearVelocity());
		igbody.setAngularVelocity(body.getAngularVelocity());
		igbody.setAngularDamping(body.getAngularDamping());
		igbody.setLinearDamping(body.getLinearDamping());
		//igbody.setTransform(body.getPosition().x, body.getPosition().y , body.getAngle());
	}
	
	public void render(SpriteBatch sb)
	{
		freezer.renderFreezeStart(sb);
		
		super.render(sb);
		
		freezer.renderFreezeEnd(sb);
	}
	
	public void CreateJointByTimer()
	{
		/*if (weldTimer)
		{
			CreateJoint();
			timerIsRunning = false;
		}*/
	}
	
	public void CreateJoint()
	{
		//Vector2 vec = new Vector2(body.getPosition().x, ground.getBody().getPosition().y);
		
		WeldJointDef weldJointDef = new WeldJointDef();
		
		weldJointDef.initialize(body, ground.getBody(), body.getWorldCenter());
		//weldJointDef.initialize(body, groundBody, vec);
	 
		joint = body.getWorld().createJoint(weldJointDef);
		
		//fixture.setDensity(1110);
	
		createdJoint = true;
		
		addJointToHim();
		
		StopJointTimer();
	}
	
	public void removeJoint()
	{
		if (joint != null)
		{
			body.getWorld().destroyJoint(joint);
			joint = null;
			createdJoint = false;
		}
	}
	
	public void addJointToHim()
	{
		((WeldedableB2DSprite)ground.getBody().getUserData()).addJointToUs(this);
	}
	
	public void Destroy()
	{
		// We need to remove our joint, and tell everyone that is welded to us
		// to remove their joints to us
		removeJoint();

		// Our time has come
		prepareForDelete();
	}
	
	public void delete()
	{
		super.delete();

		igbody.getWorld().destroyBody(igbody);
	}
	
	public void addTouch(Fixture touchingFixture, boolean isGroundFixture)
	{
		fixedAngle = false;

		super.addTouch(touchingFixture, isGroundFixture);
		
		if (isGroundFixture)
		{
			setSticky(Play.getInstance().getBar().shouldWeightsStick());
		}
		
		if (isGroundFixture && !createdJoint && !timerIsRunning)
		{
			ResetJointTimer();
		}
	}
	
	public void removeTouch(Fixture touchingFixture)
	{
		super.removeTouch(touchingFixture);
		
		if (touchingGrounds.size() == 0)
			setSticky(false);
		
		if (!getIsOnGround() && timerIsRunning)
		{
			StopJointTimer();
		}
	}
	
	// TIMER STUFF
	protected void ResetJointTimer()
	{
		jointTimerStartTime = System.nanoTime();
		timerIsRunning = true;
	}
	
	protected void StopJointTimer()
	{
		timerIsRunning = false;
	}

	public boolean isWelded()
	{
		return createdJoint;
	}

	@Override
	public void freeze()
	{
		freezer.freeze();
	}

	@Override
	public void thaw() {
		freezer.thaw();
	}

	@Override
	public boolean isFreezing() {
		return freezer.isFreezing();
	}

	@Override
	public boolean isFrozen() {
		return freezer.isFrozen();
	}

	@Override
	public boolean isThawing() {
		return freezer.isThawing();
	}

	public int getSize()
	{
		return this.size;
	}

	public void setSticky(boolean shouldStick)
	{
		if (shouldStick)
			body.setAngularDamping(stickyAngularDamping);
		else
			body.setAngularDamping(normalAngularDamping);
	}
	
	@Override
	public SpriteType getSpriteType()
	{
		return SpriteType.WEIGHT;
	}
}