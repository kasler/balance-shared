package entities;

public interface Freezable
{
	public void freeze();
	
	public void thaw();
	
	public boolean isFrozen();
	
	public boolean isFreezing();
	
	public boolean isThawing();
}
