package entities;

import handlers.ProcessManager;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import definitions.B2DVars;
import definitions.B2DVars.FACING_SIDE;
import definitions.B2DVars.SpriteType;

public abstract class GameSprite
{
	protected float width;
	protected float height;
	protected FACING_SIDE side;
	protected boolean deleteMe;
	protected boolean prepareForDelete;
	protected String name;
	protected int id;
	protected float drawingAngle;
	protected Vector2 position;
	private boolean shouldRender;
	protected boolean shouldUpdate;
	protected ProcessManager processManager;
	
	protected boolean isDebug;
	
	
	protected Vector2 serverPos;
	protected Vector2 serverVelocity;
	protected Vector2 serverAngVelocity;
	protected Vector2 serverAngle;
	
	public GameSprite( int id, Vector2 position)
	{
		this(id, FACING_SIDE.RIGHT);
	}
	
	public GameSprite( int id)
	{
		this(id, FACING_SIDE.RIGHT, null);
	}
	
	public GameSprite(int id, FACING_SIDE i_side)
	{
		this(id, i_side, null);
	}
	
	public GameSprite(int id, FACING_SIDE i_side, Vector2 position)
	{
		this.id = id;
		name = "unknown";
		side = i_side;
		this.position = position;
		deleteMe = false;
		prepareForDelete = false;
		
		this.shouldRender = true;
		this.shouldUpdate = true;
		
		this.processManager = new ProcessManager();
		
		this.serverPos = new Vector2();
		
		isDebug = false;
	}
	
	public void update(float dt)
	{
		if (!shouldUpdate)
			return;
		
		processManager.update(dt);
		
		// If we are deleting, we are waiting for all processes to finish
		if (prepareForDelete)
		{
			tryDelete();
		}
	}
	
	private void tryDelete()
	{
		deleteMe = !processManager.areProcessesActive();
	}
	
	public void render(SpriteBatch sb)
	{
		if (!shouldRender)
			return;
		
		processManager.render(sb);
	}
	
	public void setSide(FACING_SIDE i_side)
	{
		side = i_side;
	}
	
	public FACING_SIDE getSide()
	{
		return side;
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public float getDrawingAngle()
	{
		return drawingAngle;
	}
	
	public void prepareForDelete()
	{
		processManager.finishAllActiveProcesses();
		
	    prepareForDelete = true;
	    System.out.println("READY FOR DELETE");
	}
	
	public boolean getShouldDelete()
	{
		return deleteMe;
	}
	
	public void forceDelete()
	{
		delete();
	}
	
	public void delete()
	{
		processManager.clearAll();
	}
	
	public ProcessManager getProcessManager()
	{
		return processManager;
	}
	
	public float getXPosition() { return position.x; }
	public float getYPosition() { return position.y; }
	public float getWidth() { return width; }
	public float getHeight() { return height; }
	public String getName() { return name; }

	public void setDrawingAngle(float angle)
	{
		this.drawingAngle = angle;
	}
	
	protected void setRender(boolean shouldRender)
	{
		this.shouldRender = shouldRender;
	}
	
	protected void setUpdate(boolean shouldUpdate)
	{
		this.shouldUpdate = shouldUpdate;
	}
	
	public abstract SpriteType getSpriteType();

	public float getXServerPosition()
	{
		return serverPos.x * B2DVars.PPM;
	}
	
	public float getYServerPosition()
	{
		return serverPos.y * B2DVars.PPM;
	}
	
	public Vector2 getServerPos()
	{
		return serverPos;
	}
	
	public void setServerPos(float posX, float posY)
	{
		this.serverPos.x = posX;
		this.serverPos.y = posY;
	}

	public boolean isDebug()
	{
		return isDebug;
	}
	
	public void setDebug()
	{
		isDebug = true;
	}
}