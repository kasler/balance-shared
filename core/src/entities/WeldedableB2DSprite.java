package entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class WeldedableB2DSprite extends B2DSprite
{
	protected List<Weight> weldedToUs;

	public WeldedableB2DSprite(int id)
	{
		super(id);

		weldedToUs = new ArrayList<Weight>();
	}

	public void addJointToUs(Weight him)
	{
		weldedToUs.add(him);
	}
	
	public void removeJointsWeldedToUs()
	{
		Iterator<Weight> itr = weldedToUs.iterator();
		while(itr.hasNext())
		{
			itr.next().removeJoint();
		}
	}
	
	public void prepareForDelete()
	{
		removeJointsWeldedToUs();
		weldedToUs.clear();
		
		super.prepareForDelete();
	}
}
