package entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import definitions.B2DVars.FACING_SIDE;

public class SpriteFreezer implements Freezable
{
	protected boolean isFreezing;
	protected boolean isFrozen;
	protected boolean isThawing;
	protected float bluePercent;
	protected Color originalColor;

	public SpriteFreezer(/*int id, FACING_SIDE i_side*/)
	{
		//super(id, i_side);
		
		isFreezing = false;
		isFrozen = false;
		isThawing = false;
		bluePercent = 1;
		originalColor = null;
	}
	
	public void renderFreezeStart(SpriteBatch sb)
	{
		if (isFrozen)
		{
			// Don't make it too blue
			if (isFreezing)
			{
				bluePercent += 0.05;
				
				if (bluePercent >= 1.4f)
				{
					isFreezing = false;
				}
			}
			else if (isThawing)
			{
				bluePercent -= 0.05;
				
				if (bluePercent <= 1f)
				{
					isThawing = false;
					isFrozen = false;
				}
			}
		
			// Adding the blue percent
			Color color = sb.getColor();
						
			originalColor = color.cpy();
			
			
			color.b *= bluePercent;
			//color.r /= bluePercent*2;
			//color.g /= bluePercent*2;
			
			
			if (color.b > 2)
			{
				color.b = 2;
			}

			/*if (color.g < 0.1f)
			{
				color.g = 0.1f;
			}
			
			if (color.r < 0.1f)
			{
				color.r = 0.1f;
			}*/
			
//			color.a *= 0.9;

			//sb.setColor(color);
		}
	}
	
	public void renderFreezeEnd(SpriteBatch sb)
	{
		if (isFrozen)
		{
			sb.setColor(originalColor);
			if (bluePercent == 1)
				isFrozen = false;
		}
	}
	
	@Override
	public void freeze()
	{
		if (!isFrozen)
		{
			isFreezing = true;
			isThawing = false;
			isFrozen = true;
		}
	}

	@Override
	public void thaw()
	{
		if (isFrozen)
		{
			isThawing = true;
			isFreezing = false;
		}
	}

	@Override
	public boolean isFreezing()
	{
		return isFreezing;
	}

	@Override
	public boolean isFrozen()
	{
		return isFrozen;
	}

	@Override
	public boolean isThawing()
	{
		return isThawing;
	}
}
