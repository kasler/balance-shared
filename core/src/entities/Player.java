package entities;

import static definitions.B2DVars.PPM;

import java.util.Iterator;
import java.util.LinkedList;

import network.messages.KeyPressMessage;
import processes.Process;
import states.Play;

import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import definitions.B2DVars;
import definitions.B2DVars.FACING_SIDE;
import definitions.B2DVars.POWERUP_TYPE;
import definitions.PlayerStats;

public abstract class Player extends B2DSprite
{
	/*protected class PastPlayerImage extends PastImage
	{
		public float posX;
		public float posY;
		public float velX;
		public float velY;
		
		public PastPlayerImage(float posX, float posY, float velX, float velY, )
		{
			this.posX = posX;
			this.posY = posY;
			this.velX = velX;
			this.velY = velY;
		}
	}*/
	
	protected static final float jumpChainedFactor = 0.3f;
	protected static final float maxSlowJumpSpeed = 1f;
	protected static final long jumpsTriesMinInterval = 100;
	protected static final long jumpsMinInterval = 600;
	protected static final long groundLeaveDelay = 100;
	protected static final float brakesForceFactor = 1.5f;
	protected static final float airFastMovementForceFactor = 0.6f;
	protected static final float airSlowMovementForceFactor = 0.6f;
	
	protected boolean isDead;
	protected boolean right;
	protected boolean left;
	protected boolean jump;
	protected float currXAirVel;
	protected boolean activatePowerup;
	protected LinkedList<POWERUP_TYPE> availPowerups;
	protected POWERUP_TYPE activePowerup;
	protected float topSpeed;
	protected float acceleration;
	protected boolean lockedMovements;
	protected long lastJumpCommandTime;
	protected long lastGroundTouchTime;
	protected long lastGroundLeaveTime;
	protected long lastJumpSuccessTime;
	private boolean leftGroundTimePassed;
	private boolean isGodMode;
	protected PlayerStats stats;
	private int timesRespawned;
	protected boolean isUs;
	protected boolean respawningInProgress;
	protected int availPowerupsAmount;
	private boolean isSmashing;
	private Fixture ghostBarWeight;
	protected Fixture heartFixture;
	protected float airDamping;
	protected float jumpForce;
	protected float brakes;
	private KeyPressMessage newKeyPress;
	private boolean isNewKeyPress;
	
	public Player(int id, World world, float x, float y, String i_name, PlayerStats stats, FACING_SIDE i_side)
	{
		this(id, world, x, y, i_name, i_side, stats, false);
	}
	
	public Player(int id, World world, float x, float y, String i_name, FACING_SIDE i_side, PlayerStats stats, boolean isUs)
	{
		super(id, i_side);
		
		this.isUs = isUs;
		this.stats = stats;
		isDead = false;
		timesRespawned = 0;
		isGodMode = false;
		right = false;
		left = false;
		jump = false;
		currXAirVel = 0;
		name = i_name;
		drawingAngle = 0;
		activatePowerup = false;
		availPowerups = new LinkedList<POWERUP_TYPE>();
		availPowerups.add(POWERUP_TYPE.MISSILE);
		activePowerup = POWERUP_TYPE.NONE;
		lastJumpCommandTime = TimeUtils.millis();
		lastGroundTouchTime = lastJumpCommandTime;
		lastJumpSuccessTime = lastJumpCommandTime;
		leftGroundTimePassed = false;
		respawningInProgress = false;
		
		newKeyPress = null;
		isNewKeyPress = false;
		
		setJumpForceToDefault();
		setAccelerationToDefault();
		setTopSpeedToDefault();
		setAirDampingToDefault();
		setBrakesToDefault();
	}

	public void setBrakesToDefault()
	{
		this.brakes = stats.brakes;
	}

	public void setAirDampingToDefault()
	{
		this.airDamping = stats.airDamping;
	}

	public void setTopSpeedToDefault()
	{
		this.topSpeed = stats.topSpeed;
	}

	public void setAccelerationToDefault()
	{
		this.acceleration = stats.accelerationForce;
	}
	
	public void setJumpForceToDefault()
	{
		this.jumpForce = stats.jumpForce;
	}

	public void ApplyDirection(boolean fRight)
	{
		float Force = this.getAcceleration();

		if (!fRight)
			Force *= -1f;
		
		// If we try to switch side, we will push with a bit more force
		if (fRight && body.getLinearVelocity().x < 0 ||
			!fRight && body.getLinearVelocity().x > 0)
		{
			Force *= brakesForceFactor;
			
			// Apply the brakes too, but not in the air
			if (getIsOnGround())
				body.setLinearDamping(this.brakes);
		}
		else
			body.setLinearDamping(this.airDamping);
		
		float xVec = 0;
		float yVec = 0;
		float angle = getDrawingAngle();
		
		if ((left  && getBody().getLinearVelocity().x < -1*getTopSpeed()) ||
			(right && getBody().getLinearVelocity().x > getTopSpeed())	||
			(left  && getBody().getLinearVelocity().y < -1*getTopSpeed()) ||
			(right && getBody().getLinearVelocity().y > getTopSpeed()))
		{
			xVec = 0;
			yVec = 0;
		}
		else if (getIsOnGround())
		{
			xVec = (float) (Force * Math.cos(angle));
			yVec = (float) (Force * Math.sin(angle));
		}
		// In the air we use much less force, since we can't change our speed that much mid air
		else 
		{
			xVec = Force;
			
			if (Math.abs(getBody().getLinearVelocity().x) > getTopSpeed() / 2)
				xVec *= airFastMovementForceFactor;
			else
				xVec *= airSlowMovementForceFactor;
			yVec = 0;
			
		}
		
		getBody().applyForceToCenter(xVec, yVec, true);
	}
	
	public void update(float dt)
	{
		if (!shouldUpdate)
			return;
		
		//if (c != null)
			//System.out.println(c.getWorldManifold().getNormal().x + " " + c.getWorldManifold().getNormal().y);
		
		// Adding new past image
		//addPastImage();
				
		applyUpdateMessage();
		
		if ((isDead || hasFallen()) && !respawningInProgress)
		{
			respawningInProgress = true;
			
			System.out.println("DEAD");
			body.setActive(false);
			
			if (processManager.isProcessActive("Respawn"))
				processManager.removeFromActiveProcesses("Respawn");
			processManager.addToActiveProcessesBack("Respawn");
		}
		else
		{
			// Movement talk
			updateMovement();
	
			// Powerup talk
			if (activatePowerup)
			{
				System.out.println("ACT");
				activatePowerup();
			}
		}

		super.update(dt);
	}

	protected abstract void updateMovement();

	protected abstract void activatePowerup();

	public void setMoveRight(boolean bool)
	{
		if ((bool && right) || (!bool && !right))
			return;
		
		right = bool;

		prepareKeyPressMessage();
	}
	
	public void setMoveLeft(boolean bool)
	{
		if ((bool && left) || (!bool && !left))
			return;
		
		left = bool;
		
		prepareKeyPressMessage();
	}
	
	public void setJump(boolean bool)
	{
		if ((bool && jump) || (!bool && !jump))
			return;
		
		setJump(bool, TimeUtils.millis());
	}
	
	public void setJump(boolean bool, long pressTime)
	{
		if ((bool && jump) || (!bool && !jump))
			return;
		
		jump = bool;
		lastJumpCommandTime = pressTime;
		
		prepareKeyPressMessage();
	}

	public float getVelocity()
	{
		return (float) Math.sqrt(Math.pow(body.getLinearVelocity().x, 2) + 
								 Math.pow(body.getLinearVelocity().y, 2));
	}
	
	public void addTouch(Fixture touchingFixture, boolean isGroundFixture)
	{
		System.out.println("added " + touchingFixture.getUserData());
		super.addTouch(touchingFixture, isGroundFixture);
		System.out.println("touching " + touchingFixtures.size());
		
		if (isGroundFixture)
		{
			lastGroundTouchTime = TimeUtils.millis();
		}
	}
	
	public void removeTouch(Fixture touchingFixture)
	{
		System.out.println("removed " + touchingFixture.getUserData());
		super.removeTouch(touchingFixture);
		System.out.println("touching " + touchingFixtures.size());
		
		// If it was a weight that made us intercat with the ghost bar
		if (ghostBarWeight == touchingFixture)
			disableGhostBarMode();
		
		if (touchingGrounds.size() == 0)
		{
			body.setLinearDamping(airDamping);
			lastGroundLeaveTime = TimeUtils.millis();
			leftGroundTimePassed = false;
		}
	}

	public float getLatestAirVel()
	{
		return currXAirVel;
	}
	
	protected boolean isEnoughTimeSinceTriedJumping()
	{
		return (TimeUtils.timeSinceMillis(lastJumpCommandTime) > jumpsTriesMinInterval);
	}
	
	protected boolean isEnoughTimeSinceLastSuccessfulJump()
	{
		return (TimeUtils.timeSinceMillis(lastJumpSuccessTime) > jumpsMinInterval);
	}

	protected boolean isEnoughTimeSinceLeftGround()
	{
		if (leftGroundTimePassed)
			return true;
		if (TimeUtils.timeSinceMillis(lastGroundLeaveTime) > groundLeaveDelay)
		{
			leftGroundTimePassed = true;
			return true;
		}
		
		return false;
	}

	public void obtainPowerup(POWERUP_TYPE type)
	{
		if (availPowerups.size() == stats.powerupsCapacity)
			availPowerups.removeFirst();
		
		availPowerups.addLast(type);
		++availPowerupsAmount;
	}
	
	public LinkedList<POWERUP_TYPE> getAvailablePowerups()
	{
		return availPowerups;
	}
	
	public void usePowerup()
	{
		if (availPowerups.size() > 0)
			activatePowerup = true;
	}

	public float getTopSpeed() {
		return topSpeed;
	}

	public void setTopSpeed(float topSpeed) {
		this.topSpeed = topSpeed;
	}

	public float getFriction() {
		return body.getFixtureList().get(0).getFriction();
	}
	
	public void setAirDamping(float airDamping)
	{
		this.airDamping = airDamping;
	}

	public float getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(float acceleration) {
		this.acceleration = acceleration;
	}
	
	public void setLockedMovements(boolean areLocked)
	{
		lockedMovements = areLocked;
	}

	public void die()
	{
		if (!isGodMode)
			isDead = true;
	}
	
	public boolean isAlive()
	{
		return !isDead;
	}
	
	public void setGodMode(boolean isGodMode)
	{
		this.isGodMode = isGodMode;
		
		// Disable is heart until god mode is over
		if (heartFixture != null)
		{
			Filter f = heartFixture.getFilterData();
			
			if (isGodMode)
				f.maskBits = B2DVars.BIT_NONE;
			else
				f.maskBits = B2DVars.BIT_ALL;
			
			heartFixture.setFilterData(f);
		}
					
	}
	
	public void revive()
	{
		isDead = false;
	}

	public boolean isFixtureGroundable(Fixture otherFx)
	{
		float groundAngle = otherFx.getBody().getAngle();
		
		if ((otherFx.getUserData().equals("weight_wall_1") &&
			 (groundAngle > -1*Math.PI/2) && (groundAngle <= Math.PI/2))
			 																||
			(otherFx.getUserData().equals("weight_wall_3") &&
			 !(groundAngle > -1*Math.PI/2) && (groundAngle <= Math.PI/2))
			 																||
			(otherFx.getUserData().equals("weight_wall_2") &&
			 (groundAngle > -1*Math.PI) && (groundAngle <= 0))
			 																||
			(otherFx.getUserData().equals("weight_wall_4") &&
			 !(groundAngle > -1*Math.PI) && (groundAngle <= 0)))
		{
			return true;
		}

		return false;
	}

	public PlayerStats getStats()
	{
		return stats;
	}
	
	public int getTimesRespawned()
	{
		return timesRespawned;
	}

	public void respawn()
	{
		body.setTransform((200 + (int)(Math.random()*200)) / PPM, 400 / PPM, getDrawingAngle());
		body.setLinearVelocity(0, 0);
		body.setAngularVelocity(0);
		body.setActive(true);
		
		setRender(true);
		
		// Only we can control the camera in this situation, not other players
		if (isUs)
			Play.getInstance().GetGameCamera().followBack();
		
		lockedMovements = false;
		
		++timesRespawned;
		revive();
		
		respawningInProgress = false;
	}

	public void disappear()
	{
		body.setActive(false);
		setRender(false);
		
		lockedMovements = true;
		
		// Only we can control the camera in this situation, not other players
		if (isUs)
			Play.getInstance().GetGameCamera().showOverview();
		
		// Removing all active processes other then the one that will revive us
		Iterator<Process> itr = processManager.getActiveProcessesIterator();
		
		while (itr.hasNext())
		{
			String cur = itr.next().getName();
			if (!cur.equals("Respawn"))
				processManager.removeFromActiveProcessesByIterator(cur, itr, true);
		}
		
		removeAllTouches();
		
		//touchingFixtures.clear();
		//touchingGrounds.clear();
		ground = null;
	}

	public void setIsSmashing(boolean isSmashing)
	{
		this.isSmashing = isSmashing;
	}

	public boolean isSmashing()
	{
		return isSmashing;
	}
	
	public boolean isUs()
	{
		return this.isUs;
	}

	public void applyGhostBarMode(Fixture weightFx)
	{
		// We only apply once
		// TODO it's more correct to have a list of ghostbarweightfixtures
		if (ghostBarWeight != null)
			return;

		ghostBarWeight = weightFx;
		
		Play.getInstance().getBar().addGhostBarInteraction();
		
		// Applying interaction with ghost bar
		Filter f = body.getFixtureList().first().getFilterData();
		f.maskBits &= ~B2DVars.BIT_BAR;
		f.maskBits |=  B2DVars.BIT_BAR_GHOST;
		body.getFixtureList().first().setFilterData(f);
	}
	
	private void disableGhostBarMode()
	{
		ghostBarWeight = null;

		Play.getInstance().getBar().removeGhostBarInteraction();
		
		// Disabling interaction with ghost bar
		Filter f = body.getFixtureList().first().getFilterData();
		f.maskBits |= B2DVars.BIT_BAR;
		f.maskBits &= ~B2DVars.BIT_BAR_GHOST;
		body.getFixtureList().first().setFilterData(f);
	}

	public void setBrakes(float brakes)
	{
		this.brakes = brakes;
	}
	
	private void prepareKeyPressMessage()
	{
		isNewKeyPress = true;
	}

	public KeyPressMessage getNewPress()
	{
		if (isNewKeyPress == false)
			return null;
		
		// debugs don't send
		if (isDebug)
			return null;
		
		newKeyPress = new KeyPressMessage();

		newKeyPress.isLeftPressed = isMovingLeft();		
		newKeyPress.isRightPressed = isMovingRight();
		newKeyPress.isJumpPressed = isJumping();
		
		isNewKeyPress = false;
		
		return newKeyPress;
	}

	public boolean isMovingRight()
	{
		return right;
	}
	
	public boolean isMovingLeft()
	{
		return left;
	}
	
	public boolean isJumping()
	{
		return jump;
	}
}