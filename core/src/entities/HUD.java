package entities;

import static definitions.B2DVars.ZOOM;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import definitions.B2DVars;
import definitions.B2DVars.POWERUP_TYPE;
import main.Game;

public class HUD
{
	private TextureRegion[][] powerups;
	Player player;
	
	public HUD(Player player)
	{
		this.player = player;
		
		powerups = new TextureRegion[3][];
		
		powerups[0] = TextureRegion.split(Game.res.getTexture("powerup_missile"), 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		powerups[1] = TextureRegion.split(Game.res.getTexture("powerup_speed"), 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
		powerups[2] = TextureRegion.split(Game.res.getTexture("powerup_freeze"), 32 * Game.OBJECT_SCALE, 32 * Game.OBJECT_SCALE)[0];
	}
	
	public void render(SpriteBatch sb)
	{
		int type = 0;
		int offset = 5;

		Iterator<POWERUP_TYPE> itr = player.getAvailablePowerups().iterator();
		
		while (itr.hasNext())
		{
			switch (itr.next())
			{
			case MISSILE:
				type = 0;
				break;
			case SPEED:
				type = 1;
				break;
			case FREEZE:
			default:
				type = 2;
				break;
			}
		
			sb.draw(powerups[type][0], 
					offset,
					215f,
					(float)powerups[type][0].getRegionHeight() / Game.OBJECT_SCALE / 2f,  
					(float)powerups[type][0].getRegionWidth() / Game.OBJECT_SCALE  / 2f,
					(float)powerups[type][0].getRegionHeight() / Game.OBJECT_SCALE / 1.5f,
					(float)powerups[type][0].getRegionWidth() / Game.OBJECT_SCALE / 1.5f,
					1f,
					1f,
					0f);
			
			//sb.draw(powerups[type], offset, 200);
			offset += 20;
		}
	}
}